#pragma once

#ifndef __CHEATDATA_H__
#define __CHEATDATA_H__

#include "ImGui/imgui.h"

#define NUMBER_OF_CHEATS 1

class C_CheatData {
public:
	char update_time[32] = { "0 Days" };
	const char* name;
	const char* status = "Disabled";
	const char* version;
	const char* hash;
	int rank_boundaries[0x2];
	bool cheat_selection;
	bool has_beta;
	double build;
	int size;
	ImVec4 status_color = ImVec4(255.f, 255.f, 255.f, 255.f);
public:
	void RenewData(int identification, int* iLastError = nullptr);
	bool Verify(char* response);
};

extern C_CheatData* g_pCheatData[2];

#endif