#pragma once

#ifndef __NATIVE_H_
#define __NATIVE_H_

#include <Windows.h>

typedef struct _LSA_UNICODE_STRING
{
	USHORT Length;
	USHORT MaximumLength;
	wchar_t *Buffer;
} LSA_UNICODE_STRING, *PLSA_UNICODE_STRING, UNICODE_STRING, *PUNICODE_STRING;

typedef struct RTL_BALANCED_NODE
{
	union
	{
		void* Children[2];

		struct
		{
			void* Left;
			void* Right;
		};
	};

	union
	{
		struct
		{
			__int8 Red : 1;
		};

		struct
		{
			__int8 Balance : 2;
		};

		unsigned __int64 ParentValue;
	};
} RTL_BALANCED_NODE, *PRTL_BALANCED_NODE;

typedef struct _INITIAL_TEB
{
	PVOID 	PreviousStackBase;
	PVOID 	PreviousStackLimit;
	PVOID 	StackBase;
	PVOID 	StackLimit;
	PVOID 	AllocatedStackBase;
	PVOID 	StackCommit;
	PVOID 	StackCommitMax;
	PVOID 	StackReserved;
} INITIAL_TEB, *PINITIAL_TEB;

#ifndef BASETYPES
#define BASETYPES

typedef unsigned long ULONG;
typedef ULONG *PULONG;
typedef unsigned short USHORT;
typedef USHORT *PUSHORT;
typedef unsigned char UCHAR;
typedef UCHAR *PUCHAR;
typedef unsigned char BYTE;
#endif  /* !BASETYPES */


#ifndef VOID
#define VOID void
typedef char CHAR;
typedef short SHORT;
typedef long LONG;
#if !defined(MIDL_PASS)
typedef int INT;
#endif
#endif
typedef unsigned long DWORD;
typedef DWORD ACCESS_MASK;
typedef __int64 LONGLONG;
typedef void * PVOID;
typedef unsigned short  WORD; // 2bytes

typedef unsigned __int64 ULONGLONG;

typedef enum _MEMORY_INFORMATION_CLASS
{
	MemoryBasicInformation
} MEMORY_INFORMATION_CLASS;

typedef enum _SYSTEM_INFORMATION_CLASS
{
	SystemBasicInformation = 0x0,
	SystemProcessorInformation = 0x1,
	SystemPerformanceInformation = 0x2,
	SystemTimeOfDayInformation = 0x3,
	SystemPathInformation = 0x4,
	SystemProcessInformation = 0x5,
	SystemCallCountInformation = 0x6,
	SystemDeviceInformation = 0x7,
	SystemProcessorPerformanceInformation = 0x8,
	SystemFlagsInformation = 0x9,
	SystemCallTimeInformation = 0xA,
	SystemModuleInformation = 0xB,
	SystemLocksInformation = 0xC,
	SystemStackTraceInformation = 0xD,
	SystemPagedPoolInformation = 0xE,
	SystemNonPagedPoolInformation = 0xF,
	SystemHandleInformation = 0x10,
	SystemObjectInformation = 0x11,
	SystemPageFileInformation = 0x12,
	SystemVdmInstemulInformation = 0x13,
	SystemVdmBopInformation = 0x14,
	SystemFileCacheInformation = 0x15,
	SystemPoolTagInformation = 0x16,
	SystemInterruptInformation = 0x17,
	SystemDpcBehaviorInformation = 0x18,
	SystemFullMemoryInformation = 0x19,
	SystemLoadGdiDriverInformation = 0x1A,
	SystemUnloadGdiDriverInformation = 0x1B,
	SystemTimeAdjustmentInformation = 0x1C,
	SystemSummaryMemoryInformation = 0x1D,
	SystemMirrorMemoryInformation = 0x1E,
	SystemPerformanceTraceInformation = 0x1F,
	SystemObsolete0 = 0x20,
	SystemExceptionInformation = 0x21,
	SystemCrashDumpStateInformation = 0x22,
	SystemKernelDebuggerInformation = 0x23,
	SystemContextSwitchInformation = 0x24,
	SystemRegistryQuotaInformation = 0x25,
	SystemExtendServiceTableInformation = 0x26,
	SystemPrioritySeperation = 0x27,
	SystemVerifierAddDriverInformation = 0x28,
	SystemVerifierRemoveDriverInformation = 0x29,
	SystemProcessorIdleInformation = 0x2A,
	SystemLegacyDriverInformation = 0x2B,
	SystemCurrentTimeZoneInformation = 0x2C,
	SystemLookasideInformation = 0x2D,
	SystemTimeSlipNotification = 0x2E,
	SystemSessionCreate = 0x2F,
	SystemSessionDetach = 0x30,
	SystemSessionInformation = 0x31,
	SystemRangeStartInformation = 0x32,
	SystemVerifierInformation = 0x33,
	SystemVerifierThunkExtend = 0x34,
	SystemSessionProcessInformation = 0x35,
	SystemLoadGdiDriverInSystemSpace = 0x36,
	SystemNumaProcessorMap = 0x37,
	SystemPrefetcherInformation = 0x38,
	SystemExtendedProcessInformation = 0x39,
	SystemRecommendedSharedDataAlignment = 0x3A,
	SystemComPlusPackage = 0x3B,
	SystemNumaAvailableMemory = 0x3C,
	SystemProcessorPowerInformation = 0x3D,
	SystemEmulationBasicInformation = 0x3E,
	SystemEmulationProcessorInformation = 0x3F,
	SystemExtendedHandleInformation = 0x40,
	SystemLostDelayedWriteInformation = 0x41,
	SystemBigPoolInformation = 0x42,
	SystemSessionPoolTagInformation = 0x43,
	SystemSessionMappedViewInformation = 0x44,
	SystemHotpatchInformation = 0x45,
	SystemObjectSecurityMode = 0x46,
	SystemWatchdogTimerHandler = 0x47,
	SystemWatchdogTimerInformation = 0x48,
	SystemLogicalProcessorInformation = 0x49,
	SystemWow64SharedInformationObsolete = 0x4A,
	SystemRegisterFirmwareTableInformationHandler = 0x4B,
	SystemFirmwareTableInformation = 0x4C,
	SystemModuleInformationEx = 0x4D,
	SystemVerifierTriageInformation = 0x4E,
	SystemSuperfetchInformation = 0x4F,
	SystemMemoryListInformation = 0x50,
	SystemFileCacheInformationEx = 0x51,
	SystemThreadPriorityClientIdInformation = 0x52,
	SystemProcessorIdleCycleTimeInformation = 0x53,
	SystemVerifierCancellationInformation = 0x54,
	SystemProcessorPowerInformationEx = 0x55,
	SystemRefTraceInformation = 0x56,
	SystemSpecialPoolInformation = 0x57,
	SystemProcessIdInformation = 0x58,
	SystemErrorPortInformation = 0x59,
	SystemBootEnvironmentInformation = 0x5A,
	SystemHypervisorInformation = 0x5B,
	SystemVerifierInformationEx = 0x5C,
	SystemTimeZoneInformation = 0x5D,
	SystemImageFileExecutionOptionsInformation = 0x5E,
	SystemCoverageInformation = 0x5F,
	SystemPrefetchPatchInformation = 0x60,
	SystemVerifierFaultsInformation = 0x61,
	SystemSystemPartitionInformation = 0x62,
	SystemSystemDiskInformation = 0x63,
	SystemProcessorPerformanceDistribution = 0x64,
	SystemNumaProximityNodeInformation = 0x65,
	SystemDynamicTimeZoneInformation = 0x66,
	SystemCodeIntegrityInformation = 0x67,
	SystemProcessorMicrocodeUpdateInformation = 0x68,
	SystemProcessorBrandString = 0x69,
	SystemVirtualAddressInformation = 0x6A,
	SystemLogicalProcessorAndGroupInformation = 0x6B,
	SystemProcessorCycleTimeInformation = 0x6C,
	SystemStoreInformation = 0x6D,
	SystemRegistryAppendString = 0x6E,
	SystemAitSamplingValue = 0x6F,
	SystemVhdBootInformation = 0x70,
	SystemCpuQuotaInformation = 0x71,
	SystemNativeBasicInformation = 0x72,
	SystemErrorPortTimeouts = 0x73,
	SystemLowPriorityIoInformation = 0x74,
	SystemBootEntropyInformation = 0x75,
	SystemVerifierCountersInformation = 0x76,
	SystemPagedPoolInformationEx = 0x77,
	SystemSystemPtesInformationEx = 0x78,
	SystemNodeDistanceInformation = 0x79,
	SystemAcpiAuditInformation = 0x7A,
	SystemBasicPerformanceInformation = 0x7B,
	SystemQueryPerformanceCounterInformation = 0x7C,
	SystemSessionBigPoolInformation = 0x7D,
	SystemBootGraphicsInformation = 0x7E,
	SystemScrubPhysicalMemoryInformation = 0x7F,
	SystemBadPageInformation = 0x80,
	SystemProcessorProfileControlArea = 0x81,
	SystemCombinePhysicalMemoryInformation = 0x82,
	SystemEntropyInterruptTimingInformation = 0x83,
	SystemConsoleInformation = 0x84,
	SystemPlatformBinaryInformation = 0x85,
	SystemPolicyInformation = 0x86,
	SystemHypervisorProcessorCountInformation = 0x87,
	SystemDeviceDataInformation = 0x88,
	SystemDeviceDataEnumerationInformation = 0x89,
	SystemMemoryTopologyInformation = 0x8A,
	SystemMemoryChannelInformation = 0x8B,
	SystemBootLogoInformation = 0x8C,
	SystemProcessorPerformanceInformationEx = 0x8D,
	SystemSpare0 = 0x8E,
	SystemSecureBootPolicyInformation = 0x8F,
	SystemPageFileInformationEx = 0x90,
	SystemSecureBootInformation = 0x91,
	SystemEntropyInterruptTimingRawInformation = 0x92,
	SystemPortableWorkspaceEfiLauncherInformation = 0x93,
	SystemFullProcessInformation = 0x94,
	SystemKernelDebuggerInformationEx = 0x95,
	SystemBootMetadataInformation = 0x96,
	SystemSoftRebootInformation = 0x97,
	SystemElamCertificateInformation = 0x98,
	SystemOfflineDumpConfigInformation = 0x99,
	SystemProcessorFeaturesInformation = 0x9A,
	SystemRegistryReconciliationInformation = 0x9B,
	SystemEdidInformation = 0x9C,
	SystemManufacturingInformation = 0x9D,
	SystemEnergyEstimationConfigInformation = 0x9E,
	SystemHypervisorDetailInformation = 0x9F,
	SystemProcessorCycleStatsInformation = 0xA0,
	SystemVmGenerationCountInformation = 0xA1,
	SystemTrustedPlatformModuleInformation = 0xA2,
	SystemKernelDebuggerFlags = 0xA3,
	SystemCodeIntegrityPolicyInformation = 0xA4,
	SystemIsolatedUserModeInformation = 0xA5,
	SystemHardwareSecurityTestInterfaceResultsInformation = 0xA6,
	SystemSingleModuleInformation = 0xA7,
	SystemAllowedCpuSetsInformation = 0xA8,
	SystemDmaProtectionInformation = 0xA9,
	SystemInterruptCpuSetsInformation = 0xAA,
	SystemSecureBootPolicyFullInformation = 0xAB,
	SystemCodeIntegrityPolicyFullInformation = 0xAC,
	SystemAffinitizedInterruptProcessorInformation = 0xAD,
	SystemRootSiloInformation = 0xAE,
	SystemCpuSetInformation = 0xAF,
	SystemCpuSetTagInformation = 0xB0,
	SystemWin32WerStartCallout = 0xB1,
	SystemSecureKernelProfileInformation = 0xB2,
	MaxSystemInfoClass = 0xB3,
} SYSTEM_INFORMATION_CLASS;

typedef enum _SYSTEM_VA_TYPE
{
	SystemVaTypeAll = 0x0,
	SystemVaTypeNonPagedPool = 0x1,
	SystemVaTypePagedPool = 0x2,
	SystemVaTypeSystemCache = 0x3,
	SystemVaTypeSystemPtes = 0x4,
	SystemVaTypeSessionSpace = 0x5,
	SystemVaTypeMax = 0x6,
} SYSTEM_VA_TYPE;

typedef struct _SYSTEM_EXCEPTION_INFORMATION
{
	unsigned int AlignmentFixupCount;
	unsigned int ExceptionDispatchCount;
	unsigned int FloatingEmulationCount;
	unsigned int ByteWordEmulationCount;
} SYSTEM_EXCEPTION_INFORMATION;


typedef struct _CLIENT_ID
{
	void *UniqueProcess;
	void *UniqueThread;
} CLIENT_ID, *PCLIENT_ID;

typedef struct _CLIENT_ID32
{
	ULONG UniqueProcess;
	ULONG UniqueThread;
} CLIENT_ID32, *PCLIENT_ID32;

typedef struct __declspec(align(8)) _SYSTEM_THREAD_INFORMATION
{
	LARGE_INTEGER KernelTime;
	LARGE_INTEGER UserTime;
	LARGE_INTEGER CreateTime;
	unsigned int WaitTime;
	void *StartAddress;
	CLIENT_ID ClientId;
	int Priority;
	int BasePriority;
	unsigned int ContextSwitches;
	unsigned int ThreadState;
	unsigned int WaitReason;
} SYSTEM_THREAD_INFORMATION, *PSYSTEM_THREAD_INFORMATION;

typedef struct _SYSTEM_ISOLATED_USER_MODE_INFORMATION
{
	__int8 SecureKernelRunning : 1;
	__int8 HvciEnabled : 1;
	__int8 HvciStrictMode : 1;
	__int8 DebugEnabled : 1;
	__int8 SpareFlags : 4;
	__int8 TrustletRunning : 1;
	__int8 SpareFlags2 : 7;
	char Spare0[6];
	unsigned __int64 Spare1;
} SYSTEM_ISOLATED_USER_MODE_INFORMATION, *PSYSTEM_ISOLATED_USER_MODE_INFORMATION;

typedef struct __declspec(align(8)) _SYSTEM_HANDLE_TABLE_ENTRY_INFO
{
	unsigned __int16 UniqueProcessId;
	unsigned __int16 CreatorBackTraceIndex;
	char ObjectTypeIndex;
	char HandleAttributes;
	unsigned __int16 HandleValue;
	void *Object;
	unsigned int GrantedAccess;
} SYSTEM_HANDLE_TABLE_ENTRY_INFO, *PSYSTEM_HANDLE_TABLE_ENTRY_INFO;

typedef struct _SYSTEM_HANDLE_INFORMATION
{
	unsigned int NumberOfHandles;
	SYSTEM_HANDLE_TABLE_ENTRY_INFO Handles[1];
} SYSTEM_HANDLE_INFORMATION, *PSYSTEM_HANDLE_INFORMATION;

typedef struct _RTL_PROCESS_MODULE_INFORMATION
{
	void *Section;
	void *MappedBase;
	void *ImageBase;
	unsigned int ImageSize;
	unsigned int Flags;
	unsigned __int16 LoadOrderIndex;
	unsigned __int16 InitOrderIndex;
	unsigned __int16 LoadCount;
	unsigned __int16 OffsetToFileName;
	char FullPathName[256];
} RTL_PROCESS_MODULE_INFORMATION, *PRTL_PROCESS_MODULE_INFORMATION;

typedef struct _RTL_PROCESS_MODULE_INFORMATION_EX
{
	unsigned __int16 NextOffset;
	RTL_PROCESS_MODULE_INFORMATION BaseInfo;
	unsigned int ImageChecksum;
	unsigned int TimeDateStamp;
	void *DefaultBase;
} RTL_PROCESS_MODULE_INFORMATION_EX, *PRTL_PROCESS_MODULE_INFORMATION_EX;

typedef struct _RTL_PROCESS_MODULES
{
	ULONG NumberOfModules;
	RTL_PROCESS_MODULE_INFORMATION Modules[1];
} RTL_PROCESS_MODULES, *PRTL_PROCESS_MODULES;

typedef struct _SYSTEM_SINGLE_MODULE_INFORMATION
{
	void *TargetModuleAddress;
	RTL_PROCESS_MODULE_INFORMATION_EX ExInfo;
} SYSTEM_SINGLE_MODULE_INFORMATION, *PSYSTEM_SINGLE_MODULE_INFORMATION;

typedef struct _SYSTEM_OBJECTTYPE_INFORMATION
{
	unsigned int NextEntryOffset;
	unsigned int NumberOfObjects;
	unsigned int NumberOfHandles;
	unsigned int TypeIndex;
	unsigned int InvalidAttributes;
	GENERIC_MAPPING GenericMapping;
	unsigned int ValidAccessMask;
	unsigned int PoolType;
	char SecurityRequired;
	char WaitableObject;
	UNICODE_STRING TypeName;
} SYSTEM_OBJECTTYPE_INFORMATION, *PSYSTEM_OBJECTTYPE_INFORMATION;

typedef struct __declspec(align(8)) _SYSTEM_BASIC_INFORMATION
{
	unsigned int Reserved;
	unsigned int TimerResolution;
	unsigned int PageSize;
	unsigned int NumberOfPhysicalPages;
	unsigned int LowestPhysicalPageNumber;
	unsigned int HighestPhysicalPageNumber;
	unsigned int AllocationGranularity;
	unsigned __int64 MinimumUserModeAddress;
	unsigned __int64 MaximumUserModeAddress;
	unsigned __int64 ActiveProcessorsAffinityMask;
	char NumberOfProcessors;
} SYSTEM_BASIC_INFORMATION, *PSYSTEM_BASIC_INFORMATION;


typedef struct _OBJECT_ATTRIBUTES
{
	unsigned int Length;
	void *RootDirectory;
	UNICODE_STRING *ObjectName;
	unsigned int Attributes;
	void *SecurityDescriptor;
	void *SecurityQualityOfService;
} OBJECT_ATTRIBUTES, *POBJECT_ATTRIBUTES;

typedef struct _SYSTEM_BOOT_ENVIRONMENT_INFORMATION
{
	GUID BootIdentifier;
	FIRMWARE_TYPE FirmwareType;
	unsigned __int64 BootFlags;
} SYSTEM_BOOT_ENVIRONMENT_INFORMATION, *PSYSTEM_BOOT_ENVIRONMENT_INFORMATION;

typedef struct _SYSTEM_LOOKASIDE_INFORMATION
{
	unsigned __int16 CurrentDepth;
	unsigned __int16 MaximumDepth;
	unsigned int TotalAllocates;
	unsigned int AllocateMisses;
	unsigned int TotalFrees;
	unsigned int FreeMisses;
	unsigned int Type;
	unsigned int Tag;
	unsigned int Size;
} SYSTEM_LOOKASIDE_INFORMATION, *PSYSTEM_LOOKASIDE_INFORMATION;

typedef struct _SYSTEM_PROCESSOR_INFORMATION
{
	unsigned __int16 ProcessorArchitecture;
	unsigned __int16 ProcessorLevel;
	unsigned __int16 ProcessorRevision;
	unsigned __int16 MaximumProcessors;
	unsigned int ProcessorFeatureBits;
} SYSTEM_PROCESSOR_INFORMATION, *PSYSTEM_PROCESSOR_INFORMATION;

typedef struct _SYSTEM_HANDLE_TABLE_ENTRY_INFO_EX
{
	void *Object;
	unsigned __int64 UniqueProcessId;
	unsigned __int64 HandleValue;
	unsigned int GrantedAccess;
	unsigned __int16 CreatorBackTraceIndex;
	unsigned __int16 ObjectTypeIndex;
	unsigned int HandleAttributes;
	unsigned int Reserved;
} SYSTEM_HANDLE_TABLE_ENTRY_INFO_EX, *PSYSTEM_HANDLE_TABLE_ENTRY_INFO_EX;

typedef struct _SYSTEM_HANDLE_INFORMATION_EX
{
	unsigned __int64 NumberOfHandles;
	unsigned __int64 Reserved;
	SYSTEM_HANDLE_TABLE_ENTRY_INFO_EX Handles[1];
} SYSTEM_HANDLE_INFORMATION_EX, *PSYSTEM_HANDLE_INFORMATION_EX;

typedef struct _SYSTEM_HANDLE
{
	ULONG ProcessId;
	BYTE ObjectTypeNumber;
	BYTE Flags;
	USHORT Handle;
	PVOID Object;
	ACCESS_MASK GrantedAccess;
} SYSTEM_HANDLE, *PSYSTEM_HANDLE;

typedef struct _SYSTEM_MANUFACTURING_INFORMATION
{
	unsigned int Options;
	UNICODE_STRING ProfileName;
} SYSTEM_MANUFACTURING_INFORMATION, *PSYSTEM_MANUFACTURING_INFORMATION;

typedef struct _OBJECT_NAME_INFORMATION
{
	UNICODE_STRING Name;
} OBJECT_NAME_INFORMATION, *POBJECT_NAME_INFORMATION;


typedef enum _OBJECT_INFORMATION_CLASS
{
	ObjectBasicInformation = 0x0,
	ObjectNameInformation = 0x1,
	ObjectTypeInformation = 0x2,
	ObjectTypesInformation = 0x3,
	ObjectHandleFlagInformation = 0x4,
	ObjectSessionInformation = 0x5,
	MaxObjectInfoClass = 0x6,
} OBJECT_INFORMATION_CLASS;

typedef struct _SYSTEM_PROCESS_INFORMATION
{
	unsigned int NextEntryOffset;
	unsigned int NumberOfThreads;
	LARGE_INTEGER WorkingSetPrivateSize;
	unsigned int HardFaultCount;
	unsigned int NumberOfThreadsHighWatermark;
	unsigned __int64 CycleTime;
	LARGE_INTEGER CreateTime;
	LARGE_INTEGER UserTime;
	LARGE_INTEGER KernelTime;
	UNICODE_STRING ImageName;
	int BasePriority;
	unsigned __int64 UniqueProcessId;
	void *InheritedFromUniqueProcessId;
	unsigned int HandleCount;
	unsigned int SessionId;
	unsigned __int64 UniqueProcessKey;
	unsigned __int64 PeakVirtualSize;
	unsigned __int64 VirtualSize;
	unsigned int PageFaultCount;
	unsigned __int64 PeakWorkingSetSize;
	unsigned __int64 WorkingSetSize;
	unsigned __int64 QuotaPeakPagedPoolUsage;
	unsigned __int64 QuotaPagedPoolUsage;
	unsigned __int64 QuotaPeakNonPagedPoolUsage;
	unsigned __int64 QuotaNonPagedPoolUsage;
	unsigned __int64 PagefileUsage;
	unsigned __int64 PeakPagefileUsage;
	unsigned __int64 PrivatePageCount;
	LARGE_INTEGER ReadOperationCount;
	LARGE_INTEGER WriteOperationCount;
	LARGE_INTEGER OtherOperationCount;
	LARGE_INTEGER ReadTransferCount;
	LARGE_INTEGER WriteTransferCount;
	LARGE_INTEGER OtherTransferCount;
} SYSTEM_PROCESS_INFORMATION, *PSYSTEM_PROCESS_INFORMATION;

typedef struct _SYSTEM_VERIFIER_INFORMATION
{
	unsigned int NextEntryOffset;
	unsigned int Level;
	UNICODE_STRING DriverName;
	unsigned int RaiseIrqls;
	unsigned int AcquireSpinLocks;
	unsigned int SynchronizeExecutions;
	unsigned int AllocationsAttempted;
	unsigned int AllocationsSucceeded;
	unsigned int AllocationsSucceededSpecialPool;
	unsigned int AllocationsWithNoTag;
	unsigned int TrimRequests;
	unsigned int Trims;
	unsigned int AllocationsFailed;
	unsigned int AllocationsFailedDeliberately;
	unsigned int Loads;
	unsigned int Unloads;
	unsigned int UnTrackedPool;
	unsigned int CurrentPagedPoolAllocations;
	unsigned int CurrentNonPagedPoolAllocations;
	unsigned int PeakPagedPoolAllocations;
	unsigned int PeakNonPagedPoolAllocations;
	unsigned __int64 PagedPoolUsageInBytes;
	unsigned __int64 NonPagedPoolUsageInBytes;
	unsigned __int64 PeakPagedPoolUsageInBytes;
	unsigned __int64 PeakNonPagedPoolUsageInBytes;
} SYSTEM_VERIFIER_INFORMATION, *PSYSTEM_VERIFIER_INFORMATION;

typedef struct _SYSTEM_PAGEFILE_INFORMATION
{
	unsigned int NextEntryOffset;
	unsigned int TotalSize;
	unsigned int TotalInUse;
	unsigned int PeakUsage;
	UNICODE_STRING PageFileName;
} SYSTEM_PAGEFILE_INFORMATION, *PSYSTEM_PAGEFILE_INFORMATION;

typedef struct _SYSTEM_KERNEL_DEBUGGER_INFORMATION
{
	char KernelDebuggerEnabled;
	char KernelDebuggerNotPresent;
} SYSTEM_KERNEL_DEBUGGER_INFORMATION, *PSYSTEM_KERNEL_DEBUGGER_INFORMATION;

typedef struct _SYSTEM_CODEINTEGRITY_INFORMATION
{
	unsigned int Length;
	unsigned int CodeIntegrityOptions;
} SYSTEM_CODEINTEGRITY_INFORMATION, *PSYSTEM_CODEINTEGRITY_INFORMATION;

typedef struct _SYSTEM_OBJECT_INFORMATION
{
	unsigned int NextEntryOffset;
	void *Object;
	void *CreatorUniqueProcess;
	unsigned __int16 CreatorBackTraceIndex;
	unsigned __int16 Flags;
	int PointerCount;
	int HandleCount;
	unsigned int PagedPoolCharge;
	unsigned int NonPagedPoolCharge;
	void *ExclusiveProcessId;
	void *SecurityDescriptor;
	OBJECT_NAME_INFORMATION NameInfo;
} SYSTEM_OBJECT_INFORMATION, *PSYSTEM_OBJECT_INFORMATION;

typedef struct _SYSTEM_VA_LIST_INFORMATION
{
	unsigned __int64 VirtualSize;
	unsigned __int64 VirtualPeak;
	unsigned __int64 VirtualLimit;
	unsigned __int64 AllocationFailures;
} SYSTEM_VA_LIST_INFORMATION, *PSYSTEM_VA_LIST_INFORMATION;


typedef struct _OBJECT_DIRECTORY_INFORMATION
{
	UNICODE_STRING Name;
	UNICODE_STRING TypeName;
} OBJECT_DIRECTORY_INFORMATION, *POBJECT_DIRECTORY_INFORMATION;


typedef struct _OBJECT_BASIC_INFORMATION
{
	unsigned int Attributes;
	unsigned int GrantedAccess;
	unsigned int HandleCount;
	unsigned int PointerCount;
	unsigned int PagedPoolCharge;
	unsigned int NonPagedPoolCharge;
	unsigned int Reserved[3];
	unsigned int NameInfoSize;
	unsigned int TypeInfoSize;
	unsigned int SecurityDescriptorSize;
	LARGE_INTEGER CreationTime;
} OBJECT_BASIC_INFORMATION, *POBJECT_BASIC_INFORMATION;

typedef struct _OBJECT_TYPE_INFORMATION
{
	UNICODE_STRING TypeName;
	unsigned int TotalNumberOfObjects;
	unsigned int TotalNumberOfHandles;
	unsigned int TotalPagedPoolUsage;
	unsigned int TotalNonPagedPoolUsage;
	unsigned int TotalNamePoolUsage;
	unsigned int TotalHandleTableUsage;
	unsigned int HighWaterNumberOfObjects;
	unsigned int HighWaterNumberOfHandles;
	unsigned int HighWaterPagedPoolUsage;
	unsigned int HighWaterNonPagedPoolUsage;
	unsigned int HighWaterNamePoolUsage;
	unsigned int HighWaterHandleTableUsage;
	unsigned int InvalidAttributes;
	GENERIC_MAPPING GenericMapping;
	unsigned int ValidAccessMask;
	char SecurityRequired;
	char MaintainHandleCount;
	char TypeIndex;
	char ReservedByte;
	unsigned int PoolType;
	unsigned int DefaultPagedPoolCharge;
	unsigned int DefaultNonPagedPoolCharge;
} OBJECT_TYPE_INFORMATION, *POBJECT_TYPE_INFORMATION;

typedef struct _OBJECT_TYPES_INFORMATION
{
	unsigned int NumberOfTypes;
} OBJECT_TYPES_INFORMATION, *POBJECT_TYPES_INFORMATION;

typedef struct _OBJECT_HANDLE_FLAG_INFORMATION
{
	char Inherit;
	char ProtectFromClose;
} OBJECT_HANDLE_FLAG_INFORMATION, *POBJECT_HANDLE_FLAG_INFORMATION;

typedef enum _PROCESSINFOCLASS
{
	ProcessBasicInformation = 0x0,
	ProcessQuotaLimits = 0x1,
	ProcessIoCounters = 0x2,
	ProcessVmCounters = 0x3,
	ProcessTimes = 0x4,
	ProcessBasePriority = 0x5,
	ProcessRaisePriority = 0x6,
	ProcessDebugPort = 0x7,
	ProcessExceptionPort = 0x8,
	ProcessAccessToken = 0x9,
	ProcessLdtInformation = 0xA,
	ProcessLdtSize = 0xB,
	ProcessDefaultHardErrorMode = 0xC,
	ProcessIoPortHandlers = 0xD,
	ProcessPooledUsageAndLimits = 0xE,
	ProcessWorkingSetWatch = 0xF,
	ProcessUserModeIOPL = 0x10,
	ProcessEnableAlignmentFaultFixup = 0x11,
	ProcessPriorityClass = 0x12,
	ProcessWx86Information = 0x13,
	ProcessHandleCount = 0x14,
	ProcessAffinityMask = 0x15,
	ProcessPriorityBoost = 0x16,
	ProcessDeviceMap = 0x17,
	ProcessSessionInformation = 0x18,
	ProcessForegroundInformation = 0x19,
	ProcessWow64Information = 0x1A,
	ProcessImageFileName = 0x1B,
	ProcessLUIDDeviceMapsEnabled = 0x1C,
	ProcessBreakOnTermination = 0x1D,
	ProcessDebugObjectHandle = 0x1E,
	ProcessDebugFlags = 0x1F,
	ProcessHandleTracing = 0x20,
	ProcessIoPriority = 0x21,
	ProcessExecuteFlags = 0x22,
	ProcessTlsInformation = 0x23,
	ProcessCookie = 0x24,
	ProcessImageInformation = 0x25,
	ProcessCycleTime = 0x26,
	ProcessPagePriority = 0x27,
	ProcessInstrumentationCallback = 0x28,
	ProcessThreadStackAllocation = 0x29,
	ProcessWorkingSetWatchEx = 0x2A,
	ProcessImageFileNameWin32 = 0x2B,
	ProcessImageFileMapping = 0x2C,
	ProcessAffinityUpdateMode = 0x2D,
	ProcessMemoryAllocationMode = 0x2E,
	ProcessGroupInformation = 0x2F,
	ProcessTokenVirtualizationEnabled = 0x30,
	ProcessOwnerInformation = 0x31,
	ProcessWindowInformation = 0x32,
	ProcessHandleInformation = 0x33,
	ProcessMitigationPolicy = 0x34,
	ProcessDynamicFunctionTableInformation = 0x35,
	ProcessHandleCheckingMode = 0x36,
	ProcessKeepAliveCount = 0x37,
	ProcessRevokeFileHandles = 0x38,
	ProcessWorkingSetControl = 0x39,
	ProcessHandleTable = 0x3A,
	ProcessCheckStackExtentsMode = 0x3B,
	ProcessCommandLineInformation = 0x3C,
	ProcessProtectionInformation = 0x3D,
	ProcessMemoryExhaustion = 0x3E,
	ProcessFaultInformation = 0x3F,
	ProcessTelemetryIdInformation = 0x40,
	ProcessCommitReleaseInformation = 0x41,
	ProcessDefaultCpuSetsInformation = 0x42,
	ProcessAllowedCpuSetsInformation = 0x43,
	ProcessReserved1Information = 0x42,
	ProcessReserved2Information = 0x43,
	ProcessSubsystemProcess = 0x44,
	ProcessJobMemoryInformation = 0x45,
	ProcessInPrivate = 0x46,
	ProcessRaiseUMExceptionOnInvalidHandleClose = 0x47,
	MaxProcessInfoClass = 0x48,
} PROCESSINFOCLASS;


typedef enum _PROCESS_ENERGY_COMPONENT_TYPE_NUM
{
	PsEnergyComponentTypeCpu = 0x0,
	PsEnergyComponentTypeStorage = 0x1,
	PsEnergyComponentTypeNetwork = 0x2,
	PsEnergyComponentTypeMBB = 0x3,
	PsEnergyComponentTypeForegroundTime = 0x4,
	PsEnergyComponentTypePixelTime = 0x5,
	PsEnergyComponentTypeMax = 0x6,
} PROCESS_ENERGY_COMPONENT_TYPE_NUM;

typedef enum _PROCESS_WORKING_SET_OPERATION
{
	ProcessWorkingSetSwap = 0x0,
	ProcessWorkingSetEmpty = 0x1,
	ProcessWorkingSetOperationMax = 0x2,
} PROCESS_WORKING_SET_OPERATION;

typedef enum _PROCESS_TLS_INFORMATION_TYPE
{
	ProcessTlsReplaceIndex = 0x0,
	ProcessTlsReplaceVector = 0x1,
	MaxProcessTlsOperation = 0x2,
} PROCESS_TLS_INFORMATION_TYPE;



typedef struct _PROCESS_HANDLE_TABLE_ENTRY_INFO
{
	void *HandleValue;
	unsigned __int64 HandleCount;
	unsigned __int64 PointerCount;
	unsigned int GrantedAccess;
	unsigned int ObjectTypeIndex;
	unsigned int HandleAttributes;
	unsigned int Reserved;
} PROCESS_HANDLE_TABLE_ENTRY_INFO, *PPROCESS_HANDLE_TABLE_ENTRY_INFO;


typedef struct _PROCESS_HANDLE_SNAPSHOT_INFORMATION
{
	unsigned __int64 NumberOfHandles;
	unsigned __int64 Reserved;
	PROCESS_HANDLE_TABLE_ENTRY_INFO Handles[1];
} PROCESS_HANDLE_SNAPSHOT_INFORMATION, *PPROCESS_HANDLE_SNAPSHOT_INFORMATION;

typedef struct _PROCESS_STACK_ALLOCATION_INFORMATION
{
	unsigned __int64 ReserveSize;
	unsigned __int64 ZeroBits;
	void *StackBase;
} PROCESS_STACK_ALLOCATION_INFORMATION, *PPROCESS_STACK_ALLOCATION_INFORMATION;

typedef struct _PROCESS_HANDLE_INFORMATION
{
	unsigned int HandleCount;
	unsigned int HandleCountHighWatermark;
} PROCESS_HANDLE_INFORMATION, *PPROCESS_HANDLE_INFORMATION;

typedef struct _PROCESS_ACCESS_TOKEN
{
	void *Token;
	void *Thread;
} PROCESS_ACCESS_TOKEN, *PPROCESS_ACCESS_TOKEN;

typedef struct _PEB_LDR_DATA
{
	unsigned int Length;
	char Initialized;
	void *SsHandle;
	LIST_ENTRY InLoadOrderModuleList;
	LIST_ENTRY InMemoryOrderModuleList;
	LIST_ENTRY InInitializationOrderModuleList;
	void *EntryInProgress;
	char ShutdownInProgress;
	void *ShutdownThreadId;
} PEB_LDR_DATA, *PPEB_LDR_DATA;

typedef struct _PEB_LDR_DATA32
{
	ULONG Length;
	UCHAR Initialized;
	ULONG SsHandle;
	LIST_ENTRY32 InLoadOrderModuleList;
	LIST_ENTRY32 InMemoryOrderModuleList;
	LIST_ENTRY32 InInitializationOrderModuleList;
	ULONG EntryInProgress;
	UCHAR ShutdownInProgress;
	ULONG ShutdownThreadId;
} PEB_LDR_DATA32, *PPEB_LDR_DATA32;

typedef struct _STRING
{
	unsigned __int16 Length;
	unsigned __int16 MaximumLength;
	char *Buffer;
} STRING, *PSTRING;

typedef struct _RTL_DRIVE_LETTER_CURDIR
{
	unsigned __int16 Flags;
	unsigned __int16 Length;
	unsigned int TimeStamp;
	STRING DosPath;
} RTL_DRIVE_LETTER_CURDIR, *PRTL_DRIVE_LETTER_CURDIR;

typedef struct _CURDIR
{
	UNICODE_STRING DosPath;
	void *Handle;
} CURDIR, *PCURDIR;

typedef struct _RTL_USER_PROCESS_PARAMETERS
{
	unsigned int MaximumLength;
	unsigned int Length;
	unsigned int Flags;
	unsigned int DebugFlags;
	void *ConsoleHandle;
	unsigned int ConsoleFlags;
	void *StandardInput;
	void *StandardOutput;
	void *StandardError;
	CURDIR CurrentDirectory;
	UNICODE_STRING DllPath;
	UNICODE_STRING ImagePathName;
	UNICODE_STRING CommandLine;
	void *Environment;
	unsigned int StartingX;
	unsigned int StartingY;
	unsigned int CountX;
	unsigned int CountY;
	unsigned int CountCharsX;
	unsigned int CountCharsY;
	unsigned int FillAttribute;
	unsigned int WindowFlags;
	unsigned int ShowWindowFlags;
	UNICODE_STRING WindowTitle;
	UNICODE_STRING DesktopInfo;
	UNICODE_STRING ShellInfo;
	UNICODE_STRING RuntimeData;
	RTL_DRIVE_LETTER_CURDIR CurrentDirectores[32];
	unsigned __int64 EnvironmentSize;
	unsigned __int64 EnvironmentVersion;
	void *PackageDependencyData;
	unsigned int ProcessGroupId;
	unsigned int LoaderThreads;
} RTL_USER_PROCESS_PARAMETERS, *PRTL_USER_PROCESS_PARAMETERS;


typedef enum _LDR_DLL_LOAD_REASON
{
	LoadReasonStaticDependency = 0x0,
	LoadReasonStaticForwarderDependency = 0x1,
	LoadReasonDynamicForwarderDependency = 0x2,
	LoadReasonDelayloadDependency = 0x3,
	LoadReasonDynamicLoad = 0x4,
	LoadReasonAsImageLoad = 0x5,
	LoadReasonAsDataLoad = 0x6,
	LoadReasonUnknown = 0xFFFFFFFF,
} LDR_DLL_LOAD_REASON;


typedef struct _LDRP_CSLIST
{
	SINGLE_LIST_ENTRY *Tail;
} LDRP_CSLIST, *PLDRP_CSLIST;

enum _LDR_DDAG_STATE
{
	LdrModulesMerged = 0xFFFFFFFB,
	LdrModulesInitError = 0xFFFFFFFC,
	LdrModulesSnapError = 0xFFFFFFFD,
	LdrModulesUnloaded = 0xFFFFFFFE,
	LdrModulesUnloading = 0xFFFFFFFF,
	LdrModulesPlaceHolder = 0x0,
	LdrModulesMapping = 0x1,
	LdrModulesMapped = 0x2,
	LdrModulesWaitingForDependencies = 0x3,
	LdrModulesSnapping = 0x4,
	LdrModulesSnapped = 0x5,
	LdrModulesCondensed = 0x6,
	LdrModulesReadyToInit = 0x7,
	LdrModulesInitializing = 0x8,
	LdrModulesReadyToRun = 0x9,
};

typedef struct __declspec(align(8)) _LDR_SERVICE_TAG_RECORD
{
	void *Next;
	unsigned int ServiceTag;
} LDR_SERVICE_TAG_RECORD, *PLDR_SERVICE_TAG_RECORD;

typedef struct __declspec(align(8)) _LDR_DDAG_NODE
{
	LIST_ENTRY Modules;
	void *ServiceTagList;
	unsigned int LoadCount;
	unsigned int LoadWhileUnloadingCount;
	unsigned int LowestLink;
	LDRP_CSLIST Dependencies;
	LDRP_CSLIST IncomingDependencies;
	unsigned int State;
	SINGLE_LIST_ENTRY CondenseLink;
	unsigned int PreorderNumber;
} LDR_DDAG_NODE, *PLDR_DDAG_NODE;

typedef struct _LDR_DATA_TABLE_ENTRY
{
	LIST_ENTRY InLoadOrderLinks;
	LIST_ENTRY InMemoryOrderLinks;
	LIST_ENTRY InInitializationOrderLinks;
	void *DllBase;
	void *EntryPoint;
	unsigned int SizeOfImage;
	UNICODE_STRING FullDllName;
	UNICODE_STRING BaseDllName;
	union
	{
		char FlagGroup[4];
		unsigned int Flags;
		struct
		{
			unsigned __int32 PackagedBinary : 1;
			unsigned __int32 MarkedForRemoval : 1;
			unsigned __int32 ImageDll : 1;
			unsigned __int32 LoadNotificationsSent : 1;
			unsigned __int32 TelemetryEntryProcessed : 1;
			unsigned __int32 ProcessStaticImport : 1;
			unsigned __int32 InLegacyLists : 1;
			unsigned __int32 InIndexes : 1;
			unsigned __int32 ShimDll : 1;
			unsigned __int32 InExceptionTable : 1;
			unsigned __int32 ReservedFlags1 : 2;
			unsigned __int32 LoadInProgress : 1;
			unsigned __int32 LoadConfigProcessed : 1;
			unsigned __int32 EntryProcessed : 1;
			unsigned __int32 ProtectDelayLoad : 1;
			unsigned __int32 ReservedFlags3 : 2;
			unsigned __int32 DontCallForThreads : 1;
			unsigned __int32 ProcessAttachCalled : 1;
			unsigned __int32 ProcessAttachFailed : 1;
			unsigned __int32 CorDeferredValidate : 1;
			unsigned __int32 CorImage : 1;
			unsigned __int32 DontRelocate : 1;
			unsigned __int32 CorILOnly : 1;
			unsigned __int32 ReservedFlags5 : 3;
			unsigned __int32 Redirected : 1;
			unsigned __int32 ReservedFlags6 : 2;
			unsigned __int32 CompatDatabaseProcessed : 1;
		};
	};
	unsigned __int16 ObsoleteLoadCount;
	unsigned __int16 TlsIndex;
	LIST_ENTRY HashLinks;
	unsigned int TimeDateStamp;
	void *EntryPointActivationContext;
	void *Lock;
	void *DdagNode;
	LIST_ENTRY NodeModuleLink;
	void *LoadContext;
	void *ParentDllBase;
	void *SwitchBackContext;
	RTL_BALANCED_NODE BaseAddressIndexNode;
	RTL_BALANCED_NODE MappingInfoIndexNode;
	unsigned __int64 OriginalBase;
	LARGE_INTEGER LoadTime;
	unsigned int BaseNameHashValue;
	LDR_DLL_LOAD_REASON LoadReason;
	unsigned int ImplicitPathOptions;
	unsigned int ReferenceCount;
} LDR_DATA_TABLE_ENTRY, *PLDR_DATA_TABLE_ENTRY;

typedef struct _PEB
{
	char InheritedAddressSpace;
	char ReadImageFileExecOptions;
	char BeingDebugged;

	union
	{
		char BitField;
		struct
		{
			__int8 ImageUsesLargePages : 1;
			__int8 IsProtectedProcess : 1;
			__int8 IsImageDynamicallyRelocated : 1;
			__int8 SkipPatchingUser32Forwarders : 1;
			__int8 IsPackagedProcess : 1;
			__int8 IsAppContainer : 1;
			__int8 IsProtectedProcessLight : 1;
			__int8 SpareBits : 1;
		};
	};

#ifdef _M_AMD64
	char Padding0[4];
#elif _M_IX86
#endif

	void *Mutant;
	void *ImageBaseAddress;
	PEB_LDR_DATA *Ldr;
	RTL_USER_PROCESS_PARAMETERS *ProcessParameters;
	void *SubSystemData;
	void *ProcessHeap;
	RTL_CRITICAL_SECTION *FastPebLock;
	void *AtlThunkSListPtr;
	void *IFEOKey;

	union
	{
		unsigned int CrossProcessFlags;
		struct
		{
			unsigned __int32 ProcessInJob : 1;
			unsigned __int32 ProcessInitializing : 1;
			unsigned __int32 ProcessUsingVEH : 1;
			unsigned __int32 ProcessUsingVCH : 1;
			unsigned __int32 ProcessUsingFTH : 1;
			unsigned __int32 ReservedBits0 : 27;
		};
	};

	char Padding1[4];

	union
	{
		void *KernelCallbackTable;
		void *UserSharedInfoPtr;
	};

	unsigned int SystemReserved[1];
	unsigned int AtlThunkSListPtr32;
	void *ApiSetMap;
	unsigned int TlsExpansionCounter;
	char Padding2[4];
	void *TlsBitmap;
	unsigned int TlsBitmapBits[2];
	void *ReadOnlySharedMemoryBase;
	void *SparePvoid0;
	void **ReadOnlyStaticServerData;
	void *AnsiCodePageData;
	void *OemCodePageData;
	void *UnicodeCaseTableData;
	unsigned int NumberOfProcessors;
	unsigned int NtGlobalFlag;
	LARGE_INTEGER CriticalSectionTimeout;
	unsigned __int64 HeapSegmentReserve;
	unsigned __int64 HeapSegmentCommit;
	unsigned __int64 HeapDeCommitTotalFreeThreshold;
	unsigned __int64 HeapDeCommitFreeBlockThreshold;
	unsigned int NumberOfHeaps;
	unsigned int MaximumNumberOfHeaps;
	void **ProcessHeaps;
	void *GdiSharedHandleTable;
	void *ProcessStarterHelper;
	unsigned int GdiDCAttributeList;
	char Padding3[4];
	RTL_CRITICAL_SECTION *LoaderLock;
	unsigned int OSMajorVersion;
	unsigned int OSMinorVersion;
	unsigned __int16 OSBuildNumber;
	unsigned __int16 OSCSDVersion;
	unsigned int OSPlatformId;
	unsigned int ImageSubsystem;
	unsigned int ImageSubsystemMajorVersion;
	unsigned int ImageSubsystemMinorVersion;
	char Padding4[4];
	unsigned __int64 ActiveProcessAffinityMask;
	unsigned int GdiHandleBuffer[60];
	void(__cdecl *PostProcessInitRoutine)();
	void *TlsExpansionBitmap;
	unsigned int TlsExpansionBitmapBits[32];
	unsigned int SessionId;
	char Padding5[4];
	ULARGE_INTEGER AppCompatFlags;
	ULARGE_INTEGER AppCompatFlagsUser;
	void *pShimData;
	void *AppCompatInfo;
	UNICODE_STRING CSDVersion;
	void *ActivationContextData;
	void *ProcessAssemblyStorageMap;
	void *SystemDefaultActivationContextData;
	void *SystemAssemblyStorageMap;
	unsigned __int64 MinimumStackCommit;
	void *FlsCallback;
	LIST_ENTRY FlsListHead;
	void *FlsBitmap;
	unsigned int FlsBitmapBits[4];
	unsigned int FlsHighIndex;
	void *WerRegistrationData;
	void *WerShipAssertPtr;
	void *pUnused;
	void *pImageHeaderHash;

	union
	{
		unsigned int TracingFlags;
		struct
		{
			unsigned __int32 HeapTracingEnabled : 1;
			unsigned __int32 CritSecTracingEnabled : 1;
			unsigned __int32 LibLoaderTracingEnabled : 1;
			unsigned __int32 SpareTracingBits : 29;
		};
	};

	char Padding6[4];
	unsigned __int64 CsrServerReadOnlySharedMemoryBase;
	unsigned __int64 TppWorkerpListLock;
	LIST_ENTRY TppWorkerpList;
	void *WaitOnAddressHashTable[128];
} PEB, *PPEB;

typedef struct _PROCESS_BASIC_INFORMATION
{
	int ExitStatus;
	PEB *PebBaseAddress;
	unsigned __int64 AffinityMask;
	int BasePriority;
	unsigned __int64 UniqueProcessId;
	unsigned __int64 InheritedFromUniqueProcessId;
} PROCESS_BASIC_INFORMATION, *PPROCESS_BASIC_INFORMATION;

typedef struct _PROCESS_BASIC_INFORMATION64
{
	int ExitStatus;
	unsigned int Pad1;
	unsigned __int64 PebBaseAddress;
	unsigned __int64 AffinityMask;
	int BasePriority;
	unsigned int Pad2;
	unsigned __int64 UniqueProcessId;
	unsigned __int64 InheritedFromUniqueProcessId;
} PROCESS_BASIC_INFORMATION64, *PPROCESS_BASIC_INFORMATION64;

typedef struct _PROCESS_INSTRUMENTATION_CALLBACK_INFORMATION
{
	unsigned int Version;
	unsigned int Reserved;
	unsigned __int64 Callback;
} PROCESS_INSTRUMENTATION_CALLBACK_INFORMATION, *PPROCESS_INSTRUMENTATION_CALLBACK_INFORMATION;

typedef enum _SECTION_INFORMATION_CLASS
{
	SectionBasicInformation = 0x0,
	SectionImageInformation = 0x1,
	SectionRelocationInformation = 0x2,
	MaxSectionInfoClass = 0x3,
} SECTION_INFORMATION_CLASS;

typedef struct _SECTIONBASICINFO
{
	void *BaseAddress;
	unsigned int AllocationAttributes;
	LARGE_INTEGER MaximumSize;
} SECTIONBASICINFO;

typedef struct _SECTION_IMAGE_INFORMATION
{
	void *TransferAddress;
	unsigned int ZeroBits;
	unsigned __int64 MaximumStackSize;
	unsigned __int64 CommittedStackSize;
	unsigned int SubSystemType;

	union
	{
		struct
		{
			unsigned __int16 SubSystemMinorVersion;
			unsigned __int16 SubSystemMajorVersion;
		};

		unsigned int SubSystemVersion;
	};

	union
	{
		struct
		{
			unsigned __int16 MajorOperatingSystemVersion;
			unsigned __int16 MinorOperatingSystemVersion;
		};
		unsigned int OperatingSystemVersion;
	};

	unsigned __int16 ImageCharacteristics;
	unsigned __int16 DllCharacteristics;
	unsigned __int16 Machine;
	char ImageContainsCode;

	union
	{
		char ImageFlags;
		struct
		{
			__int8 ComPlusNativeReady : 1;
			__int8 ComPlusILOnly : 1;
			__int8 ImageDynamicallyRelocated : 1;
			__int8 ImageMappedFlat : 1;
			__int8 BaseBelow4gb : 1;
			__int8 ComPlusPrefer32bit : 1;
			__int8 Reserved : 2;
		};
	};

	unsigned int LoaderFlags;
	unsigned int ImageFileSize;
	unsigned int CheckSum;
} SECTION_IMAGE_INFORMATION, *PSECTION_IMAGE_INFORMATION;

typedef struct _IO_STATUS_BLOCK
{
	union
	{
		int Status;
		void *Pointer;
	};
	unsigned __int64 Information;
} IO_STATUS_BLOCK, *PIO_STATUS_BLOCK;

typedef enum _THREADINFOCLASS {
	ThreadPagePriority
} THREADINFOCLASS;

typedef enum _SECTION_INHERIT {
	ViewShare,
	ViewUnmap
} SECTION_INHERIT;

typedef enum _VIRTUAL_MEMORY_INFORMATION_CLASS
{
	VmPrefetchInformation,
	VmPagePriorityInformation,
	VmCfgCallTargetInformation
} VIRTUAL_MEMORY_INFORMATION_CLASS;

typedef struct _MEMORY_RANGE_ENTRY
{
	PVOID VirtualAddress;
	SIZE_T NumberOfBytes;
} MEMORY_RANGE_ENTRY, *PMEMORY_RANGE_ENTRY;

typedef struct _RTL_USER_PROCESS_PARAMETERS32
{
	ULONG MaximumLength;
	ULONG Length;
	ULONG Flags;
	ULONG DebugFlags;
	ULONG ConsoleHandle;
	ULONG ConsoleFlags;
	ULONG StandardInput;
	ULONG StandardOutput;
	ULONG StandardError;
	CURDIR CurrentDirectory;
	UNICODE_STRING DllPath;
	UNICODE_STRING ImagePathName;
	UNICODE_STRING CommandLine;
	ULONG Environment;
	ULONG StartingX;
	ULONG StartingY;
	ULONG CountX;
	ULONG CountY;
	ULONG CountCharsX;
	ULONG CountCharsY;
	ULONG FillAttribute;
	ULONG WindowFlags;
	ULONG ShowWindowFlags;
	UNICODE_STRING WindowTitle;
	UNICODE_STRING DesktopInfo;
	UNICODE_STRING ShellInfo;
	UNICODE_STRING RuntimeData;
	RTL_DRIVE_LETTER_CURDIR CurrentDirectores[32];
	ULONG EnvironmentSize;
	ULONG EnvironmentVersion;
	ULONG PackageDependencyData;
	ULONG ProcessGroupId;
	ULONG LoaderThreads;
} RTL_USER_PROCESS_PARAMETERS32, *PRTL_USER_PROCESS_PARAMETERS32;

typedef struct _PEB32
{
	UCHAR InheritedAddressSpace;
	UCHAR ReadImageFileExecOptions;
	UCHAR BeingDebugged;
	union {
		UCHAR BitField;
		struct {
			UCHAR ImageUsesLargePages : 1;
			UCHAR IsProtectedProcess : 1;
			UCHAR IsImageDynamicallyRelocated : 1;
			UCHAR SkipPatchingUser32Forwarders : 1;
			UCHAR IsPackagedProcess : 1;
			UCHAR IsAppContainer : 1;
			UCHAR IsProtectedProcessLight : 1;
			UCHAR IsLongPathAwareProcess : 1;
		};
	};
	ULONG Mutant;
	ULONG ImageBaseAddress;
	ULONG Ldr;
	ULONG ProcessParameters;
	ULONG SubSystemData;
	ULONG ProcessHeap;
	ULONG FastPebLock;
	ULONG AtlThunkSListPtr;
	ULONG IFEOKey;
	union {
		ULONG CrossProcessFlags;
		struct {
			UCHAR ProcessInJob : 1;
			UCHAR ProcessInitializing : 1;
			UCHAR ProcessUsingVEH : 1;
			UCHAR ProcessUsingVCH : 1;
			UCHAR ProcessUsingFTH : 1;
			UCHAR ProcessPreviouslyThrottled : 1;
			UCHAR ProcessCurrentlyThrottled : 1;
			ULONG ReservedBits0 : 25;
		};
	};
	union {
		ULONG KernelCallbackTable;
		ULONG UserSharedInfoPtr;
	};
	ULONG SystemReserved[1];
	ULONG AtlThunkSListPtr32;
	ULONG ApiSetMap;
	ULONG TlsExpansionCounter;
	ULONG TlsBitmap;
	ULONG TlsBitmapBits[2];
	ULONG ReadOnlySharedMemoryBase;
	ULONG SharedData;
	ULONG ReadOnlyStaticServerData;
	ULONG AnsiCodePageData;
	ULONG OemCodePageData;
	ULONG UnicodeCaseTableData;
	ULONG NumberOfProcessors;
	ULONG NtGlobalFlag;
	LARGE_INTEGER CriticalSectionTimeout;
	ULONG HeapSegmentReserve;
	ULONG HeapSegmentCommit;
	ULONG HeapDeCommitTotalFreeThreshold;
	ULONG HeapDeCommitFreeBlockThreshold;
	ULONG NumberOfHeaps;
	ULONG MaximumNumberOfHeaps;
	ULONG ProcessHeaps;
	ULONG GdiSharedHandleTable;
	ULONG ProcessStarterHelper;
	ULONG GdiDCAttributeList;
	ULONG LoaderLock;
	ULONG OSMajorVersion;
	ULONG OSMinorVersion;
	USHORT OSBuildNumber;
	USHORT OSCSDVersion;
	ULONG OSPlatformId;
	ULONG ImageSubsystem;
	ULONG ImageSubsystemMajorVersion;
	ULONG ImageSubsystemMinorVersion;
	ULONG ActiveProcessAffinityMask;
	ULONG GdiHandleBuffer[34];
	ULONG PostProcessInitRoutine;
	ULONG TlsExpansionBitmap;
	ULONG TlsExpansionBitmapBits[32];
	ULONG SessionId;
	ULARGE_INTEGER AppCompatFlags;
	ULARGE_INTEGER AppCompatFlagsUser;
	ULONG pShimData;
	ULONG AppCompatInfo;
	UNICODE_STRING CSDVersion;
	ULONG ActivationContextData;
	ULONG ProcessAssemblyStorageMap;
	ULONG SystemDefaultActivationContextData;
	ULONG SystemAssemblyStorageMap;
	ULONG MinimumStackCommit;
	ULONG FlsCallback;
	LIST_ENTRY FlsListHead;
	ULONG FlsBitmap;
	ULONG FlsBitmapBits[4];
	ULONG FlsHighIndex;
	ULONG WerRegistrationData;
	ULONG WerShipAssertPtr;
	ULONG pUnused;
	ULONG pImageHeaderHash;
	union {
		ULONG TracingFlags;
		struct {
			UCHAR HeapTracingEnabled : 1;
			UCHAR CritSecTracingEnabled : 1;
			UCHAR LibLoaderTracingEnabled : 1;
			ULONG SpareTracingBits : 29;
		};
	};
	ULONGLONG CsrServerReadOnlySharedMemoryBase;
	ULONG TppWorkerpListLock;
	LIST_ENTRY TppWorkerpList;
	ULONG WaitOnAddressHashTable[128];
} PEB32, *PPEB32;

typedef struct _UNICODE_STRING32
{
	USHORT Length;
	USHORT MaximumLength;
	ULONG Buffer;
} UNICODE_STRING32, *PUNICODE_STRING32;

typedef struct _LDR_DATA_TABLE_ENTRY32
{
	LIST_ENTRY32 InLoadOrderLinks;
	LIST_ENTRY32 InMemoryOrderLinks;
	LIST_ENTRY32 InInitializationOrderLinks;
	ULONG DllBase;
	ULONG EntryPoint;
	ULONG SizeOfImage;
	UNICODE_STRING32 FullDllName;
	UNICODE_STRING32 BaseDllName;
} LDR_DATA_TABLE_ENTRY32, *PLDR_DATA_TABLE_ENTRY32;



typedef struct _THREAD_BASIC_INFORMATION
{
	LONG ExitStatus;
	PVOID TebBaseAddress;
	CLIENT_ID ClientID;
	KAFFINITY AffinityMask;
	LONG Priority;
	LONG BasePriority;
}THREAD_BASIC_INFORMATION, *PTHREAD_BASIC_INFORMATION;

typedef struct _RAW_SMBIOS_DATA
{
	BYTE    Used20CallingMethod;
	BYTE    SMBIOSMajorVersion;
	BYTE    SMBIOSMinorVersion;
	BYTE    DmiRevision;
	DWORD   Length;
	BYTE    *SMBIOSTableData;
} RAW_SMBIOS_DATA, *PRAW_SMBIOS_DATA;

typedef struct _SMBIOS_STRUCT_HEADER
{
	UCHAR Type;
	UCHAR Length;
	USHORT Handle;
} SMBIOS_STRUCT_HEADER, *PSMBIOS_STRUCT_HEADER;

#define SMBIOS_BIOS_INFORMATION_TYPE 0
typedef struct _SMBIOS_BIOS_INFORMATION_STRUCT
{
	SMBIOS_STRUCT_HEADER Header;

	UCHAR       Vendor;
	UCHAR       Version;
	USHORT      StartingAddressSegment;
	UCHAR       ReleaseDate;
	UCHAR       RomSize;
	UCHAR       Characteristics[8];
	UCHAR       CharacteristicsExtension0;
	UCHAR       CharacteristicsExtension1;
	UCHAR       SystemBiosMajorRelease;     // SMBIOS 2.3.6+
	UCHAR       SystemBiosMinorRelease;     // SMBIOS 2.3.6+
	UCHAR       ECFirmwareMajorRelease;     // SMBIOS 2.3.6+
	UCHAR       ECFirmwareMinorRelease;     // SMBIOS 2.3.6+
} SMBIOS_BIOS_INFORMATION_STRUCT, *PSMBIOS_BIOS_INFORMATION_STRUCT;

#define SMBIOS_BASE_BOARD_INFORMATION_TYPE 2
typedef struct _SMBIOS_BASE_BOARD_INFORMATION_STRUCT
{
	SMBIOS_STRUCT_HEADER Header;

	UCHAR       Manufacturer;
	UCHAR       Product;
	UCHAR       Version;
	UCHAR       SerialNumber;
	UCHAR       AssetTagNumber;
	UCHAR       FeatureFlags;
	UCHAR       Location;
	USHORT      ChassisHandle;
	UCHAR       BoardType;
	UCHAR       ObjectHandles;
} SMBIOS_BASE_BOARD_INFORMATION_STRUCT, *PSMBIOS_BASE_BOARD_INFORMATION_STRUCT;

typedef NTSTATUS(WINAPI *ZwQueryInformationProcess_t)(HANDLE ProcessHandle, PROCESSINFOCLASS ProcessInformationClass, PVOID ProcessInformation, ULONG InformationLength, PULONG ReturnLength);
static ZwQueryInformationProcess_t ZwQueryInformationProcess = reinterpret_cast<ZwQueryInformationProcess_t>(GetProcAddress(GetModuleHandleW(L"ntdll.dll"), "ZwQueryInformationProcess"));

typedef NTSTATUS(WINAPI *ZwQueryInformationThread_t)(HANDLE ThreadHandle, THREADINFOCLASS ThreadInformationClass, PVOID ThreadInformation, ULONG InformationLength, PULONG ReturnLength);
static ZwQueryInformationThread_t ZwQueryInformationThread = reinterpret_cast<ZwQueryInformationThread_t>(GetProcAddress(GetModuleHandleW(L"ntdll.dll"), "ZwQueryInformationThread"));

#endif