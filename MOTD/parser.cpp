#include "parser.h"
#include "defines.h"
#include "console.h"

#include "protection/VMProtectSDK.h"

C_Parser* g_pParser = new C_Parser();

const char* C_Parser::Parse(const char* string, char* response)
{
	VMProtectBeginUltra(__FUNCTION__);
	int phrase_index = 0;
	UINT Iterator = 0;
	auto phrase = new char[255];
	memset(phrase, 0, 255);
	std::string new_phrase = "";
	std::size_t index = std::string(response).find(string) + strlen(string);

	for (Iterator = index; Iterator <= strlen((const char*)response); Iterator++) {
		if (response[Iterator] == '\"' && response[Iterator + 1] == ';' || response[Iterator] == '\"' && response[Iterator + 1] == '}')
			break;
		else
			continue;
	}

	for (index++; index < Iterator; index++) {
		phrase[phrase_index] = response[index];
		phrase_index++;
	}
	VMProtectEnd();
	return phrase;
}