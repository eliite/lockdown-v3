#include "clientdata.h"
#include "encrypt.h"
#include "parser.h"
#include "inet.h"
#include "console.h"
#include "memory.h"
#include "menu.h"

#include "protection/VMProtectSDK.h"

C_ClientData* g_pClientData = new C_ClientData();
#define INCREMENT() g_pMenu->authentication_progress += 0.05f;

void C_ClientData::RenewData(int * iLastError)
{
	VMProtectBeginUltra(__FUNCTION__);
	char szResponse[0x100];

	switch (g_pRequest->Create((char*)client_uri.c_str(), szResponse, 0x100, (char*)GenericHeader.c_str(), &g_pMenu->authentication_progress, 0.05f)) {
	case WININET_PROCESS_SUCCESS:
		break;
	case WININET_INTERNETOPEN_FAILED:
		if (iLastError)
			*iLastError = WININET_INTERNETOPEN_FAILED;

		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	case WININET_INTERNETCONNECT_FAILED:
		if (iLastError)
			*iLastError = WININET_INTERNETCONNECT_FAILED;

		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	case WININET_OPENREQUEST_FAILED:
		if (iLastError)
			*iLastError = WININET_OPENREQUEST_FAILED;

		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	case WININET_SENDREQUEST_FAILED:
		if (iLastError)
			*iLastError = WININET_SENDREQUEST_FAILED;

		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	case WININET_RESPONSE_NULL:
		if (iLastError)
			*iLastError = WININET_RESPONSE_NULL;

		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	}
	memcpy(szResponse, g_pEncrypt->Decrypt(szResponse).c_str(), SIZEOF(szResponse));

	if (!szResponse || !g_pClientData->Verify(szResponse)) {
		NtMessageBox(InitFailed.c_str(), __LINE__, __FILE__);
		ExitProcess(0);
	}

	this->auto_update = (g_pParser->Parse(AutoUpdate.c_str(), szResponse) == FullUpdate.c_str());
	this->version = g_pParser->Parse(Version.c_str(), szResponse);
	INCREMENT();

	float loader_build = LOADER_BUILD;
	this->build = std::stof(g_pMemory->ReplaceString(this->version, Period.c_str(), ""));

	if (this->build > 999) {
		std::string altered_build = g_pMemory->ReplaceString(std::to_string(this->build), Period.c_str(), "").insert(3, Period.c_str());
		this->build = std::stof(altered_build.c_str());
	}
	if (LOADER_BUILD > 999) {
		std::string altered_build = g_pMemory->ReplaceString(std::to_string(LOADER_BUILD), Period.c_str(), "").insert(3, Period.c_str());
		loader_build = std::stof(altered_build.c_str());
	}
	INCREMENT();

	this->up_to_date = (loader_build >= this->build);
	INCREMENT();

	if (!this->up_to_date) {
		g_pMenu->authentication_progress = 100.f;
		NtMessageBox(InvalidLdr.c_str(), __LINE__, __FILE__);
		g_pMemory->Delete();

		ExitProcess(0);
	}
	INCREMENT();

	this->hash = g_pParser->Parse("sha", szResponse);

	VMProtectEnd();
}

bool C_ClientData::Verify(char* response) {
	VMProtectBeginUltra(__FUNCTION__);
	if (g_pMemory->StringCompareA(response, "")) return false;
	const char* str_version = g_pParser->Parse(Version.c_str(), response);
	std::string slim_str_version = g_pMemory->ReplaceString(str_version, Period.c_str(), "").c_str();
	float version = 0;

	if (!g_pMemory->Contains(g_pParser->Parse(AutoUpdate.c_str(), response), us_update.c_str()))
		return false;

	if (g_pMemory->StringCompareA(g_pParser->Parse("sha", response), ""))
		return false;

	if (!g_pMemory->Numeric(slim_str_version.c_str()))
		return false;
	else {
		version = std::stof(slim_str_version.c_str());
		if (!g_pMemory->Boundaries<float>(version, 1.f, 9999999.f))
			return false;
	}
	VMProtectEnd();
	return true;
}

bool C_ClientData::Verify() {
	if (!this->hash) {
		g_pConsole->Log("Invalid hash");
		return true;
	}

	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);

	std::string filename = std::string(buffer);
	std::string directory;
	const size_t last_slash_idx = filename.rfind('\\');
	if (std::string::npos != last_slash_idx)
		directory = filename.substr(0, last_slash_idx);

	snprintf(buffer, MAX_PATH, rt_string(10, prc, s, bsl, prc, s, tp, e, x, e).c_str(), directory.c_str(), g_pClientData->name);
	g_pConsole->Log("%s\n\t\t%s -\n\t\t%s", buffer, g_pEncrypt->Hash((const char*)buffer).c_str(), this->hash);

	if (!g_pMemory->StringCompareA(g_pEncrypt->Hash((const char*)buffer).c_str(), this->hash))
		return false;
	else
		return true;
}