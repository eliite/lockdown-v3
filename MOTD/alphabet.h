#pragma once

#ifndef _ALPHABET_H
#define _ALPHABET_H

enum EAlphabet
{
	a = 10,
	b,
	c,
	d,
	e,
	f,
	g,
	h,
	i,
	j,
	k,
	l,
	m,
	n,
	o,
	p,
	q,
	r,
	s,
	t,
	u,
	v,
	w,
	x,
	y,
	z,
	A,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z,
	tp,
	spc,
	nt,
	us,
	zro,
	dash,
	bsl,
	pr,
	pl,
	prc,
	amps,
	equ,
	qm,
	fs,
	colon,
	semi,
	newl,
	ht,
	gt,
	lt,
	quo
};

//
// All retrieval subroutines (the alphabet) required to build strings 
// at runtime using the RIS object.
//

__forceinline char __cdecl sa(void) { return 'a'; }
__forceinline char __cdecl sb(void) { return 'b'; }
__forceinline char __cdecl sc(void) { return 'c'; }
__forceinline char __cdecl sd(void) { return 'd'; }
__forceinline char __cdecl se(void) { return 'e'; }
__forceinline char __cdecl sf(void) { return 'f'; }
__forceinline char __cdecl sg(void) { return 'g'; }
__forceinline char __cdecl sh(void) { return 'h'; }
__forceinline char __cdecl si(void) { return 'i'; }
__forceinline char __cdecl sj(void) { return 'j'; }
__forceinline char __cdecl sk(void) { return 'k'; }
__forceinline char __cdecl sl(void) { return 'l'; }
__forceinline char __cdecl sm(void) { return 'm'; }
__forceinline char __cdecl sn(void) { return 'n'; }
__forceinline char __cdecl so(void) { return 'o'; }
__forceinline char __cdecl sp(void) { return 'p'; }
__forceinline char __cdecl sq(void) { return 'q'; }
__forceinline char __cdecl sr(void) { return 'r'; }
__forceinline char __cdecl ss(void) { return 's'; }
__forceinline char __cdecl st(void) { return 't'; }
__forceinline char __cdecl su(void) { return 'u'; }
__forceinline char __cdecl sv(void) { return 'v'; }
__forceinline char __cdecl sw(void) { return 'w'; }
__forceinline char __cdecl sx(void) { return 'x'; }
__forceinline char __cdecl sy(void) { return 'y'; }
__forceinline char __cdecl sz(void) { return 'z'; }
__forceinline char __cdecl sA(void) { return 'A'; }
__forceinline char __cdecl sB(void) { return 'B'; }
__forceinline char __cdecl sC(void) { return 'C'; }
__forceinline char __cdecl sD(void) { return 'D'; }
__forceinline char __cdecl sE(void) { return 'E'; }
__forceinline char __cdecl sF(void) { return 'F'; }
__forceinline char __cdecl sG(void) { return 'G'; }
__forceinline char __cdecl sH(void) { return 'H'; }
__forceinline char __cdecl sI(void) { return 'I'; }
__forceinline char __cdecl sJ(void) { return 'J'; }
__forceinline char __cdecl sK(void) { return 'K'; }
__forceinline char __cdecl sL(void) { return 'L'; }
__forceinline char __cdecl sM(void) { return 'M'; }
__forceinline char __cdecl sN(void) { return 'N'; }
__forceinline char __cdecl sO(void) { return 'O'; }
__forceinline char __cdecl sP(void) { return 'P'; }
__forceinline char __cdecl sQ(void) { return 'Q'; }
__forceinline char __cdecl sR(void) { return 'R'; }
__forceinline char __cdecl sS(void) { return 'S'; }
__forceinline char __cdecl sT(void) { return 'T'; }
__forceinline char __cdecl sU(void) { return 'U'; }
__forceinline char __cdecl sV(void) { return 'V'; }
__forceinline char __cdecl sW(void) { return 'W'; }
__forceinline char __cdecl sX(void) { return 'X'; }
__forceinline char __cdecl sY(void) { return 'Y'; }
__forceinline char __cdecl sZ(void) { return 'Z'; }

__forceinline char __cdecl sn1(void) { return '1'; }
__forceinline char __cdecl sn2(void) { return '2'; }
__forceinline char __cdecl sn3(void) { return '3'; }
__forceinline char __cdecl sn4(void) { return '4'; }
__forceinline char __cdecl sn5(void) { return '5'; }
__forceinline char __cdecl sn6(void) { return '6'; }
__forceinline char __cdecl sn7(void) { return '7'; }
__forceinline char __cdecl sn8(void) { return '8'; }
__forceinline char __cdecl sn9(void) { return '9'; }

__forceinline char __cdecl stp(void) { return '.'; }
__forceinline char __cdecl sspc(void) { return ' '; }
__forceinline char __cdecl ssnt(void) { return 0x0; }
__forceinline char __cdecl ssus(void) { return '_'; }
__forceinline char __cdecl sszro(void) { return '0'; }
__forceinline char __cdecl ssd(void) { return '-'; }
__forceinline char __cdecl sssl(void) { return '\\'; }
__forceinline char __cdecl sspr(void) { return '('; }
__forceinline char __cdecl sspl(void) { return ')'; }
__forceinline char __cdecl ssnl(void) { return 0xA; }

__forceinline char __cdecl ssprc(void) { return '%'; }
__forceinline char __cdecl ssequ(void) { return '='; }
__forceinline char __cdecl ssamp(void) { return '&'; }
__forceinline char __cdecl ssqm(void) { return '?'; }
__forceinline char __cdecl ssfs(void) { return '/'; }
__forceinline char __cdecl sscol(void) { return ':'; }
__forceinline char __cdecl sssem(void) { return ';'; }
__forceinline char __cdecl ssht(void) { return '#'; }
__forceinline char __cdecl ssgt(void) { return '>'; }
__forceinline char __cdecl sslt(void) { return '<'; }
__forceinline char __cdecl ssquo(void) { return '\"'; }
#endif