#pragma once
#ifndef __PROTECTION_H__
#define __PROTECTION_H__

class C_Protection {
public:
	void Verify();
	void VerifyThread(int ThreadId);
	long int thread_time;

	private:
	bool ListProcessModules();
	bool ListProcessThreads();

	const char* WhitelistModules[79] = {
	"Lockdown.exe",
	"ntdll.dll",
	"KERNEL32.DLL",
	"KERNELBASE.dll",
	"USER32.dll",
	"win32u.dll",
	"GDI32.dll",
	"gdi32full.dll",
	"msvcp_win.dll",
	"ucrtbase.dll",
	"ADVAPI32.dll",
	"msvcrt.dll",
	"sechost.dll",
	"RPCRT4.dll",
	"SspiCli.dll",
	"CRYPTBASE.dll",
	"bcryptPrimitives.dll",
	"ole32.dll",
	"combase.dll",
	"IMM32.dll",
	"WININET.dll",
	"MSVCP140D.dll",
	"d3d9.dll",
	"VMProtectSDK32.dll",
	"VCRUNTIME140D.dll",
	"ucrtbased.dll",
	"dwmapi.dll",
	"CRYPT32.dll",
	"MSASN1.dll",
	"CRYPTSP.dll",
	"kernel.appcore.dll",
	"uxtheme.dll",
	"MSCTF.dll",
	"OLEAUT32.dll",
	"TextInputFramework.dll",
	"CoreUIComponents.dll",
	"CoreMessaging.dll",
	"SHCORE.dll",
	"ntmarta.dll",
	"wintypes.dll",
	"clbcatq.dll",
	"explorerframe.dll",
	"SHELL32.dll",
	"cfgmgr32.dll",
	"windows.storage.dll",
	"profapi.dll",
	"powrprof.dll",
	"shlwapi.dll",
	"aticfx32.dll",
	"VERSION.dll",
	"WINMM.dll",
	"winmmbase.dll",
	"atiu9pag.dll",
	"atiumdag.dll",
	"atiumdva.dll",
	"amdihk32.dll",
	"iertutil.dll",
	"WS2_32.dll",
	"ondemandconnroutehelper.dll",
	"winhttp.dll",
	"mswsock.dll",
	"DPAPI.DLL",
	"IPHLPAPI.DLL",
	"WINNSI.DLL",
	"NSI.dll",
	"WINTRUST.dll",
	"rsaenh.dll",
	"bcrypt.dll",
	"urlmon.dll",
	"DNSAPI.dll",
	"mdnsNSP.dll",
	"rasadhlp.dll",
	"fwpuclnt.dll",
	"schannel.dll",
	"mskeyprotect.dll",
	"ncrypt.dll",
	"NTASN1.dll",
	"cryptnet.dll",
	"ncryptsslp.dll",
	};
};

extern C_Protection* g_pProtection;

#endif