#pragma once

#ifndef __DEFINITIONS_H__
#define __DEFINITIONS_H__

#include "rt_string.h"
#include <string>

#define SIZEOF(s) (sizeof(s) / sizeof(s[0]))

#ifdef _DEBUG
#define WININET_USE_PROXY false
#else
#define WININET_USE_PROXY true
#endif

#define WININET_PROCESS_SUCCESS			0x00
#define WININET_INTERNETOPEN_FAILED		0x01
#define WININET_INTERNETCONNECT_FAILED	0x02
#define WININET_OPENREQUEST_FAILED		0x03
#define WININET_SENDREQUEST_FAILED		0x04
#define WININET_RESPONSE_NULL			0x05

#define MENU_LOGIN						0x00
#define MENU_AUTHENTICATE				0x01
#define MENU_CHEAT_SELECTION			0x02
#define MENU_INJECTION					0x03

#define ADMIN_INVALID_RESET_COUNT		0x30
#define ADMIN_INVALID_STATUS_INPUT		0x31
#define ADMIN_USER_ALREADY_BANNED		0x32
#define ADMIN_USER_ALREADY_UNBANNED		0x33
#define ADMIN_INVALID_USER				0x34

#define LOGIN_CREDENTIALS_INVALID		0x40
#define LOGIN_HARDWARE_INVALID			0x41
#define LOGIN_USER_BANNED				0x42
#define LOGIN_NO_LOADER_ACCESS			0x43
#define LOGIN_RANK_INVALID				0x44

#define LOADER_VERSION					LoadVer.c_str()
#define LOADER_BUILD					400.0f
#define LOADER_OUT_OF_DATE				0x50

#define ENCRYPT_BACKEND					0x51
#define ENCRYPT_LOADER					0x58
#define ENCRYPT_SHA						0x59

#define COLOR_UNDETECTED ImVec4(0.f, 1.f, 0.f, 1.f)
#define COLOR_UNKNOWN ImVec4(0.97f, 0.65f, 0.03f, 1.f)
#define COLOR_DETECTED ImVec4(1.f, 0.f, 0.f, 1.f)
#define COLOR_DISABLED ImVec4(1.f, 1.f, 1.f, 1.f)

#define SELF_REMOVE_STRING  TEXT(SelfRemove.c_str())

typedef int REQUEST;

static rt_string SOCKS(7, S, O, C, K, S, equ);
static rt_string AGENT(28, x, equ, u, P, L, c, c, 9, us, N, 9, q, 6, W, 2, B, 2, M, g, r, m, Q, 9, a, P, S, b);
static rt_string POST(5, P, O, S, T);
static rt_string Url(17, c, d, n, tp, a, y, y, m, b, o, t, tp, t, e, a, m);
static rt_string GenericHeader(48, C, o, n, t, e, n, t, dash, T, y, p, e, colon, spc, a, p, p, l, i, c, a, t, i, o, n, fs, x, dash, w, w, w, dash, f, o, r, m, dash, u, r, l, e, n, c, o, d, e, d);
static rt_string WriteVM(21, N, t, W, r, i, t, e, V, i, r, t, u, a, l, M, e, m, o, r, y);
static rt_string ReadVM(20, N, t, R, e, a, d, V, i, r, t, u, a, l, M, e, m, o, r, y);
static rt_string krnl(10, n, t, d, l, l, tp, d, l, l);
static rt_string cs_name(9, c, s, g, o, tp, e, x, e);
static rt_string serverbrowser(18, s, e, r, v, e, r, b, r, o, w, s, e, r, tp, d, l, l);
static rt_string Lockdown(9, L, o, c, k, d, o, w, n, tp, e, x, e);
static rt_string Error(6, E, r, r, o, r);
static rt_string CloseConnect(33, T, h, e, spc, s, e, r, v, e, r, spc, c, l, o, s, e, d, spc, t, h, e, spc, c, o, n, n, e, c, t, i, o, n);
static rt_string Rank(5, r, a, n, k);
static rt_string VipTime(9, v, i, p, us, t, i, m, e);
static rt_string Years(6, Y, e, a, r, s);
static rt_string Year(5, Y, e, a, r);
static rt_string Months(7, M, o, n, t, h, s);
static rt_string Month(6, M, o, n, t, h);
static rt_string Days(5, D, a, y, s);
static rt_string Day(4, D, a, y);
static rt_string Hours(6, H, o, u, r, s);
static rt_string Hour(5, H, o, u, r);
static rt_string Minutes(8, M, i, n, u, t, e, s);
static rt_string Minute(7, M, i, n, u, t, e);
static rt_string Seconds(8, S, e, c, o, n, d, s);
static rt_string Second(7, S, e, c, o, n, d);
static rt_string Lifetime(9, L, i, f, e, t, i, m, e);
static rt_string HalfLife(8, h, l, 2, tp, e, x, e);
static rt_string InitInterrupt(28, I, n, i, t, i, a, l, i, z, a, t, i, o, n, spc, i, n, t, e, r, r, u, p, t, e, d, tp);
static rt_string AutoUpdate(12, a, u, t, o, us, u, p, d, a, t, e);
static rt_string FullUpdate(12, f, u, l, l, us, u, p, d, a, t, e);
static rt_string Period(2, tp);
static rt_string Name(5, n, a, m, e);
static rt_string Version(8, v, e, r, s, i, o, n);
static rt_string InvalidLdr(16, I, n, v, a, l, i, d, spc, l, o, a, d, e, r, tp);
static rt_string BetaAvail(15, b, e, t, a, us, a, v, a, i, l, a, b, l, e);
static rt_string LastMod(14, l, a, s, t, us, m, o, d, i, f, i, e, d);
static rt_string Status(7, s, t, a, t, u, s);
static rt_string Status_Undetected(11, U, n, d, e, t, e, c, t, e, d);
static rt_string Status_Unknown(8, U, n, k, n, o, w, n);
static rt_string Status_Detected(9, D, e, t, e, c, t, e, d);
static rt_string Status_Disabled(9, D, i, s, a, b, l, e, d);
static rt_string RankBounds(16, r, a, n, k, us, b, o, u, n, d, a, r, i, e, s);
static rt_string InitFailed(23, I, n, i, t, i, a, l, i, z, a, t, i, o, n, spc, f, a, i, l, e, d, tp);
static rt_string LdrAccess(14, l, o, a, d, e, r, us, a, c, c, e, s, s);
static rt_string HWID(5, h, w, i, d);
static rt_string CRED(5, c, r, e, d);
static rt_string IncorLogin(26, I, n, c, o, r, r, e, c, t, spc, l, o, g, i, n, spc, p, r, o, v, i, d, e, d, tp);
static rt_string IncorHWID(35, I, n, c, o, r, r, e, c, t, spc, h, a, r, d, w, a, r, e, spc, i, d, e, n, t, i, f, i, c, a, t, i, o, n, tp);
static rt_string InvalAcc(16, I, n, v, a, l, i, d, spc, a, c, c, e, s, s, tp);
static rt_string InsufPriv(25, I, n, s, u, f, f, i, c, i, e, n, t, spc, p, r, i, v, i, l, e, g, e, s, tp);
static rt_string UserBanned(13, U, s, e, r, spc, b, a, n, n, e, d, tp);
static rt_string Login(6, L, o, g, i, n);
static rt_string InnerChild(14, ht, ht, i, n, n, e, r, us, c, h, i, l, d);
static rt_string Ayymbot(8, A, y, y, m, b, o, t);
static rt_string Username(9, U, s, e, r, n, a, m, e);
static rt_string htUsername(11, ht, ht, u, s, e, r, n, a, m, e);
static rt_string Password(9, P, a, s, s, w, o, r, d);
static rt_string htPassword(11, ht, ht, p, a, s, s, w, o, r, d);
static rt_string Loading(8, L, o, a, d, i, n, g);
static rt_string Injection(10, I, n, j, e, c, t, i, o, n);
static rt_string htInjection(12, ht, ht, i, n, j, e, c, t, i, o, n);
static rt_string LoadingGame(13, l, o, a, d, i, n, g, us, g, a, m, e);
static rt_string htFinished(11, ht, ht, f, i, n, i, s, h, e, d);
static rt_string Injecting(10, I, n, j, e, c, t, i, n, g);
static rt_string htStatus(9, ht, ht, s, t, a, t, u, s);
static rt_string Status_col(9, S, t, a, t, u, s, colon, spc);
static rt_string SubscriptionBuffer(17, S, u, b, s, c, r, i, p, t, i, o, n, colon, spc, prc, s);
static rt_string LastUpdateBuffer(16, L, a, s, t, spc, u, p, d, a, t, e, colon, spc, prc, s);
static rt_string htInjectionOptions(20, ht, ht, i, n, j, e, c, t, i, o, n, us, o, p, t, i, o, n, s);
static rt_string Load(5, L, o, a, d);
static rt_string Warning(8, W, a, r, n, i, n, g);
static rt_string CheatUnsafe(17, C, h, e, a, t, spc, i, s, spc, u, n, s, a, f, e, tp);
static rt_string CheatDisabled(29, C, h, e, a, t, spc, i, s, spc, c, u, r, r, e, n, t, l, y, spc, d, i, s, a, b, l, e, d, tp);
static rt_string SelectCheat(23, P, l, e, a, s, e, spc, s, e, l, e, c, t, spc, a, spc, C, h, e, a, t, tp);
static rt_string Exit(5, E, x, i, t);
static rt_string htGameSelect(17, ht, ht, g, a, m, e, us, s, e, l, e, c, t, i, o, n);
static rt_string PurchasedGames(16, P, u, r, c, h, a, s, e, d, spc, g, a, m, e, s);
static rt_string COMSPEC(8, C, O, M, S, P, E, C);
static rt_string cdel(8, fs, c, spc, d, e, l, spc);
static rt_string gn(7, spc, gt, spc, n, u, l);
static rt_string Open(5, O, p, e, n);
static rt_string access_us(8, a, c, c, e, s, s, us);
static rt_string hwid_us(6, h, w, i, d, us);
static rt_string cred_us(6, c, r, e, d, us);
static rt_string minus_one(3, dash, 1);
static rt_string us_update(8, us, u, p, d, a, t, e);
static rt_string ldr_username(25, d, L, V, q, 9, d, R, d, Q, t, 3, w, Q, n, a, a, 3, s, H, f, 2, 5, h, z);
static rt_string ldr_password(31, u, 2, 4, 6, V, Q, p, B, 6, 6, 8, z, r, e, v, M, M, r, d, X, g, q, C, y, M, U, S, u, k, B);
static rt_string LdrInitFail(30, L, o, a, d, e, r, spc, i, n, i, t, i, a, l, i, z, a, t, i, o, n, spc, f, a, i, l, e, d, tp);
static rt_string InvalidIndex(15, I, n, v, a, l, i, d, spc, i, n, d, e, x, tp);
static rt_string setproccrit(24, R, t, l, S, e, t, P, r, o, c, e, s, s, I, s, C, r, i, t, i, c, a, l);
static rt_string SelfRemove(60, c, m, d, tp, e, x, e, spc, fs, C, spc, p, i, n, g, spc, 1, tp, 1, tp, 1, tp, 1, spc, dash, n, spc, 1, spc, dash, w, spc, 3, zro, zro, zro, spc, gt, spc, N, u, l, spc, amps, spc, D, e, l,
	spc, fs, f, spc, fs, q, spc, quo, prc, s, quo);
static rt_string login_uri(55, a, b, equ, a, amps, a, equ, prc, s, amps, b, equ, prc, s, amps, c, equ, prc, s, amps, d, equ, prc, s, amps, e, equ, prc, s,
	amps, f, equ, prc, s, amps, g, equ, prc, s, amps, h, equ, prc, s, amps, i, equ, prc, s, amps, j, equ, prc, s);
static rt_string cheat_uri(10, a, b, equ, b, amps, a, equ, prc, s);
static rt_string client_uri(5, a, b, equ, c);
static rt_string inject_uri(60, a, b, equ, d, amps, a, equ, prc, s, amps, b, equ, prc, s, amps, c, equ, prc, s, amps, d, equ, prc, s, amps, e, equ, prc, s,
	amps, f, equ, prc, s, amps, g, equ, prc, s, amps, h, equ, prc, s, amps, i, equ, prc, s, amps, j, equ, prc, s, amps, k, equ, prc, s);
static rt_string error_uri(60, a, b, equ, e, amps, a, equ, prc, s, amps, b, equ, prc, s, amps, c, equ, prc, s, amps, d, equ, prc, s, amps, e, equ, prc, s,
	amps, f, equ, prc, s, amps, g, equ, prc, s, amps, h, equ, prc, s, amps, i, equ, prc, s, amps, j, equ, prc, s, amps, k, equ, prc, s);
static rt_string debug_uri(15, a, b, equ, f, amps, a, equ, prc, s, amps, b, equ, prc, s);
static rt_string action_php(12, fs, a, c, t, i, o, n, tp, p, h, p);
static rt_string protection_err(42, U, s, e, r, spc, r, a, n, spc, a, spc, l, o, a, d, e, r, spc, t, h, a, t, spc, w, a, s, spc, n, o, t, spc, p, r, o, t, e, c, t, e, d, tp);
static rt_string debugger_err(51, U, s, e, r, spc, a, t, t, e, m, p, t, e, d, spc, t, o, spc, a, t, t, a, c, h, spc, a, spc, d, e, b, u, g, g, e, r, spc, t, o, spc, t, h, e, spc, l, o, a, d, e, r, tp);
static rt_string vm_error(58, U, s, e, r, spc, a, t, t, e, m, p, t, e, d, spc, t, o, spc, r, u, n, spc, t, h, e, spc, l, o, a, d, e, r, spc, u, n, d, e, r, spc, a, spc, v, i, r, t, u, a, l, spc, m, a, c, h, i, n, e, tp);
static rt_string rtlap(19, R, t, l, A, d, j, u, s, t, P, r, i, v, i, l, e, g, e);
static rt_string ntrhe(17, N, t, R, a, i, s, e, H, a, r, d, E, r, r, o, r);
static rt_string has_cs(9, h, a, s, us, c, s, g, o);
static rt_string tru(5, t, r, u, e);
static rt_string fals(6, f, a, l, s, e);
static rt_string InvalSub(22, I, n, v, a, l, i, d, spc, s, u, b, s, c, r, i, p, t, i, o, n, tp);
static rt_string strSize(5, s, i, z, e);

static const char *AcceptTypes[2] = {
	{ "*/*" },
	{ 0 }
};

#endif