#pragma once
#ifndef __CLIENT_DATA_H__
#define __CLIENT_DATA_H__

#define BUILD_NUM 4.0.1

class C_ClientData {
public:
	const char* name;
	const char* version;
	const char* hash;
	bool auto_update;
	bool up_to_date;
	float build;
public:
	void RenewData(int* iLastError = nullptr);
	bool Verify(char* response);
	bool Verify();
};

extern C_ClientData* g_pClientData;

#endif