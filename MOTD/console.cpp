#include "console.h"
#include <ctime>

C_Console* g_pConsole;
std::string console_title;

#pragma unmanaged

void C_Console::OpenConsole(std::string szTitle) {
	AllocConsole();
	FILE *conin, *conout;

	freopen_s(&conin, "conin$", "r", stdin);
	freopen_s(&conout, "conout$", "w", stderr);
	freopen_s(&conout, "conout$", "w", stdout);

	if (szTitle.length() > 0)
	{
		SetConsoleTitleA(szTitle.c_str());
		console_title = szTitle;
	}
}

void C_Console::CloseConsole() {
	FreeConsole();
	HWND hWnd = FindWindowA(NULL, console_title.c_str());
	if (hWnd)
	{
		DestroyWindow(hWnd);
		hWnd = NULL;
	}
}

std::string C_Console::GetTimeString() {
	//Time related variables
	time_t current_time;
	struct tm *time_info;
	static char timeString[10];

	//Get current time
	time(&current_time);
	time_info = localtime(&current_time);

	//Get current time as string
	strftime(timeString, sizeof(timeString), "%I:%M%p", time_info);
	return timeString;
}

void C_Console::Log(const char* fmt, ...) {
	if (!fmt) return; //if the passed string is null return
	if (strlen(fmt) < 2) return;

	//Set up va_list and buffer to hold the params 
	va_list va_alist;
	char logBuf[256] = { 0 };

	//Do sprintf with the parameters
	va_start(va_alist, fmt);
	_vsnprintf(logBuf + strlen(logBuf), sizeof(logBuf) - strlen(logBuf), fmt, va_alist);
	va_end(va_alist);

	//Output to console
	if (logBuf[0] != '\0')
	{
		SetConsoleColor(FOREGROUND_MAGENTA);
		printf("[%s]", GetTimeString().c_str());
		SetConsoleColor(FOREGROUND_WHITE);
		printf(" ==> %s\n", logBuf);
	}
}

void C_Console::SetConsoleColor(unsigned short color) {
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
}

