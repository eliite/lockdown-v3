#ifndef _RUNTIME_STRING_HPP
#define _RUNTIME_STRING_HPP

#include <cstdarg>
#include <string>

#include "alphabet.h"

#pragma warning( disable : 4018 )
#pragma unmanaged
//
// Below are type definitions for retrieval subroutines used in the runtime initialized strings
//

typedef char(__cdecl *__AlphabetRoutine_t)(void);

static __AlphabetRoutine_t AlphanumericRoutines[0x52]
{
	&sn1,
	&sn2,
	&sn3,
	&sn4,
	&sn5,
	&sn6,
	&sn7,
	&sn8,
	&sn9,
	&sa,
	&sb,
	&sc,
	&sd,
	&se,
	&sf,
	&sg,
	&sh,
	&si,
	&sj,
	&sk,
	&sl,
	&sm,
	&sn,
	&so,
	&sp,
	&sq,
	&sr,
	&ss,
	&st,
	&su,
	&sv,
	&sw,
	&sx,
	&sy,
	&sz,
	&sA,
	&sB,
	&sC,
	&sD,
	&sE,
	&sF,
	&sG,
	&sH,
	&sI,
	&sJ,
	&sK,
	&sL,
	&sM,
	&sN,
	&sO,
	&sP,
	&sQ,
	&sR,
	&sS,
	&sT,
	&sU,
	&sV,
	&sW,
	&sX,
	&sY,
	&sZ,
	&stp,
	&sspc,
	&ssnt,
	&ssus,
	&sszro,
	&ssd,
	&sssl,
	&sspr,
	&sspl,
	&ssprc,
	&ssamp,
	&ssequ,
	&ssqm,
	&ssfs,
	&sscol,
	&sssem,
	&ssnl,
	&ssht,
	&ssgt,
	&sslt,
	&ssquo
};

//
// Runtime initialized string structures for indirect initialization
//

typedef struct _RIS
{
	unsigned int StringLength;
	__AlphabetRoutine_t *AlphabetRoutineTable;

private:
	std::string str;

	_RIS(const _RIS &) = default;

public:
	explicit _RIS(unsigned int StringLength, ...)
	{
		this->StringLength = StringLength;

		AlphabetRoutineTable = new __AlphabetRoutine_t[StringLength - 1];

		va_list routines;
		__crt_va_start(routines, StringLength);

		for (auto iter = 0; iter < StringLength - 1; iter++) {
			AlphabetRoutineTable[iter] = AlphanumericRoutines[__crt_va_arg(routines, int) - 1];
		}

		__crt_va_end(routines);

		init_string();
	}


	~_RIS(void)
	{
		delete[] AlphabetRoutineTable;
	}

	void init_string(void)
	{
		str.resize(StringLength);
		for (auto iter = 0; iter < StringLength - 1; iter++) {
			str[iter] = AlphabetRoutineTable[iter]();
		}

		str[str.size()] = 0x00;
	}

	const char *c_str(void)
	{
		return str.c_str();
	}

} rt_string, *prt_string;

#endif