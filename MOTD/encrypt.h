#ifndef __ENCRYPT_H__
#include <Windows.h>
#include "defines.h"
#include <string>

class C_Encrypt {
public:
	std::string Encrypt(char* string, int style = ENCRYPT_BACKEND);
	std::string Decrypt(char* string, int style = ENCRYPT_BACKEND);

	std::string Hash(const char* file);
	std::string Hash(char* byte, int size);

	void GenerateKey();
//private:
	char key[256];
};

extern C_Encrypt* g_pEncrypt;

#endif