#pragma once
#ifndef __CONFIG_H__
#define __CONFIG_H__

#pragma comment(lib, "urlmon.lib")

class C_Config
{
public:
	void Save();
	void Load();
	char* Handle();
	bool CheckConfigs();
	void Delete();
	int ReadInt(char* Cat, char* Name, int defaultval);
	float ReadFloat(char* Cat, char* Name, float defaultval);
	char* ReadString(char* Cat, char* Name, char* defaultval);
	void WriteInt(char* Cat, char* Name, int defaultval);
	void WriteFloat(char* Cata, char* Name, float setVal);
	void WriteString(char* Cat, char* Name, char* defaultval);
	bool bSaveCredentials;
private:
	char Path[255];
	char Path2[255];
	char Path3[255];
	char Path4[255];
	char Picture[255];
};

extern C_Config* g_pConfig;
#endif#pragma once
