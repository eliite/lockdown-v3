#include "userdata.h"
#include "hardware.h"
#include "inet.h"
#include "config.h"
#include "console.h"
#include "encrypt.h"
#include "memory.h"
#include "parser.h"
#include "menu.h"
#include "protection/VMProtectSDK.h"

#define INCREMENT() g_pMenu->authentication_progress += 0.05f;

C_UserData* g_pUserData = new C_UserData();

void C_UserData::RenewData(const char* szUsername, const char* szPassword, int* iLastError)
{
	VMProtectBeginUltra(__FUNCTION__);
	char szBuffer[0x200] = { 0 };
	char szResponse[0x200] = { 0 }; // 1

	username = buf_username;
	password = buf_password;
	INCREMENT();

	g_pHardware->Renew();
	INCREMENT();

	snprintf(szBuffer, 0x200, login_uri.c_str(),
		g_pEncrypt->Decrypt((char*)reinterpret_cast<const char*>(username)).c_str(),
		g_pEncrypt->Decrypt((char*)reinterpret_cast<const char*>(password)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::CPU_VENDOR)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::CPU)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::HDD_SERIAL)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::VIDEO_ADAPTER)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::PC_NAME)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::OS)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::OS_INSTALL_DATE)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::PHYS_MEM_SIZE)).c_str());
	INCREMENT();

	switch (g_pRequest->Create(szBuffer, szResponse, 0x200, (char*)GenericHeader.c_str(), &g_pMenu->authentication_progress, 0.05f)) {
	case WININET_PROCESS_SUCCESS:
		break;
	case WININET_INTERNETOPEN_FAILED:
		if (iLastError)
			*iLastError = WININET_INTERNETOPEN_FAILED;

		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	case WININET_INTERNETCONNECT_FAILED:
		if (iLastError)
			*iLastError = WININET_INTERNETCONNECT_FAILED;

		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	case WININET_OPENREQUEST_FAILED:
		if (iLastError)
			*iLastError = WININET_OPENREQUEST_FAILED;

		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	case WININET_SENDREQUEST_FAILED:
		if (iLastError)
			*iLastError = WININET_SENDREQUEST_FAILED;

		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	case WININET_RESPONSE_NULL:
		if (iLastError)
			*iLastError = WININET_RESPONSE_NULL;

		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	} INCREMENT();
	memcpy(szResponse, g_pEncrypt->Decrypt(szResponse).c_str(), SIZEOF(szResponse));

	if (szResponse && !g_pMemory->StringCompareA(g_pEncrypt->Encrypt((char*)g_pParser->Parse(CRED.c_str(), szResponse), ENCRYPT_SHA).c_str(),
		VMProtectDecryptStringA("bdf1aeb272d6a17c3bb466029b5b057a10180c7fb56afbea4f58e6ca8851baea"))) {
		std::thread err_thread(C_Communication::Log, IncorLogin.c_str(), __LINE__, __FILE__);
		err_thread.detach();
		was_invalid = true;
		g_pConfig->Save();
		MessageBox(0, IncorLogin.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		ExitProcess(0);
	}
	else if (!szResponse || !g_pUserData->Verify(szResponse)) {
		std::thread err_thread(C_Communication::Log, InitFailed.c_str(), __LINE__, __FILE__);
		err_thread.detach();

		NtMessageBox(InitFailed.c_str(), __LINE__, __FILE__);
		ExitProcess(0);
	}

	g_pUserData->was_invalid = false;

	for (INT i = 1; i <= 10; i++) {
		if (g_pMemory->StringCompareA(g_pEncrypt->Encrypt((char*)g_pParser->Parse(Rank.c_str(), szResponse), ENCRYPT_SHA).c_str(), szRanks[i - 1])) {
			this->rank = i;
			break;
		}
		else
			continue;
	}
	this->user_banned = (this->rank == 7);
	this->valid_rank = (this->rank > 0 && this->rank <= 8);
	INCREMENT();

	if (!g_pMemory->StringCompareA(g_pEncrypt->Encrypt((char*)g_pParser->Parse(has_cs.c_str(), szResponse), ENCRYPT_SHA).c_str(),
		VMProtectDecryptStringA("5feceb66ffc86f38d952786c6d696c79c2dbc239dd4e91b46729d73a27fb57e9"))) {
		cheat[0] = true;
		cheat[1] = false;
		valid_sub = true;
	}

	if (cheat[0]) {
		int difference = 0;
		char* suffix = (char*)"";
		time_t ptime = std::stoi(g_pParser->Parse(has_cs.c_str(), szResponse));
		time_t _time = ptime - time(0);

		if ((int)_time > 31536000) {
			difference = (int)_time / 31536000;

			if (difference != 1)
				suffix = (char*)Years.c_str();
			else
				suffix = (char*)Year.c_str();
		}
		else if ((int)_time > 2592000) {
			difference = (int)_time / 2592000;

			if (difference != 1)
				suffix = (char*)Months.c_str();
			else
				suffix = (char*)Month.c_str();
		}
		else if ((int)_time > 86400) {
			difference = (int)_time / 86400;

			if (difference != 1)
				suffix = (char*)Days.c_str();
			else
				suffix = (char*)Day.c_str();
		}
		else if ((int)_time > 3600) {
			difference = (int)_time / 3600;

			if (difference != 1)
				suffix = (char*)Hours.c_str();
			else
				suffix = (char*)Hour.c_str();
		}
		else if ((int)_time > 60) {
			difference = (int)_time / 60;

			if (difference != 1)
				suffix = (char*)Minutes.c_str();
			else
				suffix = (char*)Minute.c_str();
		}
		else if ((int)_time < 60 && (int)_time > 0) {
			if ((int)_time != 1)
				suffix = (char*)Seconds.c_str();
			else
				suffix = (char*)Second.c_str();
		}
		else if ((int)ptime == 0) {
			suffix = (char*)Seconds.c_str();
		}
		else {
			suffix = (char*)Lifetime.c_str();
		}

		if (difference > 0)
			snprintf(csgo_sub, 32, rt_string(6, prc, i, spc, prc, s).c_str(), difference, suffix);
		else if ((int)ptime == -1)
			snprintf(csgo_sub, 32, rt_string(3, prc, s).c_str(), suffix);
		else if ((int)ptime == 0)
			snprintf(csgo_sub, 32, rt_string(3, prc, s).c_str(), "0 Seconds");
	}
	INCREMENT();

	if (g_pMemory->StringCompareA(g_pEncrypt->Encrypt((char*)g_pParser->Parse(LdrAccess.c_str(), szResponse), ENCRYPT_SHA).c_str(),
		VMProtectDecryptStringA("785bbf56bf26b6e7a5bc65d42e93d1b1c032352b54b208e6c6b5d27f39402b23"))) {
		this->loader_access = true;
		this->has_beta = false;
	}
	else if (g_pMemory->StringCompareA(g_pEncrypt->Encrypt((char*)g_pParser->Parse(LdrAccess.c_str(), szResponse), ENCRYPT_SHA).c_str(),
		VMProtectDecryptStringA("f7767ac5b8fe559a68a091460517d61ad832e3b65a9ae48d49e37e36ce12c2fd"))) {
		this->loader_access = true;
		this->has_beta = true;
	}
	else if (g_pMemory->StringCompareA(g_pEncrypt->Encrypt((char*)g_pParser->Parse(LdrAccess.c_str(), szResponse), ENCRYPT_SHA).c_str(),
		VMProtectDecryptStringA("47967114dd7216d963fd9d5da1a5e00d9a57e7317baed653f265e9b2379841af"))) {
		this->loader_access = false;
		this->has_beta = false;
	}
	INCREMENT();
	
	this->hwid_match = g_pMemory->StringCompareA(g_pEncrypt->Encrypt((char*)g_pParser->Parse(HWID.c_str(), szResponse), ENCRYPT_SHA).c_str(), 
		VMProtectDecryptStringA("a987059be8dfd4d2bec225df723297f750b536302c7a4ebcaff6dfa78e29c8d6"));
	INCREMENT();

	this->login_valid = g_pMemory->StringCompareA(g_pEncrypt->Encrypt((char*)g_pParser->Parse(CRED.c_str(), szResponse), ENCRYPT_SHA).c_str(),
		VMProtectDecryptStringA("bdf1aeb272d6a17c3bb466029b5b057a10180c7fb56afbea4f58e6ca8851baea"));
	INCREMENT();
	g_pConfig->Save();

	VMProtectEnd();
}

bool C_UserData::Verify(char* response) {
	VMProtectBeginUltra(__FUNCTION__);
	if (g_pMemory->StringCompareA(response, "")) return false;
	const char* str_rank = g_pParser->Parse(Rank.c_str(), response);
	int rank = 0;
	if (g_pMemory->Numeric(str_rank)) {
		rank = std::stoi(str_rank);
		if (!g_pMemory->Boundaries<int>(rank, 1, 8))
			return false;
	}
	else
		return false;

	const char* str_cs_time = g_pParser->Parse(has_cs.c_str(), response);
	int cs_time = 0;
	if (g_pMemory->Numeric(str_cs_time) || g_pMemory->StringCompareA(str_cs_time, minus_one.c_str())) {
		cs_time = std::stoi(str_cs_time);
		if (!g_pMemory->Boundaries<int>(cs_time, -1, INT_MAX))
			return false;
	}
	else
		return false;

	if (!g_pMemory->Contains(g_pParser->Parse(LdrAccess.c_str(), response), access_us.c_str()))
		return false;
	if (!g_pMemory->Contains(g_pParser->Parse(HWID.c_str(), response), hwid_us.c_str()))
		return false;
	if (!g_pMemory->Contains(g_pParser->Parse(CRED.c_str(), response), cred_us.c_str()))
		return false;

	VMProtectEnd();
	return true;
}

#undef INCREMENT