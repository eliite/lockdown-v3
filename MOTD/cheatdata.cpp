#include "cheatdata.h"
#include "defines.h"
#include "encrypt.h"
#include "console.h"
#include "inet.h"
#include "parser.h"
#include "memory.h"
#include "menu.h"
#include <time.h>
#include <chrono>
#include "protection/VMProtectSDK.h"

C_CheatData* g_pCheatData[2] = { new C_CheatData(), new C_CheatData() };
#define INCREMENT() g_pMenu->authentication_progress += 0.05f;

void C_CheatData::RenewData(int identification, int* iLastError)
{
	VMProtectBeginUltra(__FUNCTION__);
	char szResponse[0x100];
	char szBuffer[0x100];

	const char* obf_id = "a";

	if (identification == 0)
		obf_id = "a";
	else if (identification == 1)
		obf_id = "b";

	snprintf(szBuffer, SIZEOF(szBuffer), cheat_uri.c_str(), obf_id);
	switch (g_pRequest->Create(szBuffer, szResponse, SIZEOF(szResponse), (char*)GenericHeader.c_str(), &g_pMenu->authentication_progress, 0.025f)) {
	case WININET_PROCESS_SUCCESS:
		break;
	case WININET_INTERNETOPEN_FAILED:
		if (iLastError)
			*iLastError = WININET_INTERNETOPEN_FAILED;


		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	case WININET_INTERNETCONNECT_FAILED:
		if (iLastError)
			*iLastError = WININET_INTERNETCONNECT_FAILED;


		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	case WININET_OPENREQUEST_FAILED:
		if (iLastError)
			*iLastError = WININET_OPENREQUEST_FAILED;


		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	case WININET_SENDREQUEST_FAILED:
		if (iLastError)
			*iLastError = WININET_SENDREQUEST_FAILED;


		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	case WININET_RESPONSE_NULL:
		if (iLastError)
			*iLastError = WININET_RESPONSE_NULL;


		g_pMenu->authentication_progress = 1.f;
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);
		ExitProcess(0);

		return;
		break;
	}
	memcpy(szResponse, g_pEncrypt->Decrypt(szResponse).c_str(), SIZEOF(szResponse));

	if (!szResponse || !this->Verify(szResponse)) {
		NtMessageBox(InitFailed.c_str(), __LINE__, __FILE__);
		ExitProcess(0);
	}
	this->name = g_pParser->Parse(Name.c_str(), szResponse);
	this->version = g_pParser->Parse(Version.c_str(), szResponse);
	this->has_beta = g_pMemory->StringCompareA(g_pEncrypt->Encrypt((char*)g_pParser->Parse(BetaAvail.c_str(), szResponse), ENCRYPT_SHA).c_str(), VMProtectDecryptStringA("7ac3ed2f7a34979cd2d23208d1f4c2e09f1c9e1ddab50dbdf95561a169bc34c3"));
	INCREMENT();

	int difference = 0;
	char* suffix = (char*)"";

	time_t current_time = std::stoi(g_pParser->Parse(LastMod.c_str(), szResponse));
	current_time = time(0) - current_time;
	if ((int)current_time > 31536000) {
		difference = (int)current_time / 31536000;

		if (difference != 1)
			suffix = (char*)Years.c_str();
		else
			suffix = (char*)Year.c_str();
	}
	else if ((int)current_time > 2592000) {
		difference = (int)current_time / 2592000;

		if (difference != 1)
			suffix = (char*)Months.c_str();
		else
			suffix = (char*)Month.c_str();
	}
	else if ((int)current_time > 86400) {
		difference = (int)current_time / 86400;

		if (difference != 1)
			suffix = (char*)Days.c_str();
		else
			suffix = (char*)Day.c_str();
	}
	else if ((int)current_time > 3600) {
		difference = (int)current_time / 3600;

		if (difference != 1)
			suffix = (char*)Hours.c_str();
		else
			suffix = (char*)Hour.c_str();

	}
	else if ((int)current_time > 60) {
		difference = (int)current_time / 60;

		if (difference != 1)
			suffix = (char*)Minutes.c_str();
		else
			suffix = (char*)Minute.c_str();
	}
	else {
		if (current_time != 1)
			suffix = (char*)Seconds.c_str();
		else
			suffix = (char*)Second.c_str();
	}

	snprintf(this->update_time, SIZEOF(this->update_time), rt_string(6, prc, i, spc, prc, s).c_str(), difference, suffix);
	INCREMENT();

	this->status = g_pParser->Parse(Status.c_str(), szResponse);
	if (g_pMemory->StringCompareA(status, Status_Undetected.c_str()))
		status_color = COLOR_UNDETECTED;
	else if (g_pMemory->StringCompareA(status, Status_Unknown.c_str()))
		status_color = COLOR_UNKNOWN;
	else if (g_pMemory->StringCompareA(status, Status_Detected.c_str()))
		status_color = COLOR_DETECTED;
	else if (g_pMemory->StringCompareA(status, Status_Disabled.c_str()))
		status_color = COLOR_DISABLED;
	INCREMENT();

	this->build = std::stoi(g_pMemory->ReplaceString(this->version, Period.c_str(), ""));
	if ((int)this->build > 999) {
		std::string new_build_string = std::to_string(this->build);
		new_build_string.insert(3, Period.c_str());
		this->build = std::stod(new_build_string);
	}
	INCREMENT();

	size = std::stoi(g_pParser->Parse(strSize.c_str(), szResponse));

	this->hash = g_pParser->Parse("sha", szResponse);

	const char* ranks = g_pParser->Parse(RankBounds.c_str(), szResponse);
	this->rank_boundaries[0] = ranks[0] - 48;
	this->rank_boundaries[1] = ranks[2] - 48;
	INCREMENT();

	VMProtectEnd();
}

bool C_CheatData::Verify(char* response) {
	VMProtectBeginUltra(__FUNCTION__);
	if (g_pMemory->StringCompareA(response, "")) return false;
	const char* str_version = g_pParser->Parse(Version.c_str(), response);
	std::string slim_str_version = g_pMemory->ReplaceString(str_version, Period.c_str(), "");
	float version = 0;

	if (!g_pMemory->Numeric(slim_str_version.c_str()))
		return false;
	else {
		version = std::stof(slim_str_version);
		if (!g_pMemory->Boundaries<float>(version, 1.f, 9999999.f))
			return false;
	}

	const char* str_mod_time = g_pParser->Parse(LastMod.c_str(), response);
	int mod_time = 0;
	if (g_pMemory->Numeric(str_mod_time)) {
		mod_time = std::stoi(str_mod_time);
		if (!g_pMemory->Boundaries<int>(mod_time, 1552179663, INT_MAX))
			return false;
	}
	else
		return false;

	const char* str_size = g_pParser->Parse(strSize.c_str(), response);
	int size = 0;
	if (g_pMemory->Numeric(str_size))
		size = std::stoi(str_size);
	else
		return false;

	if (!g_pMemory->Contains(g_pParser->Parse(BetaAvail.c_str(), response), access_us.c_str()))
		return false;

	if (g_pMemory->StringCompareA(g_pParser->Parse("sha", response), ""))
		return false;

	VMProtectEnd();
	return true;
}