#include "encrypt.h"
#include "defines.h"
#include "protection/VMProtectSDK.h"
#include "sha.h"
#include <chrono>

C_Encrypt* g_pEncrypt = new C_Encrypt();


std::string C_Encrypt::Encrypt(char* string, int style) {
	VMProtectBeginUltra(__FUNCTION__);
	std::string ret = "";
	if (style == ENCRYPT_BACKEND) {
		int inc = 0;
		int size = strlen(string);

		for (int i = 0; i < size; i++) {
			if (inc >= 3) inc = 0;
			ret += std::string(1, string[i] + inc);
			inc += 1;
		}
	}
	else if (style == ENCRYPT_LOADER) {
		int size = strlen(string);

		for (int i = 0; i < size; i++)
			ret += std::string(1, string[i] ^ 'b');
	}
	else if (style == ENCRYPT_SHA)
		picosha2::hash256_hex_string(std::string(string), ret);

	VMProtectEnd();
	return ret;
}

std::string C_Encrypt::Decrypt(char* string, int style) {
	VMProtectBeginUltra(__FUNCTION__);
	std::string ret = "";
	if (style == ENCRYPT_BACKEND) {
		int inc = 0;
		int size = strlen(string);

		for (int i = 0; i < size; i++) {
			if (inc >= 3) inc = 0;
			ret += std::string(1, string[i] - inc);
			inc += 1;
		}
		VMProtectEnd();
	}
	else if (style == ENCRYPT_LOADER) {
		int size = strlen(string);

		for (int i = 0; i < size; i++)
			ret += std::string(1, string[i] ^ 'b');
	}
	else if (style == ENCRYPT_SHA)
		ret = std::string(string);
	return ret;
}

std::string C_Encrypt::Hash(const char* file) {
	std::string ret = "";
	std::ifstream f(file, std::ios::binary);
	std::string str((std::istreambuf_iterator<char>(f)),
		std::istreambuf_iterator<char>());
	picosha2::hash256_hex_string(std::string(str), ret);
	f.close();
	return ret;
}

std::string Hash(char* byte, int size) {
	std::string ret = "";
	std::string str(byte, size);
	picosha2::hash256_hex_string(str, ret);
	return ret;
}

void C_Encrypt::GenerateKey() {
	VMProtectBeginUltra(__FUNCTION__);

	srand((UINT)time(NULL));
	for (int i = 0; i < 256; ++i)
		key[i] = 65 + rand() % 26 + 256 * (rand() % 2);
	key[256] = '\0';
	VMProtectEnd();
}