#include "protection.h"
#include "inet.h"
#include "hardware.h"
#include "manual_map.h"
#include "encrypt.h"
#include "memory.h"
#include "console.h"
#include "clientdata.h"
#include <Windows.h>
#include <chrono>

#include "protection/VMProtectSDK.h"

C_Protection* g_pProtection = new C_Protection();

void C_Protection::Verify() {
	VMProtectBeginUltra(__FUNCTION__);
	while (true) {
		this->thread_time = static_cast<long int>(time(NULL));

		if (!VMProtectIsProtected()) {
			char szBuffer[0x200] = { 0 };
			char szResponse[0x200] = { 0 };
			snprintf(szBuffer, 0x200, debug_uri.c_str(), g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::HDD_SERIAL)).c_str(), g_pEncrypt->Decrypt((char*)protection_err.c_str()).c_str());

			g_pRequest->Create(szBuffer, szResponse, 0x200, (char*)GenericHeader.c_str());
			Sleep(500);
			g_pMemory->CrashSystem();
		}
		else if (VMProtectIsDebuggerPresent(true)) {
			char szBuffer[0x200] = { 0 };
			char szResponse[0x200] = { 0 };
			snprintf(szBuffer, 0x200, debug_uri.c_str(), g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::HDD_SERIAL)).c_str(), g_pEncrypt->Decrypt((char*)VMProtectDecryptStringA("User attempted to attach a debugger to the loader.")).c_str());

			g_pRequest->Create(szBuffer, szResponse, 0x200, (char*)GenericHeader.c_str());
			Sleep(500);
			g_pMemory->CrashSystem();
		}
		else if (VMProtectIsVirtualMachinePresent()) {
			char szBuffer[0x200] = { 0 };
			char szResponse[0x200] = { 0 };
			snprintf(szBuffer, 0x200, debug_uri.c_str(), g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::HDD_SERIAL)).c_str(), g_pEncrypt->Decrypt((char*)vm_error.c_str()).c_str());

			g_pRequest->Create(szBuffer, szResponse, 0x200, (char*)GenericHeader.c_str());
			Sleep(500);
			g_pMemory->CrashSystem();
		}
		if (!VMProtectIsValidImageCRC()) {
			char szBuffer[0x200] = { 0 };
			char szResponse[0x200] = { 0 };
			snprintf(szBuffer, 0x200, debug_uri.c_str(), g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::HDD_SERIAL)).c_str(), g_pEncrypt->Decrypt((char*)protection_err.c_str()).c_str());

			g_pRequest->Create(szBuffer, szResponse, 0x200, (char*)GenericHeader.c_str());
			Sleep(500);
			g_pMemory->CrashSystem();
		}
		Sleep(500);
	}
	VMProtectEnd();
}

void C_Protection::VerifyThread(int ThreadId) {

}

bool C_Protection::ListProcessModules()
{
	VMProtectBeginUltra(__FUNCTION__);
	HANDLE hModuleSnap = INVALID_HANDLE_VALUE;
	MODULEENTRY32 me32;
	DWORD dwPID = GetCurrentProcessId();

	// Take a snapshot of all modules in the specified process.
	hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwPID);
	if (hModuleSnap == INVALID_HANDLE_VALUE)
	{
		return(false);
	}

	// Set the size of the structure before using it.
	me32.dwSize = sizeof(MODULEENTRY32);

	// Retrieve information about the first module,
	// and exit if unsuccessful
	if (!Module32First(hModuleSnap, &me32))
	{
		CloseHandle(hModuleSnap);           // clean the snapshot object
		return(false);
	}
	// Now walk the module list of the process,
	// and display information about each module
	do
	{
		bool found_match = false;
		int size = SIZEOF(g_pProtection->WhitelistModules);
		for (INT i = 0; i < size; i++) {
			if (g_pMemory->StringCompareA(g_pProtection->WhitelistModules[i], me32.szModule)) {
				found_match = true;
				continue;
			}
		}
		if (!found_match) {
			CloseHandle(hModuleSnap);
			return(true);
		}
	} while (Module32Next(hModuleSnap, &me32));
	CloseHandle(hModuleSnap);
	VMProtectEnd();
	return(false);
}

bool C_Protection::ListProcessThreads()
{
	VMProtectBeginUltra(__FUNCTION__);
	HANDLE hThreadSnap = INVALID_HANDLE_VALUE;
	THREADENTRY32 te32;
	DWORD dwPID = GetCurrentProcessId();

	// Take a snapshot of all running threads  
	hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	if (hThreadSnap == INVALID_HANDLE_VALUE)
		return(false);

	// Fill in the size of the structure before using it. 
	te32.dwSize = sizeof(THREADENTRY32);

	// Retrieve information about the first thread,
	// and exit if unsuccessful
	if (!Thread32First(hThreadSnap, &te32))
	{
		CloseHandle(hThreadSnap);          // clean the snapshot object
		return(false);
	}

	// Now walk the thread list of the system,
	// and display information about each thread
	// associated with the specified process
	int thread_count = 0;
	do
	{
		
		if (te32.th32OwnerProcessID == dwPID)
			thread_count++;
	} while (Thread32Next(hThreadSnap, &te32));
	if (thread_count > 21) return true;
	CloseHandle(hThreadSnap);
	VMProtectEnd();
	return(false);
}