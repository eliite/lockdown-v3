#pragma once

#ifndef	__WIN_INET_H__
#define	__WIN_INET_H__

#include "defines.h"
#include "rt_string.h"
#include <Windows.h>

#pragma comment( lib, "Wininet.lib" )

class C_Communication {
public:
	REQUEST Create(char* szData, char* szResponse, int size, char* szHeader, float* const &prog = nullptr, float amount = 0.f);
	static REQUEST Log(const char* err, int line, const char* file);
private:
	VOID Close(HANDLE);
private:
	HANDLE hOpen;
	HANDLE hConnect;
	HANDLE hRequest;
	bool bSent;
	ULONG BytesRead;
};

void NtMessageBox(const char* error, int line, const char* file);
void NtMessageBox(const char* error, int line, const char* file, const char* message, UINT type);

extern C_Communication* g_pRequest;
#endif