#pragma once

#ifndef _MANUAL_MAP_H__
#define _MANUAL_MAP_H__

#include <windows.h>
#include <assert.h>
#include <tlhelp32.h>
#include <iostream>
#include <excpt.h>
#include <signal.h>
#include <shlwapi.h>
#include <windows.h>
#include <string>
#include <iostream>
#include <ostream>

#undef GetModuleHandleEx

typedef HMODULE(WINAPI *pLoadLibraryA)(LPCSTR);
typedef FARPROC(WINAPI *pGetProcAddress)(HMODULE, LPCSTR);

typedef BOOL(WINAPI *PDLL_MAIN)(HMODULE, DWORD, PVOID);

typedef NTSTATUS(NTAPI *TNtWriteVirtualMemory)(HANDLE, PVOID, PVOID, ULONG, PULONG);
typedef NTSTATUS(NTAPI *TNtReadVirtualMemory)(HANDLE, PVOID, PVOID, ULONG, PULONG);

typedef struct _MANUAL_INJECT
{
	PVOID ImageBase;
	PIMAGE_NT_HEADERS NtHeaders;
	PIMAGE_BASE_RELOCATION BaseRelocation;
	PIMAGE_IMPORT_DESCRIPTOR ImportDirectory;
	pLoadLibraryA fnLoadLibraryA;
	pGetProcAddress fnGetProcAddress;
}MANUAL_INJECT, *PMANUAL_INJECT;

class C_ManualMap {
public:
	int Inject(int cheat_number);
	UINT GetProcessID(const char* process);
private:
	int Map(const char* process, DWORD process_id, int cheat_number);
	HMODULE GetModuleHandleEx(const char* module_name, DWORD process_id);
	bool LoadProcess(const char* process);
private:
	static DWORD WINAPI LoadDll(PVOID p);
	static DWORD WINAPI LoadDllEnd();
	static void ProcessLoop(const char* szProcessName);
private:
	TNtWriteVirtualMemory NtWriteVirtualMemory = NULL;
	TNtReadVirtualMemory NtReadVirtualMemory = NULL;
private:
	HANDLE hProcess = NULL;
	DWORD dwProcessId = NULL;
	bool bProcess = false;
	const char* szProcessName = "";
};

extern C_ManualMap* g_pInject;

#endif // !_MANUAL_MAP_H__