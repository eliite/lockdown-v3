#include "hardware.h"
#include "memory.h"
#include "console.h"
#include "rt_string.h"
#include "defines.h"
#include "inet.h"

#include <cstring>
#include <intrin.h>
#include <d3d9.h>
#include <time.h>

#include "protection/VMProtectSDK.h"

#pragma comment( lib, "d3d9.lib" )

#undef GetComputerName
C_HWID* g_pHardware;

char CpuProcessorVendor[0x100] = { 0 };
char CpuProcessorNameAndSpeed[0x100] = { 0 };
char HarddiskSerialNumber[0x100] = { 0 };
char VideoAdapter[0x100] = { 0 };
char ComputerName[0x100] = { 0 };
char PCOperatingSystem[0x100] = { 0 };
char InstallationDate[0x100] = { 0 };
char PhysicalMemory[0x100] = { 0 };
char MonitorName[0x100] = { 0 };

char* C_HWID::Retrieve(E_Hardware Type) {
	switch (Type) {
	case E_Hardware::CPU:
		return CpuProcessorNameAndSpeed;
		break;
	case E_Hardware::CPU_VENDOR:
		return CpuProcessorVendor;
		break;
	case E_Hardware::HDD_SERIAL:
		return HarddiskSerialNumber;
		break;
	case E_Hardware::VIDEO_ADAPTER:
		return VideoAdapter;
		break;
	case E_Hardware::PC_NAME:
		return ComputerName;
		break;
	case E_Hardware::OS:
		return PCOperatingSystem;
		break;
	case E_Hardware::OS_INSTALL_DATE:
		return InstallationDate;
		break;
	case E_Hardware::PHYS_MEM_SIZE:
		return PhysicalMemory;
		break;
	case E_Hardware::MONITOR_NAME:
		return MonitorName;
	default:

		return (char*)InvalidIndex.c_str();
		break;
	}
}

void C_HWID::Renew() {
	Retrieve1();
	Retrieve2();
	Retrieve3(0);
	Retrieve4();
	Retrieve5();
	Retrieve6();
	Retrieve7();
	Retrieve8();
	Retrieve9();
}

int C_HWID::Hash() {
	VMProtectBeginUltra(__FUNCTION__);
	std::size_t str_full_hash = 0;
	std::size_t str_hash[0x7] = { 0 };

	str_hash[0x0] = std::hash<char *>{}(CpuProcessorVendor);
	str_hash[0x1] = std::hash<char *>{}(CpuProcessorNameAndSpeed);
	str_hash[0x2] = std::hash<char *>{}(HarddiskSerialNumber);
	str_hash[0x3] = std::hash<char *>{}(VideoAdapter);
	str_hash[0x4] = std::hash<char *>{}(ComputerName);
	str_hash[0x5] = std::hash<char *>{}(PCOperatingSystem);
	str_hash[0x6] = std::hash<char *>{}(InstallationDate);

	for (UINT Iterator = 0; Iterator <= 0x6; Iterator++)
		str_full_hash += (str_hash[Iterator]);

	VMProtectEnd();
	return str_full_hash;
}

void C_HWID::Retrieve1() {
	VMProtectBeginUltra(__FUNCTION__);
	CPUID_REGISTERS cr = { 0 };

	__cpuid(reinterpret_cast<int *>(&cr), 0x00);

	g_pMemory->CopyMoveMemory(CpuProcessorVendor, &cr.ebx, sizeof(int));
	g_pMemory->CopyMoveMemory(reinterpret_cast<unsigned int *>(CpuProcessorVendor + 4), &cr.edx, sizeof(int));
	g_pMemory->CopyMoveMemory(reinterpret_cast<unsigned int *>(CpuProcessorVendor + 8), &cr.ecx, sizeof(int));
	memset(reinterpret_cast<unsigned int *>(CpuProcessorVendor + 12), 0, sizeof(CpuProcessorVendor) - 12);

	strcpy(CpuProcessorVendor, g_pMemory->ReplaceString(CpuProcessorVendor, "  ", "").c_str());
	VMProtectEnd();
	//g_pConsole->Log("[System Support] %s completed with no errors.\n\t[+] Vendor: %s|", __FUNCTION__, CpuProcessorVendor);
}

void C_HWID::Retrieve2() {
	VMProtectBeginUltra(__FUNCTION__);
	CPUID_REGISTERS cr = { 0 };
	__cpuid(reinterpret_cast<int *>(&cr), 0x80000000);

	if (cr.eax & 0x80000000) {
		if (cr.eax >= 0x80000004) {
			__cpuid(reinterpret_cast<int *>(&cr), 0x80000002);
			g_pMemory->CopyMoveMemory(CpuProcessorNameAndSpeed, &cr.eax, sizeof(int) * 4);

			__cpuid(reinterpret_cast<int *>(&cr), 0x80000003);
			g_pMemory->CopyMoveMemory(reinterpret_cast<unsigned int *>(CpuProcessorNameAndSpeed + 16), &cr.eax, sizeof(int) * 4);

			__cpuid(reinterpret_cast<int *>(&cr), 0x80000004);
			g_pMemory->CopyMoveMemory(reinterpret_cast<unsigned int *>(CpuProcessorNameAndSpeed + 31), &cr.eax, sizeof(int) * 4);
		}
	}

	strcpy(CpuProcessorNameAndSpeed, g_pMemory->ReplaceString(g_pMemory->ReplaceString(CpuProcessorNameAndSpeed, "Procssor", "Processor").c_str(), "  ", "").c_str());
	VMProtectEnd();
	//g_pConsole->Log("[System Support] %s completed with no errors.\n\t[+] Processor: %s|", __FUNCTION__, CpuProcessorNameAndSpeed);
}

void C_HWID::Retrieve7() {
	VMProtectBeginUltra(__FUNCTION__);
	IDirect3D9 *pD3D9 = nullptr;
	pD3D9 = Direct3DCreate9(D3D_SDK_VERSION);

	if (pD3D9) {
		auto adapter_count = pD3D9->GetAdapterCount();

		for (auto iter = 0; iter < 1; iter++) {
			D3DADAPTER_IDENTIFIER9 ident = { 0 };
			pD3D9->GetAdapterIdentifier(iter, 0, &ident);

			auto length = 0;
			while (ident.Description[length++]);

			g_pMemory->CopyMoveMemory(VideoAdapter, ident.Description, length);
		}
	}
	else {
		//g_pConsole->Log("[System Support Exception] Video Adapter could not be retrieved.\n");

		//
		// Not Found
		//
		rt_string not_found(10, N, o, t, spc, F, o, u, n, d);
		g_pMemory->CopyMoveMemory(VideoAdapter, (void *)(not_found.c_str()), 10);
	}
	strcpy(VideoAdapter, g_pMemory->ReplaceString(VideoAdapter, "  ", "").c_str());
	//g_pConsole->Log("[System Support] %s completed with no errors.\n\t[+] Video Adapter: %s|\n", __FUNCTION__, VideoAdapter);
	VMProtectEnd();
	pD3D9->Release();
}

void C_HWID::Retrieve4() {
	VMProtectBeginUltra(__FUNCTION__);
	unsigned long Result = 0;

	char Buffer[512] = { 0 };
	unsigned long BufferSize = sizeof(Buffer);

	HKEY Key;
	rt_string CurrentSubkey(46, S, O, F, T, W, A, R, E, bsl, M, i, c, r, o, s, o, f, t, bsl, W, i, n, d, o, w, s, spc, N, T, bsl, C, u, r, r, e, n, t, V, e, r, s, i, o, n, bsl);
	Result = RegOpenKeyA(HKEY_LOCAL_MACHINE, CurrentSubkey.c_str(), &Key);

	unsigned long type = REG_SZ;
	rt_string ProductName(12, P, r, o, d, u, c, t, N, a, m, e);
	Result = RegQueryValueExA(Key, ProductName.c_str(), 0, &type, (BYTE *)(&Buffer), &BufferSize);

	if (Result == ERROR_SEVERITY_SUCCESS) {
		auto Length = g_pMemory->StringLengthA(Buffer);
		g_pMemory->CopyMoveMemory(PCOperatingSystem, Buffer, Length);

		RegCloseKey(Key);
	}
	strcpy(PCOperatingSystem, g_pMemory->ReplaceString(PCOperatingSystem, "  ", "").c_str());
	VMProtectEnd();
	//g_pConsole->Log("[System Support] %s completed with no errors.\n\t[+] Operating System: %s|\n", __FUNCTION__, PCOperatingSystem);
}

void C_HWID::Retrieve5() {
	VMProtectBeginUltra(__FUNCTION__);
	unsigned long Result = 0;

	time_t InstallTime = 0;
	struct tm timeinfo;

	char TimeBuffer[128] = { 0 };
	unsigned long BufferSize = sizeof(TimeBuffer);

	HKEY Key;
	rt_string CurrentSubkey(46, S, O, F, T, W, A, R, E, bsl, M, i, c, r, o, s, o, f, t, bsl, W, i, n, d, o, w, s, spc, N, T, bsl, C, u, r, r, e, n, t, V, e, r, s, i, o, n, bsl);
	Result = RegOpenKeyExA(HKEY_LOCAL_MACHINE, CurrentSubkey.c_str(), 0, KEY_READ | KEY_WOW64_64KEY, &Key);


	unsigned long type = REG_DWORD;
	rt_string InstallDate(12, I, n, s, t, a, l, l, D, a, t, e);
	Result = RegQueryValueExA(Key, InstallDate.c_str(), 0, &type, (BYTE *)(&InstallTime), &BufferSize);

	localtime_s(&timeinfo, &InstallTime);
	asctime_s(TimeBuffer, 128, &timeinfo);

	if (Result == ERROR_SEVERITY_SUCCESS) {
		auto Length = g_pMemory->StringLengthA(TimeBuffer);
		snprintf(TimeBuffer, 128, rt_string(3, prc, s).c_str(), g_pMemory->ReplaceString(std::string(TimeBuffer), "  ", "").c_str());
		g_pMemory->CopyMoveMemory(InstallationDate, (void*)g_pMemory->ReplaceString(TimeBuffer, "\n", "").c_str(), Length);

		RegCloseKey(Key);
	}
	VMProtectEnd();
	//g_pConsole->Log("[System Support] %s completed with no errors.\n\t[+] Install Date: %s|\n", __FUNCTION__, InstallationDate);
}

void C_HWID::Retrieve6() {
	VMProtectBeginUltra(__FUNCTION__);
	unsigned long size = 0x100;

	char sample[0x100];
	auto result = GetComputerNameA(sample, &size);

	if (!result) {

		rt_string name_unavailable(26, C, o, m, p, u, t, e, r, spc, N, a, m, e, spc, U, n, a, v, a, i, l, a, b, l, e);
		g_pMemory->CopyMoveMemory(ComputerName, (void *)(name_unavailable.c_str()), 26);

	}
	else {
		g_pMemory->CopyMoveMemory(ComputerName, (void *)(sample), size);
	}
	strcpy(ComputerName, g_pMemory->ReplaceString(ComputerName, "  ", "").c_str());
	VMProtectEnd();
	//g_pConsole->Log("[System Support] %s completed with no errors.\n\t[+] Computer Name: %s|\n", __FUNCTION__, ComputerName);
}

void C_HWID::Retrieve3(unsigned short DriveNumber) {
	VMProtectBeginUltra(__FUNCTION__);
	char szPhysicalDevice[0x100] = { 0 };
	rt_string phys_drive(18, bsl, bsl, tp, bsl, P, h, y, s, i, c, a, l, D, r, i, v, e);

	rt_string fmt(5, prc, s, prc, u);
	snprintf(szPhysicalDevice, 0x100, fmt.c_str(), phys_drive.c_str(), DriveNumber);

	void *DeviceHandle = CreateFileA(szPhysicalDevice, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, 0, 0);

	if (DeviceHandle == INVALID_HANDLE_VALUE) {
		//g_pConsole->Log("[System Support Exception] ==> Could not get handle to physical device.\n");

		NtMessageBox(LdrInitFail.c_str(), __LINE__, __FILE__);
		ExitProcess(0);
		return;
	}

	STORAGE_PROPERTY_QUERY sqpPropertyQuery;
	ZeroMemory(&sqpPropertyQuery, sizeof(STORAGE_PROPERTY_QUERY));

	sqpPropertyQuery.PropertyId = StorageDeviceProperty;
	sqpPropertyQuery.QueryType = PropertyStandardQuery;

	STORAGE_DESCRIPTOR_HEADER sdhDescriptorHeader = { 0 };
	unsigned long ulBytesReturned = 0;
	if (!DeviceIoControl(DeviceHandle, IOCTL_STORAGE_QUERY_PROPERTY, &sqpPropertyQuery, sizeof(STORAGE_PROPERTY_QUERY), &sdhDescriptorHeader, sizeof(STORAGE_DESCRIPTOR_HEADER), &ulBytesReturned, 0)) {
		//g_pConsole->Log("[System Support Exception] ==> Failed to query drive storage property structure. First wind.\n");

		CloseHandle(DeviceHandle);
		NtMessageBox(LdrInitFail.c_str(), __LINE__, __FILE__);
		ExitProcess(0);
		return;
	}

	const unsigned long ulOutSize = sdhDescriptorHeader.Size;
	unsigned char *pOutputBuffer = new unsigned char[ulOutSize];
	ZeroMemory(pOutputBuffer, ulOutSize);

	if (!DeviceIoControl(DeviceHandle, IOCTL_STORAGE_QUERY_PROPERTY, &sqpPropertyQuery, sizeof(STORAGE_PROPERTY_QUERY), pOutputBuffer, ulOutSize, &ulBytesReturned, 0)) {
		//g_pConsole->Log("[System Support Exception] ==> Failed to query drive storage property structure. Second wind.\n");

		CloseHandle(DeviceHandle);

		NtMessageBox(LdrInitFail.c_str(), __LINE__, __FILE__);
		ExitProcess(0);
		return;
	}

	STORAGE_DEVICE_DESCRIPTOR *psddDeviceDescriptor = reinterpret_cast<STORAGE_DEVICE_DESCRIPTOR *>(pOutputBuffer);
	if (psddDeviceDescriptor->SerialNumberOffset) {
		memcpy(HarddiskSerialNumber, &pOutputBuffer[psddDeviceDescriptor->SerialNumberOffset], ulOutSize);
	}

	snprintf(HarddiskSerialNumber, sizeof(HarddiskSerialNumber), g_pMemory->ReplaceString(HarddiskSerialNumber, "  ", "").c_str());

	//g_pConsole->Log("[System Support] %s completed with no errors.\n\t[+] HDD Serial: %s|\n", __FUNCTION__, HarddiskSerialNumber);

	delete[] pOutputBuffer;

	strcpy(HarddiskSerialNumber, g_pMemory->ReplaceString(HarddiskSerialNumber, " ", "").c_str());
	CloseHandle(DeviceHandle);

	VMProtectEnd();
	return;
}

void C_HWID::Retrieve8() {
	VMProtectBeginUltra(__FUNCTION__);
	ULONGLONG ram_size = 0;
	BOOL return_val = GetPhysicallyInstalledSystemMemory(&ram_size);
	if (return_val == FALSE || ram_size == 0) {
		NtMessageBox(LdrInitFail.c_str(), __LINE__, __FILE__);
		ExitProcess(0);
	}
	else
		ram_size /= (1024 * 1024); // KB to GB

	strcpy(PhysicalMemory, g_pMemory->ReplaceString(std::to_string(ram_size), " ", "").append(" GB").c_str());
	VMProtectEnd();
	//g_pConsole->Log("Ram size %s", PhysicalMemory);
	return;
}

void C_HWID::Retrieve9() {
	VMProtectBeginUltra(__FUNCTION__);
	DISPLAY_DEVICE dd;
	dd.cb = sizeof(dd);
	int deviceIndex = 0;
	char MonitorString[0x32] = { 0 };
	while (EnumDisplayDevices(0, deviceIndex, &dd, 0))
	{
		std::string deviceName = dd.DeviceName;
		int monitorIndex = 0;
		while (EnumDisplayDevices(deviceName.c_str(), monitorIndex, &dd, 0))
		{
			strcpy(MonitorString, dd.DeviceString);
			++monitorIndex;
		}
		++deviceIndex;
	}
	strcpy(MonitorName, MonitorString);
	VMProtectEnd();
	return;
}