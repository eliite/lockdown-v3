#include "manual_map.h"
#include "cheatdata.h"
#include "console.h"
#include "manual_map.h"
#include "encrypt.h"
#include "userdata.h"
#include "defines.h"
#include "hardware.h"
#include "memory.h"
#include "menu.h"
#include "inet.h"
#include "sha.h"
#include <thread>
#include <fstream>

#include "protection/VMProtectSDK.h"

#define INCREMENT() g_pMenu->inject_progress += 0.04f

C_ManualMap* g_pInject = new C_ManualMap();

static bool good = true;
void C_ManualMap::ProcessLoop(const char* szProcessName) {
	VMProtectBeginUltra(__FUNCTION__);
	while (good) {
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
		good = g_pInject->LoadProcess(szProcessName);
		if (good) g_pConsole->Log(rt_string(3, i, i).c_str());
	}

	if (!good) {

		NtMessageBox(InitInterrupt.c_str(), __LINE__, __FILE__);
		ExitProcess(0);
	}
	VMProtectEnd();
}

int C_ManualMap::Inject(int cheat_number) {
	VMProtectBeginUltra(__FUNCTION__);
	NtWriteVirtualMemory = (TNtWriteVirtualMemory)GetProcAddress(GetModuleHandle(krnl.c_str()), WriteVM.c_str());
	NtReadVirtualMemory = (TNtReadVirtualMemory)GetProcAddress(GetModuleHandle(krnl.c_str()), ReadVM.c_str());

	/*if (g_pCheatData[0]->cheat_selection)
		szProcessName = HalfLife.c_str();
	else if (g_pCheatData[1]->cheat_selection)
		szProcessName = cs_name.c_str();

	if (g_pUserData->cheat[0] && g_pUserData->cheat[1] && cheat_number == 0)
		szProcessName = cs_name.c_str();
	else if (g_pUserData->cheat[0] && g_pUserData->cheat[1] && cheat_number == 1)
		szProcessName = HalfLife.c_str();
	else if (g_pUserData->cheat[0] && !g_pUserData->cheat[1])
		szProcessName = cs_name.c_str();
	else if (!g_pUserData->cheat[0] && g_pUserData->cheat[1])
		szProcessName = HalfLife.c_str();*/

	szProcessName = cs_name.c_str();

	while (!LoadProcess(szProcessName))
		Sleep(1000);

	while (!dwProcessId)
		dwProcessId = GetProcessID(szProcessName);

	std::thread proc_loop(ProcessLoop, szProcessName);
	proc_loop.detach();
	VMProtectEnd();

	return Map(szProcessName, dwProcessId, cheat_number);
}

int C_ManualMap::Map(const char* process, DWORD process_id, int cheat_number) {
	VMProtectBegin(__FUNCTION__);

	PIMAGE_DOS_HEADER pIDH;
	PIMAGE_NT_HEADERS pINH;
	PIMAGE_SECTION_HEADER pISH;

	HANDLE hProcess, hThread, hToken;
	PVOID image, mem;
	DWORD i, ExitCode;

	TOKEN_PRIVILEGES tp;
	MANUAL_INJECT ManualInject;

	INCREMENT();
	INCREMENT();
	INCREMENT();
	INCREMENT();

	if (OpenProcessToken((HANDLE)-1, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) {
		tp.PrivilegeCount = 1;
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

		tp.Privileges[0].Luid.LowPart = 20;
		tp.Privileges[0].Luid.HighPart = 0;

		AdjustTokenPrivileges(hToken, FALSE, &tp, 0, NULL, NULL);
		CloseHandle(hToken);
	}
	INCREMENT();

	const char* number_to_use = "a";
	/*if (g_pMemory->StringCompareA(szProcessName, cs_name.c_str()))
		number_to_use = "a";
	else if (g_pMemory->StringCompareA(szProcessName, HalfLife.c_str()))
		number_to_use = "b";*/

	char szBuffer[0x200] = { 0 };

	snprintf(szBuffer, 0x200, inject_uri.c_str(),
		g_pEncrypt->Decrypt(g_pUserData->username).c_str(),
		g_pEncrypt->Decrypt(g_pUserData->password).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::CPU_VENDOR)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::CPU)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::HDD_SERIAL)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::VIDEO_ADAPTER)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::PC_NAME)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::OS)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::OS_INSTALL_DATE)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::PHYS_MEM_SIZE)).c_str(),
		number_to_use);
	INCREMENT();

	char dllbuffer[600000] = { 0 };

	char buf[256] = { 0 };

	INCREMENT();

	switch (g_pRequest->Create(szBuffer, (char*)dllbuffer, g_pCheatData[cheat_number]->size, (char*)GenericHeader.c_str(), &g_pMenu->inject_progress, 0.04f)) {
	case WININET_PROCESS_SUCCESS:
		break;
	case WININET_INTERNETOPEN_FAILED:
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);

		ExitProcess(0);
		return WININET_INTERNETOPEN_FAILED;
		break;
	case WININET_INTERNETCONNECT_FAILED:
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);

		ExitProcess(0);
		return WININET_INTERNETCONNECT_FAILED;
		break;
	case WININET_OPENREQUEST_FAILED:
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);

		ExitProcess(0);
		return WININET_OPENREQUEST_FAILED;
		break;
	case WININET_SENDREQUEST_FAILED:
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);

		ExitProcess(0);
		return WININET_SENDREQUEST_FAILED;
		break;
	case WININET_RESPONSE_NULL:
		NtMessageBox(CloseConnect.c_str(), __LINE__, __FILE__);

		ExitProcess(0);
		return WININET_RESPONSE_NULL;
		break;
	} INCREMENT();

	int inc = 0;
	for (int i = 0; i < g_pCheatData[cheat_number]->size; i++) {
		if (inc >= 3) inc = 0;
		dllbuffer[i] -= inc;
		inc += 1;
	}

	pIDH = (PIMAGE_DOS_HEADER)dllbuffer;
	if (pIDH->e_magic != IMAGE_DOS_SIGNATURE) {
		VirtualFree(dllbuffer, 0, MEM_RELEASE);
		MessageBox(0, InitFailed.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		ExitProcess(0);
		return -1;
	} INCREMENT();

	pINH = (PIMAGE_NT_HEADERS)((LPBYTE)dllbuffer + pIDH->e_lfanew);
	if (pINH->Signature != IMAGE_NT_SIGNATURE) { // er

		VirtualFree(dllbuffer, 0, MEM_RELEASE);
		MessageBox(0, InitFailed.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		ExitProcess(0);
		return -1;
	} INCREMENT();


	if (!(pINH->FileHeader.Characteristics & IMAGE_FILE_DLL)) {

		VirtualFree(dllbuffer, 0, MEM_RELEASE);
		MessageBox(0, InitFailed.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		ExitProcess(0);
		return -1;
	} INCREMENT();

	if (g_pMemory->StringCompareA(process, cs_name.c_str())) {
		HMODULE hModule = GetModuleHandleEx(serverbrowser.c_str(), dwProcessId);

		while (!hModule) {
			hModule = GetModuleHandleEx(serverbrowser.c_str(), dwProcessId);
			Sleep(1000);
		}
	}
	INCREMENT();

	hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwProcessId);
	if (!hProcess) {
		VirtualFree(dllbuffer, 0, MEM_RELEASE);
		CloseHandle(hProcess);


		NtMessageBox(InitFailed.c_str(), __LINE__, __FILE__);
		//NtMessageBox(0, InitFailed.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		ExitProcess(0);

		return -1;
	} INCREMENT();

	image = VirtualAllocEx(hProcess, NULL, pINH->OptionalHeader.SizeOfImage, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if (!image) {
		VirtualFree(dllbuffer, 0, MEM_RELEASE);
		CloseHandle(hProcess);

		MessageBox(0, InitFailed.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		ExitProcess(0);

		return -1;
	} INCREMENT();

	if (NtWriteVirtualMemory(hProcess, image, dllbuffer, pINH->OptionalHeader.SizeOfHeaders, NULL) != 0) {
		VirtualFreeEx(hProcess, image, 0, MEM_RELEASE);
		CloseHandle(hProcess);

		VirtualFree(dllbuffer, 0, MEM_RELEASE);


		MessageBox(0, InitFailed.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		ExitProcess(0);

		return -1;
	} INCREMENT();
	pISH = (PIMAGE_SECTION_HEADER)(pINH + 1);
	INCREMENT();

	for (i = 0; i < pINH->FileHeader.NumberOfSections; i++)
		NtWriteVirtualMemory(hProcess, (PVOID)((LPBYTE)image + pISH[i].VirtualAddress), (PVOID)((LPBYTE)dllbuffer + pISH[i].PointerToRawData), pISH[i].SizeOfRawData, NULL);
	INCREMENT();

	mem = VirtualAllocEx(hProcess, NULL, 4096, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if (!mem) {
		VirtualFreeEx(hProcess, image, 0, MEM_RELEASE);
		CloseHandle(hProcess);

		VirtualFree(dllbuffer, 0, MEM_RELEASE);


		MessageBox(0, InitFailed.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		ExitProcess(0);

		return -1;
	} INCREMENT();

	memset(&ManualInject, 0, sizeof(MANUAL_INJECT));
	ManualInject.ImageBase = image;
	ManualInject.NtHeaders = (PIMAGE_NT_HEADERS)((LPBYTE)image + pIDH->e_lfanew);
	ManualInject.BaseRelocation = (PIMAGE_BASE_RELOCATION)((LPBYTE)image + pINH->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress);
	ManualInject.ImportDirectory = (PIMAGE_IMPORT_DESCRIPTOR)((LPBYTE)image + pINH->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
	ManualInject.fnLoadLibraryA = LoadLibraryA;
	ManualInject.fnGetProcAddress = GetProcAddress;
	INCREMENT();

	NtWriteVirtualMemory(hProcess, mem, &ManualInject, sizeof(MANUAL_INJECT), NULL);
	NtWriteVirtualMemory(hProcess, (PVOID)((PMANUAL_INJECT)mem + 1), LoadDll, (DWORD)LoadDllEnd - (DWORD)LoadDll, NULL);
	INCREMENT();

	hThread = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)((PMANUAL_INJECT)mem + 1), mem, 0, NULL);
	if (!hThread) {
		VirtualFreeEx(hProcess, mem, 0, MEM_RELEASE);
		VirtualFreeEx(hProcess, image, 0, MEM_RELEASE);

		CloseHandle(hProcess);

		VirtualFree(dllbuffer, 0, MEM_RELEASE);


		MessageBox(0, InitFailed.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		ExitProcess(0);

		return -1;
	} INCREMENT();

	WaitForSingleObject(hThread, INFINITE);
	GetExitCodeThread(hThread, &ExitCode);
	INCREMENT();

	if (!ExitCode)
	{
		VirtualFreeEx(hProcess, mem, 0, MEM_RELEASE);
		VirtualFreeEx(hProcess, image, 0, MEM_RELEASE);

		CloseHandle(hThread);
		CloseHandle(hProcess);

		VirtualFree(dllbuffer, 0, MEM_RELEASE);


		MessageBox(0, InitFailed.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		ExitProcess(0);

		return -1;
	} INCREMENT();

	CloseHandle(hThread);
	VirtualFreeEx(hProcess, mem, 0, MEM_RELEASE);

	CloseHandle(hProcess);
	INCREMENT();

	VMProtectEnd();

	return 0;
}

bool C_ManualMap::LoadProcess(const char* process)
{
	VMProtectBeginUltra(__FUNCTION__);
	HANDLE hProcessId = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	PROCESSENTRY32 pEntry;
	pEntry.dwSize = sizeof(pEntry);

	do
	{
		if (!strcmp(pEntry.szExeFile, process))
		{
			dwProcessId = pEntry.th32ProcessID;
			CloseHandle(hProcessId);

			hProcess = OpenProcess(PROCESS_ALL_ACCESS, false, dwProcessId);
			return (bProcess = true);
		}

	} while (Process32Next(hProcessId, &pEntry));

	VMProtectEnd();
	return (bProcess = false);
}

UINT C_ManualMap::GetProcessID(const char* process) {
	VMProtectBeginUltra(__FUNCTION__);
	HANDLE snap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	unsigned int count = 0;
	unsigned int pid = 0;

	if (snap == INVALID_HANDLE_VALUE) {
		throw GetLastError();
	}

	if (!WaitForSingleObject(snap, 0) == WAIT_TIMEOUT) {
		return 0;
	}

	PROCESSENTRY32 proc;
	proc.dwSize = sizeof(PROCESSENTRY32);
	BOOL ret = Process32Next(snap, &proc);

	while (ret) {
		if (!_stricmp(proc.szExeFile, process)) {
			count++;
			pid = proc.th32ProcessID;
		}
		ret = Process32Next(snap, &proc);
	}

	if (count > 1) {
		pid = -1;
	}

	CloseHandle(snap);
	VMProtectEnd();
	return pid;
}

HMODULE C_ManualMap::GetModuleHandleEx(const char *module_name, DWORD process_id) // GetMoguleHandle recode for external processes
{
	VMProtectBeginUltra(__FUNCTION__);
	if (!module_name || !process_id) { return NULL; } // invalid input
	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, process_id);
	if (hSnap == INVALID_HANDLE_VALUE) { return NULL; }
	MODULEENTRY32 me;
	me.dwSize = sizeof(MODULEENTRY32);
	if (Module32First(hSnap, &me)) // we go now
	{
		while (Module32Next(hSnap, &me)) // through all modules in the target process
		{
			if (!strcmp(me.szModule, module_name)) // is this the model we are looking for?
			{
				CloseHandle(hSnap);
				return me.hModule; // this is our module, return it.
			}
		}
	}
	CloseHandle(hSnap);
	VMProtectEnd();
	return NULL; // counldn't find module
}

DWORD WINAPI C_ManualMap::LoadDll(PVOID p)
{
	PMANUAL_INJECT ManualInject;

	HMODULE hModule;
	DWORD i, Function, count, delta;

	PDWORD ptr;
	PWORD list;

	PIMAGE_BASE_RELOCATION pIBR;
	PIMAGE_IMPORT_DESCRIPTOR pIID;
	PIMAGE_IMPORT_BY_NAME pIBN;
	PIMAGE_THUNK_DATA FirstThunk, OrigFirstThunk;

	PDLL_MAIN EntryPoint;

	ManualInject = (PMANUAL_INJECT)p;

	pIBR = ManualInject->BaseRelocation;
	delta = (DWORD)((LPBYTE)ManualInject->ImageBase - ManualInject->NtHeaders->OptionalHeader.ImageBase); // Calculate the delta																						  // Relocate the image

	while (pIBR->VirtualAddress)
	{
		if (pIBR->SizeOfBlock >= sizeof(IMAGE_BASE_RELOCATION))
		{
			count = (pIBR->SizeOfBlock - sizeof(IMAGE_BASE_RELOCATION)) / sizeof(WORD);
			list = (PWORD)(pIBR + 1);

			for (i = 0; i < count; i++)
			{
				if (list[i])
				{
					ptr = (PDWORD)((LPBYTE)ManualInject->ImageBase + (pIBR->VirtualAddress + (list[i] & 0xFFF)));
					*ptr += delta;
				}
			}
		}

		pIBR = (PIMAGE_BASE_RELOCATION)((LPBYTE)pIBR + pIBR->SizeOfBlock);
	}

	pIID = ManualInject->ImportDirectory;

	// Resolve DLL imports

	while (pIID->Characteristics)
	{
		OrigFirstThunk = (PIMAGE_THUNK_DATA)((LPBYTE)ManualInject->ImageBase + pIID->OriginalFirstThunk);
		FirstThunk = (PIMAGE_THUNK_DATA)((LPBYTE)ManualInject->ImageBase + pIID->FirstThunk);

		hModule = ManualInject->fnLoadLibraryA((LPCSTR)ManualInject->ImageBase + pIID->Name);

		if (!hModule)
		{
			return FALSE;
		}

		while (OrigFirstThunk->u1.AddressOfData)
		{
			if (OrigFirstThunk->u1.Ordinal & IMAGE_ORDINAL_FLAG)
			{
				// Import by ordinal

				Function = (DWORD)ManualInject->fnGetProcAddress(hModule, (LPCSTR)(OrigFirstThunk->u1.Ordinal & 0xFFFF));

				if (!Function)
				{
					return FALSE;
				}

				FirstThunk->u1.Function = Function;
			}

			else
			{
				// Import by name

				pIBN = (PIMAGE_IMPORT_BY_NAME)((LPBYTE)ManualInject->ImageBase + OrigFirstThunk->u1.AddressOfData);
				Function = (DWORD)ManualInject->fnGetProcAddress(hModule, (LPCSTR)pIBN->Name);

				if (!Function)
				{
					return FALSE;
				}

				FirstThunk->u1.Function = Function;
			}

			OrigFirstThunk++;
			FirstThunk++;
		}

		pIID++;
	}

	if (ManualInject->NtHeaders->OptionalHeader.AddressOfEntryPoint)
	{
		EntryPoint = (PDLL_MAIN)((LPBYTE)ManualInject->ImageBase + ManualInject->NtHeaders->OptionalHeader.AddressOfEntryPoint);
		return EntryPoint((HMODULE)ManualInject->ImageBase, DLL_PROCESS_ATTACH, NULL); // Call the entry point
	}
	return TRUE;
}

DWORD WINAPI C_ManualMap::LoadDllEnd()
{
	return 0;
}