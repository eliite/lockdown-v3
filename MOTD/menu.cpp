#include "menu.h"
#include "defines.h"
#include "config.h"
#include "console.h"
#include "clientdata.h"
#include "cheatdata.h"
#include "manual_map.h"
#include "inet.h"
#include "memory.h"
#include "parser.h"

#include <chrono>
#include <stdio.h>
#include <time.h>
#include <thread>

#include "protection/VMProtectSDK.h"

C_Menu* g_pMenu = new C_Menu();
static bool updating = false;
static bool login = false;
static bool close = false;
static bool game_select[NUMBER_OF_CHEATS] = { false };
static bool ready = false;
static int cheat_number = 0;
static int other_number = 0;
static int cheats = 0;

void UpdateUser() {
	updating = true;
	g_pClientData->RenewData();
	g_pUserData->RenewData(g_pUserData->buf_username, g_pUserData->buf_password, nullptr);

	for (int i = 0; i < NUMBER_OF_CHEATS; i++)
		g_pCheatData[i]->RenewData(i);

	if (!g_pUserData->login_valid) {
		std::thread err_thread(C_Communication::Log, IncorLogin.c_str(), __LINE__, __FILE__);
		err_thread.detach();
		g_pUserData->was_invalid = false;
		MessageBox(0, IncorLogin.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		login = false;
		ExitProcess(0);
	}
	else if (!g_pUserData->hwid_match) {
		std::thread err_thread(C_Communication::Log, IncorHWID.c_str(), __LINE__, __FILE__);
		err_thread.detach();
		MessageBox(0, IncorHWID.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		g_pMemory->Delete();
		login = false;
		ExitProcess(0);
	}
	else if (!g_pUserData->valid_sub) {
		std::thread err_thread(C_Communication::Log, InvalAcc.c_str(), __LINE__, __FILE__);
		err_thread.detach();
		MessageBox(0, InvalSub.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		login = false;
		g_pMemory->Delete();
		ExitProcess(0);
	}
	else if (!g_pUserData->loader_access) {
		std::thread err_thread(C_Communication::Log, InvalAcc.c_str(), __LINE__, __FILE__);
		err_thread.detach();
		MessageBox(0, InvalAcc.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		login = false;
		g_pMemory->Delete();
		ExitProcess(0);
	}
	else if (!g_pUserData->valid_rank) {
		std::thread err_thread(C_Communication::Log, InsufPriv.c_str(), __LINE__, __FILE__);
		err_thread.detach();
		MessageBox(0, InsufPriv.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		login = false;
		ExitProcess(0);
	}
	else if (g_pUserData->user_banned) {
		std::thread err_thread(C_Communication::Log, UserBanned.c_str(), __LINE__, __FILE__);
		err_thread.detach();
		MessageBox(0, UserBanned.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
		login = false;
		g_pMemory->Delete();
		ExitProcess(0);
	}

	if (g_pUserData->login_valid
		&& g_pUserData->hwid_match
		&& g_pUserData->loader_access
		&& g_pUserData->valid_rank
		&& !g_pUserData->user_banned
		&& g_pUserData->valid_sub)
		login = true;

	ready = true;
}

void Inj() {
	g_pConfig->Save();
	int error = g_pInject->Inject(other_number);
	if (error != 0) {
		std::thread err_thread(C_Communication::Log, InitFailed.c_str(), __LINE__, __FILE__);
		err_thread.detach();
		MessageBox(0, InitFailed.c_str(), Error.c_str(), MB_ICONERROR | MB_OK | MB_TOPMOST);
		close = false;
		ExitProcess(error);
	}
	else
		close = true;
}

void UpdateListbox() {
	if (cheat_number == 0) other_number = 1;
	else other_number = 0;

	g_pCheatData[cheat_number]->cheat_selection = true;
	g_pCheatData[other_number]->cheat_selection = false;
}

#pragma region Menu
void C_Menu::DrawMenu(bool& open, ImGuiStyle& style)
{
	VMProtectBeginUltra(__FUNCTION__);
	ImGui::PushFont(font_tabs);

	if (!updated) {
		for (int i = 0; i < NUMBER_OF_CHEATS; i++)
			game_select[i] = g_pCheatData[i]->cheat_selection;
		if (game_select[0]) cheat_number = 0;
		else if (game_select[1]) cheat_number = 1;
	}

	switch (sequence) {
	case MENU_LOGIN:
		if (!g_pUserData->was_invalid) {
			std::thread sle = std::thread(UpdateUser);
			sle.detach();
			sequence++;
			break;
		}
		else {
			ImGui::Begin(Login.c_str(), &open, ImVec2(g_pMenu->width, g_pMenu->height), 1.f, dwFlag);
			{
				ImGui::BeginChild(InnerChild.c_str(), ImVec2(284, 234), true);
				{

					ImGui::MyColorEdit4("##menu_color", main_clr, 1 << 7);
					style.Colors[ImGuiCol_PlotHistogram] = ImVec4(main_clr[0], main_clr[1], main_clr[2], main_clr[3] / 2);
					style.Colors[ImGuiCol_PlotHistogramHovered] = ImVec4(main_clr[0], main_clr[1], main_clr[2], main_clr[3]);
					style.Colors[ImGuiCol_CheckMark] = ImVec4(main_clr[0], main_clr[1], main_clr[2], main_clr[3]);

					ImGui::SameLine(116);
					ImGui::Text("TITAN");
					ImGui::Separator();

					ImGui::NewLine();
					ImGui::NewLine();

					ImGui::SameLine(49);
					ImGui::Text(Username.c_str());

					ImGui::NewLine();
					ImGui::SameLine(49);
					ImGui::InputText(htUsername.c_str(), g_pUserData->buf_username, ARRAYSIZE(g_pUserData->buf_username));

					ImGui::NewLine();

					ImGui::SameLine(49);
					ImGui::Text(Password.c_str());

					ImGui::NewLine();
					ImGui::SameLine(49);
					ImGui::InputText(htPassword.c_str(), g_pUserData->buf_password, ARRAYSIZE(g_pUserData->buf_password), ImGuiInputTextFlags_Password);

					ImGui::NewLine();
					ImGui::SameLine(49);
					if (ImGui::Button(Login.c_str(), ImVec2(184, 35)) || GetAsyncKeyState(VK_RETURN) && !updating) {
						std::thread sle = std::thread(UpdateUser);
						sle.detach();
						sequence++;
					}

				} ImGui::EndChild();

			} ImGui::End();
			break;
		}
	case MENU_AUTHENTICATE:
		g_pMenu->width = 225;
		g_pMenu->height = 83;

		if (g_pMenu->updated == 0)
			g_pMenu->need_update = true;

		ImGui::Begin(Login.c_str(), &open, ImVec2(g_pMenu->width, g_pMenu->height), 1.f, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
		{
			ImGui::BeginChild(InnerChild.c_str(), ImVec2(209, 68), true);
			{
				ImGui::Text(Loading.c_str());
				ImGui::Separator();

				ImGui::ProgressBar(g_pMenu->authentication_progress);

			} ImGui::EndChild();
		}
		ImGui::End();

		if (g_pMenu->authentication_progress >= 1.f && ready)
			sequence++;
		break;
	case MENU_CHEAT_SELECTION:
		g_pMenu->width = 400;
		g_pMenu->height = 250;
		if (g_pMenu->updated != 2)
			g_pMenu->need_update = true;

		cheats = 0;
		if (g_pUserData->cheat[0]) cheats++;
		if (g_pUserData->cheat[1]) cheats++;
		if (cheats == 1)
			cheat_number = 0;

		ImGui::Begin(Injection.c_str(), &open, ImVec2(g_pMenu->width, g_pMenu->height), 1.f, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
		{
			ImGui::Columns(2, htInjection.c_str(), false);

			ImGui::BeginChild(htGameSelect.c_str(), ImVec2(g_pMenu->width / 2.125f, g_pMenu->height - 17.f), true);
			{
				ImGui::Text(PurchasedGames.c_str());
				ImGui::Separator();

				if (g_pUserData->cheat[0]) {
					ImGui::ListBoxHeader("");
					ImGui::ListBox("", &cheat_number, csgo_only, ARRAYSIZE(csgo_only));
					ImGui::ListBoxFooter();
					other_number = cheat_number;
				}
				else {
					exit(0);
				}

				/*
				if (g_pUserData->cheat[0] && g_pUserData->cheat[1]) {
					ImGui::ListBoxHeader("");
					ImGui::ListBox("", &cheat_number, cs_amps_gm, ARRAYSIZE(cs_amps_gm));
					ImGui::ListBoxFooter();
					other_number = cheat_number;
				}
				else if (g_pUserData->cheat[0] && !g_pUserData->cheat[1]) {
					ImGui::ListBoxHeader("");
					ImGui::ListBox("", &cheat_number, csgo_only, ARRAYSIZE(csgo_only));
					ImGui::ListBoxFooter();
					other_number = cheat_number;
				}
				else if (!g_pUserData->cheat[0] && g_pUserData->cheat[1]) {
					ImGui::ListBoxHeader("");
					ImGui::ListBox("", &cheat_number, gmod_only, ARRAYSIZE(gmod_only));
					ImGui::ListBoxFooter();
					other_number = cheat_number;
				}*/

			} ImGui::EndChild();

			ImGui::NextColumn();
			ImGui::BeginChild(htInjectionOptions.c_str(), ImVec2((g_pMenu->width / 2.2f) - 1.f, g_pMenu->height / 2.f - 9.f), true);
			{
				if (ImGui::Button(Load.c_str(), ImVec2(164.f, (float)(g_pMenu->height / 4.f - 18.f)))) {
					if (other_number != -1) {
						sequence++;

						if (other_number == 0) {
							g_pCheatData[0]->cheat_selection = true;
							g_pCheatData[1]->cheat_selection = false;
						}
						else if (other_number == 1) {
							g_pCheatData[0]->cheat_selection = false;
							g_pCheatData[1]->cheat_selection = true;
						}

						if (other_number == 0 && g_pUserData->cheat[0] && g_pUserData->cheat[1]) {
							g_pCheatData[0]->cheat_selection = true;
							g_pCheatData[1]->cheat_selection = false;
						}
						else if (other_number == 1 && g_pUserData->cheat[0] && g_pUserData->cheat[1]) {
							g_pCheatData[0]->cheat_selection = false;
							g_pCheatData[1]->cheat_selection = true;
						}
						else if (g_pUserData->cheat[0] && !g_pUserData->cheat[1]) {
							g_pCheatData[0]->cheat_selection = true;
							g_pCheatData[1]->cheat_selection = false;
						}
						else if (!g_pUserData->cheat[0] && g_pUserData->cheat[1]) {
							g_pCheatData[0]->cheat_selection = false;
							g_pCheatData[1]->cheat_selection = true;
						}


						if (g_pMemory->StringCompareA(g_pCheatData[other_number]->status, Status_Undetected.c_str())) {}
						else if (g_pMemory->StringCompareA(g_pCheatData[other_number]->status, Status_Unknown.c_str()) || g_pMemory->StringCompareA(g_pCheatData[other_number]->status, Status_Detected.c_str()))
							MessageBox(0, CheatUnsafe.c_str(), Warning.c_str(), MB_OK | MB_ICONWARNING | MB_TOPMOST);
						else if (g_pMemory->StringCompareA(g_pCheatData[other_number]->status, Status_Disabled.c_str())) {
							MessageBox(0, CheatDisabled.c_str(), Error.c_str(), MB_OK | MB_ICONERROR | MB_TOPMOST);
							ExitProcess(0);
						}

						std::thread inject(Inj);
						inject.detach();
					}
					else MessageBox(0, SelectCheat.c_str(), Error.c_str(), MB_OK | MB_TOPMOST);
				}

				if (ImGui::Button(Exit.c_str(), ImVec2(164.f, (float)(g_pMenu->height / 4 - 18)))) ExitProcess(0);

			} ImGui::EndChild();

			ImGui::BeginChild(htStatus.c_str(), ImVec2((float)(g_pMenu->width / 2.2f) - 1.f, (float)(g_pMenu->height / 2.f - 17.f)), true);
			{
				if (g_pUserData->cheat[0] && g_pUserData->cheat[1] && other_number == 0) {
					ImGui::Text(Status_col.c_str());
					ImGui::SameLine();
					ImGui::TextColored(g_pCheatData[0]->status_color, g_pCheatData[0]->status);
					ImGui::Separator();

					ImGui::Text(SubscriptionBuffer.c_str(), g_pUserData->csgo_sub);
				}
				else if (g_pUserData->cheat[0] && g_pUserData->cheat[1] && other_number == 1) {
					ImGui::Text(Status_col.c_str());
					ImGui::SameLine();
					ImGui::TextColored(g_pCheatData[1]->status_color, g_pCheatData[1]->status);
					ImGui::Separator();

					ImGui::Text(SubscriptionBuffer.c_str(), g_pUserData->gmod_sub);
				}
				else if (g_pUserData->cheat[0] && !g_pUserData->cheat[1]) {
					ImGui::Text(Status_col.c_str());
					ImGui::SameLine();
					ImGui::TextColored(g_pCheatData[0]->status_color, g_pCheatData[0]->status);
					ImGui::Separator();

					ImGui::Text(SubscriptionBuffer.c_str(), g_pUserData->csgo_sub);
				}
				else if (!g_pUserData->cheat[0] && g_pUserData->cheat[1]) {
					ImGui::Text(Status_col.c_str());
					ImGui::SameLine();
					ImGui::TextColored(g_pCheatData[1]->status_color, g_pCheatData[1]->status);
					ImGui::Separator();

					ImGui::Text(SubscriptionBuffer.c_str(), g_pUserData->gmod_sub);
				}

				ImGui::Text(LastUpdateBuffer.c_str(), g_pCheatData[other_number]->update_time);

				ImGui::PopFont();
				ImGui::PushFont(font_alert);
				rt_string SessionExpires(26, S, e, s, s, i, o, n, spc, e, x, p, i, r, e, s, spc, i, n, spc, prc, i, spc, s, e, c);
				ImGui::TextColored(style.Colors[ImGuiCol_PlotHistogramHovered], SessionExpires.c_str(), abs(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - g_pMenu->timer).count()));
				ImGui::PopFont();
				ImGui::PushFont(font_tabs);

			} ImGui::EndChild();
		} ImGui::End();
		break;
	case MENU_INJECTION:
		g_pMenu->width = 225;
		g_pMenu->height = 83;

		if (g_pMenu->updated != 3)
			g_pMenu->need_update = true;

		ImGui::Begin(LoadingGame.c_str(), &open, ImVec2(g_pMenu->width, g_pMenu->height), 1.f, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
		{
			ImGui::BeginChild(htFinished.c_str(), ImVec2(209, 68), true);
			{
				ImGui::Text(Injecting.c_str());
				ImGui::Separator();

				ImGui::ProgressBar(g_pMenu->inject_progress);

			} ImGui::EndChild();
		} ImGui::End();
	}
	if (g_pMenu->inject_progress >= 1.f) {
		style.Alpha -= 0.03f;

		if (style.Alpha <= 0.f && close)
			ExitProcess(0);
	}
	ImGui::PopFont();
	VMProtectEnd();
}
#pragma endregion

#pragma region MenuSetup

void C_Menu::TextColor(bool active)
{
	ImGuiStyle& style = ImGui::GetStyle();

	if (active)
	{
		style.Colors[ImGuiCol_Text] = ImColor(255, 255, 255);
		style.Colors[ImGuiCol_Text] = ImColor(255, 255, 255);
		style.Colors[ImGuiCol_Text] = ImColor(255, 255, 255);
	}
	else
	{
		style.Colors[ImGuiCol_Text] = ImColor(200, 200, 200);
		style.Colors[ImGuiCol_Text] = ImColor(200, 200, 200);
		style.Colors[ImGuiCol_Text] = ImColor(200, 200, 200);
	}
}

void C_Menu::InitStyle(ImGuiStyle& style)
{
	static bool setStyle = false;

	if (setStyle)
		return;

	style.WindowPadding = ImVec2(8, 8);
	style.WindowRounding = 0.f;
	style.FramePadding = ImVec2(4, 3);
	style.FrameRounding = 0.f;
	style.ItemSpacing = ImVec2(8, 8);
	style.AntiAliasedLines = true;
	style.AntiAliasedShapes = true;
	style.Alpha = 0.1f;
	style.CurveTessellationTol = 1.25f;
	style.ItemInnerSpacing = ImVec2(8, 6);
	style.DisplayWindowPadding = ImVec2(22, 22);
	style.IndentSpacing = 21.f;
	style.ScrollbarSize = 6.f;
	style.ScrollbarRounding = 2.f;
	style.GrabMinSize = 5.f;
	style.ButtonTextAlign = ImVec2(0.5f, 0.5f);
	style.GrabRounding = 3.f;

	setStyle = true;
}

void C_Menu::InitColors(ImGuiStyle& style)
{
	static bool setColor = false;

	if (setColor)
		return;

	style.Colors[ImGuiCol_Text] = ImColor(210, 210, 210, 255);
	style.Colors[ImGuiCol_TextDisabled] = ImVec4(0.60f, 0.60f, 0.60f, 1.00f);
	style.Colors[ImGuiCol_WindowBg] = ImColor(25, 25, 25, 255);
	style.Colors[ImGuiCol_ChildWindowBg] = ImColor(90, 90, 90, 1);
	style.Colors[ImGuiCol_PopupBg] = ImVec4(0.05f, 0.05f, 0.10f, 0.90f);
	style.Colors[ImGuiCol_Border] = ImColor(44, 44, 44, 255);
	style.Colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_FrameBg] = ImColor(34, 34, 34, 255);
	style.Colors[ImGuiCol_FrameBgActive] = ImColor(167, 244, 66, 255);
	style.Colors[ImGuiCol_TitleBg] = ImVec4(0.27f, 0.27f, 0.54f, 0.f);
	style.Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.40f, 0.40f, 0.80f, 0.f);
	style.Colors[ImGuiCol_TitleBgActive] = ImVec4(0.32f, 0.32f, 0.63f, 0.f);
	style.Colors[ImGuiCol_MenuBarBg] = ImVec4(0.40f, 0.40f, 0.55f, 0.80f);
	style.Colors[ImGuiCol_ScrollbarBg] = ImVec4(0.10f, 0.10f, 0.10f, 1.f);
	style.Colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.25f, 0.25f, 0.25f, 1.f);
	style.Colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.25f, 0.25f, 0.25f, 1.f);
	style.Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.25f, 0.25f, 0.25f, 1.f);
	style.Colors[ImGuiCol_ComboBg] = ImVec4(0.12f, 0.12f, 0.12f, 1.f);
	style.Colors[ImGuiCol_CheckMark] = ImVec4(g_pMenu->main_clr[0], g_pMenu->main_clr[1], g_pMenu->main_clr[2], g_pMenu->main_clr[3]);
	style.Colors[ImGuiCol_SliderGrab] = ImColor(0, 255, 255, 255);
	style.Colors[ImGuiCol_SliderGrabActive] = ImColor(0, 255, 255, 255);
	style.Colors[ImGuiCol_Button] = ImColor(35, 35, 35, 255);
	style.Colors[ImGuiCol_ButtonHovered] = ImColor(43, 43, 43, 255);
	style.Colors[ImGuiCol_ButtonActive] = ImVec4(0.25f, 0.25f, 0.25f, 1.00f);
	style.Colors[ImGuiCol_Header] = ImVec4(0.40f, 0.40f, 0.90f, 0.45f);
	style.Colors[ImGuiCol_HeaderHovered] = ImVec4(0.45f, 0.45f, 0.90f, 0.80f);
	style.Colors[ImGuiCol_HeaderActive] = ImVec4(0.53f, 0.53f, 0.87f, 0.80f);
	style.Colors[ImGuiCol_ResizeGrip] = ImVec4(1.00f, 1.00f, 1.00f, 0.30f);
	style.Colors[ImGuiCol_ResizeGripHovered] = ImVec4(1.00f, 1.00f, 1.00f, 0.60f);
	style.Colors[ImGuiCol_ResizeGripActive] = ImVec4(1.00f, 1.00f, 1.00f, 0.90f);
	style.Colors[ImGuiCol_CloseButton] = ImVec4(0.50f, 0.50f, 0.90f, 0.50f);
	style.Colors[ImGuiCol_CloseButtonHovered] = ImVec4(0.70f, 0.70f, 0.90f, 0.60f);
	style.Colors[ImGuiCol_CloseButtonActive] = ImVec4(0.70f, 0.70f, 0.70f, 1.00f);
	style.Colors[ImGuiCol_PlotLines] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
	style.Colors[ImGuiCol_PlotLinesHovered] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_PlotHistogram] = ImVec4(g_pMenu->main_clr[0], g_pMenu->main_clr[1], g_pMenu->main_clr[2], g_pMenu->main_clr[3] / 2);
	style.Colors[ImGuiCol_PlotHistogramHovered] = ImVec4(g_pMenu->main_clr[0], g_pMenu->main_clr[1], g_pMenu->main_clr[2], g_pMenu->main_clr[3]);
	style.Colors[ImGuiCol_TextSelectedBg] = ImVec4(0.00f, 0.00f, 1.00f, 0.35f);
	style.Colors[ImGuiCol_ModalWindowDarkening] = ImVec4(0.20f, 0.20f, 0.20f, 0.35f);

	setColor = true;
}

void C_Menu::TabColor(bool bActive)
{
	ImGuiStyle& style = ImGui::GetStyle();

	if (bActive)
		style.Colors[ImGuiCol_Text] = style.Colors[ImGuiCol_CheckMark];
	else
		style.Colors[ImGuiCol_Text] = ImColor(210, 210, 210, 255);
}

void C_Menu::InitFonts()
{
	static bool setupFonts = false;

	if (setupFonts)
		return;
	font_text = ImGui::GetIO().Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\Verdana.ttf", 13);
	font_title = ImGui::GetIO().Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\Verdana.ttf", 16);
	font_tabs = ImGui::GetIO().Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\Verdana.ttf", 15);
	font_alert = ImGui::GetIO().Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\Verdanab.ttf", 14.5);

	setupFonts = true;
}

#pragma endregion