#include "Config.h"
#include "cheatdata.h"
#include "userdata.h"
#include "console.h"
#include "encrypt.h"
#include "rt_string.h"
#include "menu.h"

#include <string>
#include <Windows.h>

#pragma region Disabling warnings

#pragma warning(disable: 4244)
#pragma warning(disable: 4800)
#pragma warning(disable: 4018)
#pragma warning(disable: 4715)

#pragma endregion

C_Config* g_pConfig = new C_Config();

inline bool Check(std::string File)
{
	struct stat buf;
	return (stat(File.c_str(), &buf) == 0);
}

bool C_Config::CheckConfigs()
{
	rt_string DefaultFormat_002(5, prc, s, prc, s);
	rt_string Lockdown(9, L, o, c, k, d, o, w, n);
	rt_string Cfg(4, c, f, g);
	rt_string LockdownCFG(17, L, o, c, k, d, o, w, n, bsl, c, f, g, tp, i, n, i);

	/*=====================================*/
	char Temp[MAX_PATH] = { 0 };
	GetTempPathA(MAX_PATH, Temp);

	char TempPath[MAX_PATH] = { 0 };
	snprintf(TempPath, MAX_PATH, DefaultFormat_002.c_str(), Temp, g_pEncrypt->Encrypt((char*)Lockdown.c_str()).c_str());

	char FullPath[MAX_PATH] = { 0 };
	snprintf(FullPath, MAX_PATH, DefaultFormat_002.c_str(), Temp, std::string(g_pEncrypt->Encrypt((char*)Lockdown.c_str()).c_str()).append("\\").append(g_pEncrypt->Encrypt((char*)Cfg.c_str()).c_str()).c_str());

	strcat(Path, FullPath);
	CreateDirectoryA(TempPath, nullptr);
	strcpy(Path2, Path);
	strcpy(Path3, Path);
	strcpy(Path4, Path);

	return true;
}

void C_Config::Delete() {
	DeleteFile(Handle());
}

char* C_Config::Handle()
{
	return Path;
}

int C_Config::ReadInt(char* Cata, char* Name, int DefaultVal)
{
	int iResult;
	iResult = GetPrivateProfileIntA(Cata, Name, DefaultVal, this->Handle());
	return iResult;
}

float C_Config::ReadFloat(char* Cata, char* Name, float DefaultVal)
{
	char result[255];
	char cDefault[255];
	float fresult;
	sprintf(cDefault, "%f", DefaultVal);
	GetPrivateProfileStringA(Cata, Name, cDefault, result, 255, this->Handle());
	fresult = atof(result);
	return fresult;
}

char* C_Config::ReadString(char* Cata, char* Name, char* DefaultVal)
{
	auto result = new char[255];
	DWORD oProtection;
	VirtualProtect(reinterpret_cast<void*>(result), 4, PAGE_READWRITE, &oProtection);
	memset(result, 0x00, 255);
	VirtualProtect(reinterpret_cast<void*>(result), 4, oProtection, &oProtection);
	GetPrivateProfileStringA(Cata, Name, DefaultVal, result, 255, this->Handle());
	return result;
}

void C_Config::WriteFloat(char* Cata, char* Name, float SetVal)
{
	char buf[255];
	sprintf(buf, "%f", SetVal);
	WritePrivateProfileStringA(Cata, Name, buf, this->Handle());
}

void C_Config::WriteInt(char* Cata, char* Name, int SetVal)
{
	char buf[255];
	sprintf(buf, "%d", SetVal);
	WritePrivateProfileStringA(Cata, Name, buf, this->Handle());
}

void C_Config::WriteString(char* Cata, char* Name, char* SetVal)
{
	WritePrivateProfileStringA(Cata, Name, SetVal, this->Handle());
}


void C_Config::Save()
{
	WriteString((char*)g_pEncrypt->Encrypt((char*)"Credentials").c_str(), (char*)g_pEncrypt->Encrypt((char*)"Username").c_str(), (char*)g_pEncrypt->Encrypt(g_pUserData->buf_username).c_str());
	WriteString((char*)g_pEncrypt->Encrypt((char*)"Credentials").c_str(), (char*)g_pEncrypt->Encrypt((char*)"Password").c_str(), (char*)g_pEncrypt->Encrypt(g_pUserData->buf_password).c_str());
	WriteInt((char*)g_pEncrypt->Encrypt((char*)"Loader").c_str(), (char*)g_pEncrypt->Encrypt((char*)"RunNextTime").c_str(), (int)g_pUserData->was_invalid + 5);

	for (int i = 0; i <= 3; i++)
		WriteFloat((char*)(g_pEncrypt->Encrypt((char*)"Color").c_str()), (char*)(std::string(g_pEncrypt->Encrypt((char*)"Index[").c_str()).append(std::to_string(i + 5)).append(g_pEncrypt->Encrypt((char*)"]").c_str()).c_str()), (float)g_pMenu->main_clr[i]);

	for (int i = 0; i < 2; i++)
		WriteInt((char*)(g_pEncrypt->Encrypt((char*)"Cheat").c_str()), (char*)(std::string(g_pEncrypt->Encrypt((char*)"Selection[").c_str()).append(std::to_string(i + 5)).append(g_pEncrypt->Encrypt((char*)"]").c_str()).c_str()), (int)g_pCheatData[i]->cheat_selection);
}

void C_Config::Load()
{
	strcpy(g_pUserData->buf_username, g_pEncrypt->Decrypt(ReadString((char*)g_pEncrypt->Encrypt((char*)"Credentials").c_str(), (char*)g_pEncrypt->Encrypt((char*)"Username").c_str(), (char*)g_pEncrypt->Encrypt((char*)"Username").c_str())).c_str());
	strcpy(g_pUserData->buf_password, g_pEncrypt->Decrypt(ReadString((char*)g_pEncrypt->Encrypt((char*)"Credentials").c_str(), (char*)g_pEncrypt->Encrypt((char*)"Password").c_str(), (char*)g_pEncrypt->Encrypt((char*)"Password").c_str())).c_str());
	g_pUserData->was_invalid = ReadInt((char*)g_pEncrypt->Encrypt((char*)"Loader").c_str(), (char*)g_pEncrypt->Encrypt((char*)"RunNextTime").c_str(), 1) - 5;

	for (int i = 0; i <= 3; i++)
		g_pMenu->main_clr[i] = ReadFloat((char*)(g_pEncrypt->Encrypt((char*)"Color").c_str()), (char*)(std::string(g_pEncrypt->Encrypt((char*)"Index[").c_str()).append(std::to_string(i + 5)).append(g_pEncrypt->Encrypt((char*)"]").c_str()).c_str()), 2.f);
	
	if (g_pMenu->main_clr[0] == 2.f)
		g_pMenu->main_clr[0] = 46.f / 255.f;
	if (g_pMenu->main_clr[1] == 2.f)
		g_pMenu->main_clr[1] = 204.f / 255.f;
	if (g_pMenu->main_clr[2] == 2.f)
		g_pMenu->main_clr[2] = 113 / 255.f;
	if (g_pMenu->main_clr[3] == 2.f)
		g_pMenu->main_clr[3] = 1.f;

	for (int i = 0; i < 2; i++)
		g_pCheatData[i]->cheat_selection = ReadInt((char*)(g_pEncrypt->Encrypt((char*)"Cheat").c_str()), (char*)(std::string(g_pEncrypt->Encrypt((char*)"Selection[").c_str()).append(std::to_string(i + 5)).append(g_pEncrypt->Encrypt((char*)"]").c_str()).c_str()), 0);
}