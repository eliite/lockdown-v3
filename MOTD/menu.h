#pragma once

#ifndef menu_h
#define menu_h

#include "ImGui/imgui.h"
#include "manual_map.h"
#include "userdata.h"
#include <chrono>
#include <future>
#include <Windows.h>

static const char* csgo_only[] = { "CS:GO" };
static const char* gmod_only[] = { "Garry's Mod" };
static const char* cs_amps_gm[] = { "CS:GO", "Garry's Mod" };

enum E_GameID {
	CSGO = 0,
	GMOD = 1
};

class C_Menu
{
public:
	
	void DrawMenu( bool&, ImGuiStyle& );
	void TextColor( bool );
	void InitStyle( ImGuiStyle& );
	void InitColors( ImGuiStyle& );
	void TabColor(bool bActive);
	void InitFonts();

	float height = 250.f, width = 300.f;
	int sequence = 0;
	int updated = 0;

	bool need_update = false;

	float alpha_fade = 0.02f;
	float inject_progress = 0.f;
	float authentication_progress = 0.f;

	float main_clr[4] = { 46.f/255.f, 204.f/255.f, 113/255.f, 1.f };


	std::chrono::steady_clock::time_point timer;
private:
	int selected_tab;

	ImFont* font_text;
	ImFont* font_title;
	ImFont* font_tabs;
	ImFont* font_alert;

	DWORD dwFlag = ImGuiWindowFlags_NoResize 
		| ImGuiWindowFlags_NoSavedSettings 
		| ImGuiWindowFlags_NoMove
		| ImGuiWindowFlags_NoTitleBar
		| ImGuiWindowFlags_NoCollapse
		| ImGuiWindowFlags_NoScrollbar
		| ImGuiWindowFlags_NoScrollWithMouse;
};

extern C_Menu* g_pMenu;

#endif

