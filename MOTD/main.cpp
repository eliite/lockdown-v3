#include "WndProc.h"
#include "menu.h"
#include "cheatdata.h"
#include "clientdata.h"
#include "config.h"
#include "defines.h"
#include "encrypt.h"
#include "console.h"
#include "memory.h"
#include "hardware.h"
#include "protection.h"
#include "inet.h"
#include "sha.h"

#include <chrono>
#include <future>
#include <Shobjidl.h>
#include <string>
#include <thread>

#include "protection/VMProtectSDK.h"

#pragma comment(lib, "Winmm.lib")
#pragma comment(lib, "ntdll.lib")

std::string RandomString(int Length)
{
	VMProtectBeginUltra(__FUNCTION__);

	srand((UINT)time(NULL));
	char temp[32 + 1];
	for (int i = 0; i < Length; ++i)
		temp[i] = 65 + rand() % 26 + 32;
	temp[Length] = '\0';

	VMProtectEnd();

	return temp;
}

ATOM RegMyWindowClass(HINSTANCE hInst, LPCTSTR lpzClassName)
{
	WNDCLASS wcWindowClass = { 0 };

	wcWindowClass.lpfnWndProc = (WNDPROC)WndProc;
	wcWindowClass.style = CS_HREDRAW | CS_VREDRAW;
	wcWindowClass.hInstance = hInst;
	wcWindowClass.lpszClassName = lpzClassName;
	wcWindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcWindowClass.hbrBackground = (HBRUSH)COLOR_APPWORKSPACE;

	return RegisterClass(&wcWindowClass);
}

void RunProt() {
	g_pProtection->Verify();
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	VMProtectBeginUltra(__FUNCTION__);

	while (g_pInject->GetProcessID("csgo.exe"))
		TerminateProcess(OpenProcess(PROCESS_ALL_ACCESS, false, g_pInject->GetProcessID("csgo.exe")), 0);

	//int retval = ::_tsystem(_T("taskkill /F /IM csgo.exe"));

	g_pHardware->Renew();
	g_pEncrypt->GenerateKey();

#ifndef _DEBUG
	std::thread run_prot(RunProt);
	run_prot.detach();
	g_pProtection->thread_time = time(NULL);
#endif
#ifndef NDEBUG
	g_pConsole->OpenConsole(Lockdown.c_str());
#endif

	if (!RegMyWindowClass(hInstance, Lockdown.c_str()))
		return 1;

	g_pMenu->timer = std::chrono::steady_clock::now() + std::chrono::seconds(60);
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	std::string file_path = std::string(buffer);
	std::string random = RandomString(9);
	std::rename(buffer, file_path.substr(0, file_path.find_last_of(rt_string(2, bsl).c_str()) + 1).append(random).append(rt_string(5, tp, e, x, e).c_str()).c_str());
	g_pClientData->name = random.c_str();

	std::string filename = std::string(buffer);
	std::string directory;
	const size_t last_slash_idx = filename.rfind('\\');
	if (std::string::npos != last_slash_idx)
		directory = filename.substr(0, last_slash_idx);

	snprintf(buffer, MAX_PATH, rt_string(10, prc, s, bsl, prc, s, tp, e, x, e).c_str(), directory.c_str(), g_pClientData->name);
	HANDLE hFile = CreateFileA(buffer, GENERIC_READ | GENERIC_WRITE, NULL, NULL, OPEN_EXISTING, NULL, NULL);
	SYSTEMTIME system_time;

	srand(time(NULL));
	GetSystemTime(&system_time);
	system_time.wMilliseconds = (rand() % (999 - 0 + 1)) + 0;
	system_time.wSecond = (rand() % (50 - 0 + 1)) + 0;
	system_time.wMinute = (rand() % (59 - 0 + 1)) + 0;
	system_time.wHour = (rand() % (23 - 0 + 1)) + 0;
	system_time.wDay = (rand() % (31 - 1 + 1)) + 1;
	system_time.wMonth = (rand() % (12 - 1 + 1)) + 1;
	system_time.wYear = (rand() % (2018 - 1982 + 1)) + 1982;

	FILETIME file_time;
	SystemTimeToFileTime(&system_time, &file_time);
	hFile = CreateFile(buffer, FILE_WRITE_ATTRIBUTES, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	SetFileTime(hFile, (LPFILETIME)&file_time, (LPFILETIME)&file_time, &file_time);
	CloseHandle(hFile);

	if (g_pConfig->CheckConfigs())
		g_pConfig->Load();

	RECT desktop;
	int horizontal = 0;
	int vertical = 0;
	HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &desktop);

	HWND hWnd = CreateWindowExA(WS_EX_TOPMOST, Lockdown.c_str(), 0, WS_POPUP, ((desktop.right / 2) - (g_pMenu->width / 2)), ((desktop.bottom / 2) - (g_pMenu->height / 2)), g_pMenu->width, g_pMenu->height, NULL, NULL, hInstance, NULL);
	SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, g_pMenu->width, g_pMenu->height, SWP_NOMOVE);

	if (!g_pUserData->was_invalid) {
		g_pMenu->width = 225;
		g_pMenu->height = 83;
		hWnd = CreateWindowEx(WS_EX_TOPMOST, Lockdown.c_str(), 0, WS_POPUP, ((desktop.right / 2) - (g_pMenu->width / 2)), ((desktop.bottom / 2) - (g_pMenu->height / 2)), g_pMenu->width, g_pMenu->height, NULL, NULL, hInstance, NULL);
		SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, g_pMenu->width, g_pMenu->height, SWP_NOMOVE);
	}

	ITaskbarList *pTaskList = NULL;
	HRESULT initRet = CoInitialize(NULL);
	HRESULT createRet = CoCreateInstance(CLSID_TaskbarList,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_ITaskbarList,
		(LPVOID*)&pTaskList);

	if (createRet == S_OK)
	{
		pTaskList->DeleteTab(hWnd);
		pTaskList->Release();
	}

	CoUninitialize();

	if (!hWnd)
		return 2;

	LPDIRECT3D9 pD3D;

	if ((pD3D = Direct3DCreate9(D3D_SDK_VERSION)) == NULL)
		UnregisterClass(Lockdown.c_str(), hInstance);

	ZeroMemory(&g_d3dpp, sizeof(g_d3dpp));
	g_d3dpp.Windowed = TRUE;
	g_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	g_d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	g_d3dpp.EnableAutoDepthStencil = TRUE;
	g_d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	g_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE; // Present with vsync

	if (pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &g_d3dpp, &g_pd3dDevice) < 0) {
		pD3D->Release();
		UnregisterClass(Lockdown.c_str(), hInstance);
		return 0;
	}

	// Setup ImGui binding
	ImGui_ImplDX9_Init(hWnd, g_pd3dDevice);

	ImGuiStyle& style = ImGui::GetStyle();

	g_pMenu->InitStyle(style);
	g_pMenu->InitColors(style);
	g_pMenu->InitFonts();

	// Main loop
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	ShowWindow(hWnd, SW_SHOWDEFAULT);
	UpdateWindow(hWnd);


	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			continue;
		}

#ifndef _DEBUG
		if (static_cast<long int>(time(NULL)) > (g_pProtection->thread_time + 1)) {
			run_prot = std::thread(RunProt);
			run_prot.detach();
		}
#endif

		if (g_pCheatData[0]->cheat_selection && g_pCheatData[1]->cheat_selection)
			g_pCheatData[1]->cheat_selection = false;

		if (std::chrono::steady_clock::now() > g_pMenu->timer)
			ExitProcess(0);

		//backup render states
		DWORD colorwrite, srgbwrite;
		g_pd3dDevice->GetRenderState(D3DRS_COLORWRITEENABLE, &colorwrite);
		g_pd3dDevice->GetRenderState(D3DRS_SRGBWRITEENABLE, &srgbwrite);

		// fix drawing without calling engine functons/cl_showpos
		g_pd3dDevice->SetRenderState(D3DRS_COLORWRITEENABLE, 0xffffffff);
		// removes the source engine color correction
		g_pd3dDevice->SetRenderState(D3DRS_SRGBWRITEENABLE, false);

		ImGui_ImplDX9_NewFrame();

		static bool open = true;

		if (g_pMenu->need_update) {
			SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, (int)g_pMenu->width, (int)g_pMenu->height, SWP_NOMOVE);
			g_pMenu->need_update = false;
			g_pMenu->updated++;
		}

		if (style.Alpha < 1.f) {
			style.Alpha += g_pMenu->alpha_fade;
			SetLayeredWindowAttributes(hWnd, RGB(0, 0, 0), (BYTE)style.Alpha, LWA_ALPHA);
		}

		if (!open) {

			ExitProcess(0);
		}
		else
			g_pMenu->DrawMenu(open, style);

		if (g_pd3dDevice->BeginScene() >= 0)
		{
			ImGui::Render();
			g_pd3dDevice->EndScene();
		}
		g_pd3dDevice->Present(NULL, NULL, NULL, NULL);

		g_pd3dDevice->SetRenderState(D3DRS_COLORWRITEENABLE, colorwrite);
		g_pd3dDevice->SetRenderState(D3DRS_SRGBWRITEENABLE, srgbwrite);
	}

	ImGui_ImplDX9_Shutdown();
	if (g_pd3dDevice) g_pd3dDevice->Release();
	if (pD3D) pD3D->Release();
	UnregisterClass(Lockdown.c_str(), hInstance);

	VMProtectEnd();
	return 0;
}