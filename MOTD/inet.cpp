#include "inet.h"
#include "console.h"
#include "encrypt.h"
#include "hardware.h"
#include "memory.h"
#include <thread>
#include <WinInet.h>
#include "protection/VMProtectSDK.h"

C_Communication* g_pRequest;

REQUEST C_Communication::Create(char* szData, char* szResponse, int size, char* szHeader, float* const &prog, float amount) {
	VMProtectBegin(__FUNCTION__);
	HANDLE hOpen = nullptr;

	if (WININET_USE_PROXY)
		hOpen = InternetOpenA(AGENT.c_str(), INTERNET_OPEN_TYPE_PROXY | INTERNET_OPEN_TYPE_PRECONFIG, (LPCSTR)SOCKS.c_str(), 0, 0);
	else
		hOpen = InternetOpenA(AGENT.c_str(), INTERNET_OPEN_TYPE_PRECONFIG, 0, 0, 0);
	if (prog) *prog += (amount / 6.f);

	if (!hOpen) {
		return WININET_INTERNETOPEN_FAILED;
	}
	if (prog) *prog += (amount / 6.f);

	HANDLE hConnect = InternetConnectA(hOpen, "cdn.titanhack.net", INTERNET_DEFAULT_HTTPS_PORT, ldr_username.c_str(), ldr_password.c_str(), INTERNET_SERVICE_HTTP, 0, 0);
	if (!hConnect) {
		return WININET_INTERNETCONNECT_FAILED;
	}
	if (prog) *prog += (amount / 6.f);

	HANDLE hRequest = HttpOpenRequestA(hConnect, POST.c_str(), action_php.c_str(), 0, 0, AcceptTypes, INTERNET_FLAG_RELOAD | INTERNET_FLAG_KEEP_CONNECTION | INTERNET_FLAG_SECURE, 0);
	if (!hRequest) {

		return WININET_OPENREQUEST_FAILED;
	}
	if (prog) *prog += (amount / 6.f);

	bool bSent = HttpSendRequestA(hRequest, szHeader, strlen(szHeader), szData, strlen(szData));
	if (!bSent)
		return WININET_SENDREQUEST_FAILED;
	if (prog) *prog += (amount / 6.f);

	ULONG BytesRead = 0;
	while (InternetReadFile(hRequest, szResponse, size, &BytesRead) && BytesRead) {
		szResponse[BytesRead] = 0x0;
		BytesRead = 0;
	}
	if (prog) *prog += (amount / 6.f);

	InternetCloseHandle(hOpen);
	//InternetCloseHandle(hConnect);
	InternetCloseHandle(hRequest);
	VMProtectEnd();
	return WININET_PROCESS_SUCCESS;
}

REQUEST C_Communication::Log(const char* err, int line, const char* file) {

	std::string s(file);
	const size_t last_slash_idx = s.find_last_of("\\/");
	if (std::string::npos != last_slash_idx)
		s.erase(0, last_slash_idx + 1);

	char buffer[0x200] = { 0 };
	char response[0x100] = { 0 };
	snprintf(buffer, 0x200, error_uri.c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::CPU_VENDOR)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::CPU)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::HDD_SERIAL)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::VIDEO_ADAPTER)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::PC_NAME)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::OS)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::OS_INSTALL_DATE)).c_str(),
		g_pEncrypt->Decrypt(g_pHardware->Retrieve(E_Hardware::PHYS_MEM_SIZE)).c_str(),
		g_pEncrypt->Decrypt((char*)err).c_str(), g_pEncrypt->Decrypt((char*)std::to_string(line).c_str()).c_str(), g_pEncrypt->Decrypt((char*)s.c_str()).c_str());
	g_pRequest->Create((char*)g_pMemory->EncodeUrl(buffer).c_str(), response, SIZEOF(response), (char*)GenericHeader.c_str());

	return WININET_PROCESS_SUCCESS;
}

VOID C_Communication::Close(HANDLE hHandle) {
	InternetCloseHandle(hHandle);
}

void NtMessageBox(const char* error, int line, const char* file) {
	std::thread err_thread(C_Communication::Log, error, line, file);
	err_thread.detach();

	MessageBoxA(0, error, Error.c_str(), MB_TOPMOST | MB_OK | MB_ICONERROR);
}

void NtMessageBox(const char* error, int line, const char* file, const char* message, UINT type) {
	std::thread err_thread(C_Communication::Log, error, line, file);
	err_thread.detach();

	MessageBoxA(0, error, message, type);
}