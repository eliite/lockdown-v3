#ifndef __HARDWARE_H__
#define __HARDWARE_H__

extern char CpuProcessorVendor[0x100];
extern char CpuProcessorNameAndSpeed[0x100];
extern char HarddiskSerialNumber[0x100];
extern char VideoAdapter[0x100];
extern char ComputerName[0x100];
extern char PCOperatingSystem[0x100];
extern char InstallationDate[0x100];
extern char PhysicalMemory[0x100];
extern char MonitorName[0x100];

enum E_Hardware : int {
	CPU_VENDOR,
	CPU,
	HDD_SERIAL,
	PC_NAME,
	VIDEO_ADAPTER,
	OS,
	OS_INSTALL_DATE,
	PHYS_MEM_SIZE,
	MONITOR_NAME
};

typedef struct _CPUID_REGISTERS
{
	unsigned int eax;
	unsigned int ebx;
	unsigned int ecx;
	unsigned int edx;
} CPUID_REGISTERS, *PCPUID_REGISTERS;

class C_HWID {
public:
	void Renew();
	char* Retrieve(E_Hardware Type);
	int Hash();
private:
	void Retrieve1();
	void Retrieve2();
	void Retrieve3(unsigned short DriveNumber); // hdd serial
	void Retrieve4();
	void Retrieve5();
	void Retrieve6();
	void Retrieve7();
	void Retrieve8(); // Memory size
	void Retrieve9(); // Monitor name
};

extern C_HWID* g_pHardware;

#endif