#pragma once
#ifndef __PARSER_H__
#define __PARSER_H__

#include <Windows.h>
#include <string>

class C_Parser {
public:
	const char* Parse(const char* string, char* response);
};

extern C_Parser* g_pParser;

#endif